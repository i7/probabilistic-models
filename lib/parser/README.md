These are some files blatantly ripped out of PRISM-games, without any major changes (to make merging of upstream changes easier)
The purpose of this package is to be able to parse PRISM files without needing to depend on the entirety of PRISM (which would require C compilation etc.)

The current reference version is [v3.2 of PRISM-games](https://github.com/prismmodelchecker/prism-games/releases/tag/v3.2).