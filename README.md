Probabilistic Models
====================

A library of high-performance implementations for probabilistic systems.

Setup for other projects
=======================

1. Add this repository as a submodule `git submodule add ../../i7/probabilistic-models.git <path>`, `<path>` could for example be `lib/models`.
   The relative path is needed for submodules to work nicely with GitLab.
2. Init the submodule `git submodule update --init --recursive`
3. Add this project in your `settings.gradle` with `include '<path>'` (use `:` instead of `/`, for example `lib:models`) and similarly `<path>:lib:parser`.
4. Add it as dependency in `build.gradle` with `implementation project('<path>')`
