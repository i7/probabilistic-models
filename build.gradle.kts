plugins {
    `java-library`

    pmd
    idea

    id("com.diffplug.spotless") version "6.25.0"
}

group = "de.tum.in"
version = "1.0"

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17

    withSourcesJar()
    withJavadocJar()
}

tasks.javadoc {
    (options as StandardJavadocDocletOptions).addBooleanOption("Xdoclint:none", true)
}

tasks.test {
    useJUnitPlatform()
}

idea {
    module {
        isDownloadJavadoc = true
        isDownloadSources = true
    }
}

repositories {
    mavenCentral()
}

spotless {
    java {
        palantirJavaFormat()
    }
    groovyGradle {
        greclipse()
    }
}

val prismParser = project.path + ":lib:parser"
dependencies {
    api(project(prismParser))
    // https://github.com/incaseoftrouble/naturals-util
    api("de.tum.in", "naturals-util", "0.19.0")
    // https://picocli.info/
    api("info.picocli", "picocli", "4.7.6")
    // https://github.com/google/gson
    api("com.google.code.gson", "gson", "2.10.1")
    // https://github.com/google/guava
    api("com.google.guava", "guava", "32.1.3-jre")
    // https://mvnrepository.com/artifact/it.unimi.dsi/dsiutils
    // implementation("it.unimi.dsi:dsiutils:2.7.3")
    implementation("guru.nidi", "graphviz-java", "0.18.1")

    api("org.tinylog", "tinylog-api", "2.7.0")
    implementation("org.tinylog", "slf4j-tinylog", "2.7.0")
}

// PMD
// https://docs.gradle.org/current/dsl/org.gradle.api.plugins.quality.Pmd.html

pmd {
    toolVersion = "7.1.0" // https://pmd.github.io/
    reportsDir = project.layout.buildDirectory.dir("reports/pmd").get().asFile
    ruleSetFiles = project.layout.projectDirectory.files("config/pmd-rules.xml")
    ruleSets = listOf() // We specify all rules in rules.xml
    isConsoleOutput = false
    isIgnoreFailures = false
}

tasks.withType<Pmd> {
    reports {
        xml.required.set(false)
        html.required.set(true)
    }
}