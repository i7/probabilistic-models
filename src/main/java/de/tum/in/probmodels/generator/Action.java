package de.tum.in.probmodels.generator;

import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleMaps;
import javax.annotation.Nullable;

public record Action<S, L>(@Nullable L label, Object2DoubleMap<S> transitions) {
    public static <S, L> Action<S, L> of(Object2DoubleMap<S> transitions) {
        return of(null, transitions);
    }

    public static <S, L> Action<S, L> of(@Nullable L label, Object2DoubleMap<S> transitions) {
        return new Action<>(label, transitions);
    }

    public static <S, L> Action<S, L> of(@Nullable L label, S target) {
        return new Action<>(label, Object2DoubleMaps.singleton(target, 1.0d));
    }

    public static <S, L> Action<S, L> selfLoop(S target) {
        return of(null, Object2DoubleMaps.singleton(target, 1.0d));
    }

    public Action {
        assert !transitions.isEmpty();
    }
}
