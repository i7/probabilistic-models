package de.tum.in.probmodels.generator;

import de.tum.in.probmodels.model.Actor;
import java.util.Collection;

public record Explore<S, L>(Actor actor, Collection<Action<S, L>> actions) {
    public Explore {}
}
