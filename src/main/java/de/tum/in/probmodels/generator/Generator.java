package de.tum.in.probmodels.generator;

import com.google.common.collect.Iterables;
import java.util.Set;

/**
 * An abstract representation of an object based transition model.
 */
public interface Generator<S, L> {
    Set<S> initialStates();

    default S onlyInitialState() {
        return Iterables.getOnlyElement(initialStates());
    }

    Explore<S, L> explore(S state);

    boolean isDeterministic();
}
