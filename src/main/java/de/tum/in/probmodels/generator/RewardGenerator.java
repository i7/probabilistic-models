package de.tum.in.probmodels.generator;

import javax.annotation.Nullable;

/**
 * An abstract representation of an object based reward structure.
 */
public interface RewardGenerator<S> {
    String name();

    double stateReward(S state);

    double actionReward(S state, @Nullable Object actionIdentifier);

    default boolean mayHaveChoiceRewards() {
        return true;
    }
}
