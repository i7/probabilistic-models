package de.tum.in.probmodels.explorer;

/**
 * Specifies whether self-loops should be kept or inlined. Inlining means that if there are any other transitions
 * available, they are rescaled by the self-loop probability.
 */
public enum SelfLoopHandling {
    INLINE_LOOPS,
    KEEP_LOOPS;
}
