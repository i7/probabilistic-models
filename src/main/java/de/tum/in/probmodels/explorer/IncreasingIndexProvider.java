package de.tum.in.probmodels.explorer;

import java.util.function.IntConsumer;
import java.util.function.IntPredicate;
import java.util.function.ToIntFunction;

public class IncreasingIndexProvider<S> implements ToIntFunction<S> {
    private int stateCount;
    private final IntConsumer action;
    private final IntPredicate check;

    public IncreasingIndexProvider(IntConsumer action) {
        this(0, action, s -> true);
    }

    public IncreasingIndexProvider(int initial, IntConsumer action, IntPredicate check) {
        this.stateCount = initial;
        this.action = action;
        this.check = check;
    }

    @Override
    public int applyAsInt(S value) {
        int state = stateCount++;
        action.accept(state);
        assert check.test(state);
        return state;
    }
}
