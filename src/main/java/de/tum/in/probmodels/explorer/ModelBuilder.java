package de.tum.in.probmodels.explorer;

import static com.google.common.base.Preconditions.checkArgument;
import static de.tum.in.probmodels.explorer.SelfLoopHandling.INLINE_LOOPS;
import static de.tum.in.probmodels.explorer.SelfLoopHandling.KEEP_LOOPS;

import com.google.common.base.Stopwatch;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.naturals.unionfind.IntArrayUnionFind;
import de.tum.in.naturals.unionfind.IntUnionFind;
import de.tum.in.probmodels.generator.Action;
import de.tum.in.probmodels.generator.Explore;
import de.tum.in.probmodels.generator.Generator;
import de.tum.in.probmodels.model.HasInitialStates;
import de.tum.in.probmodels.model.MutableDenseSystem;
import de.tum.in.probmodels.model.MutableSystem;
import de.tum.in.probmodels.model.ProbabilisticHyperEdge;
import de.tum.in.probmodels.model.choice.Choice;
import de.tum.in.probmodels.model.impl.AbstractMutableDenseSystem;
import de.tum.in.probmodels.model.impl.DenseDeterministicSystem;
import de.tum.in.probmodels.model.impl.DenseNondeterministicSystem;
import de.tum.in.probmodels.model.impl.QuotientModel;
import de.tum.in.probmodels.model.index.DefaultStateBijection;
import de.tum.in.probmodels.model.index.StateIndexBijection;
import de.tum.in.probmodels.model.index.StateIndexer;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;
import org.tinylog.Logger;

public final class ModelBuilder {
    public enum Labels {
        EXPLORE_LABELS,
        IGNORE_LABELS
    }

    public enum DuplicateStateIndices {
        IGNORE_DUPLICATE_INDICES,
        UNIQUE_INDICES
    }

    public record BuiltSystem<S, L, IND extends StateIndexer<S>>(MutableSystem<Choice<L>> model, IND stateIndices) {}

    private ModelBuilder() {}

    public static <S, L> List<Choice<L>> explore(
            int stateId,
            Explore<S, L> explore,
            SelfLoopHandling selfLoops,
            Labels labels,
            ToIntFunction<S> idProvider) {
        if (explore.actions().isEmpty()) {
            return Choice.onlySingleton(stateId);
        }

        boolean hasSelfLoop = false;
        Set<L> selfLoopLabels = new HashSet<>();

        List<Choice<L>> choices = new ArrayList<>();
        for (Action<S, L> action : explore.actions()) {
            var builder =
                    switch (labels) {
                        case EXPLORE_LABELS -> Choice.labelledBuilder(action.label());
                        case IGNORE_LABELS -> Choice.<L>builder();
                    };

            boolean skippedSelfLoop = false;
            var entries = action.transitions().object2DoubleEntrySet();
            var iterator =
                    entries instanceof Object2DoubleMap.FastEntrySet<S> fast ? fast.fastIterator() : entries.iterator();
            while (iterator.hasNext()) {
                Object2DoubleMap.Entry<S> transition = iterator.next();
                int target = idProvider.applyAsInt(transition.getKey());
                double probability = transition.getDoubleValue();
                assert Double.isFinite(probability) : "Invalid probability " + probability;
                if (target == stateId && selfLoops != KEEP_LOOPS) {
                    skippedSelfLoop = true;
                } else {
                    builder.add(target, probability);
                }
            }

            if (builder.isEmpty()) {
                assert selfLoops == INLINE_LOOPS;
                hasSelfLoop = true;
                if (labels == Labels.EXPLORE_LABELS) {
                    selfLoopLabels.add(action.label());
                }
                continue;
            }

            var distribution = (skippedSelfLoop ? builder.scaled() : builder.build()).orElseThrow();
            assert selfLoops == KEEP_LOOPS || !distribution.isOnlySuccessor(stateId);
            if (distribution.isOnlySuccessor(stateId)) {
                hasSelfLoop = true;
                if (labels == Labels.EXPLORE_LABELS) {
                    selfLoopLabels.add(action.label());
                }
            } else {
                choices.add(distribution);
            }
        }

        assert (labels == Labels.IGNORE_LABELS) || (hasSelfLoop == !selfLoopLabels.isEmpty());
        if (hasSelfLoop) {
            if (labels == Labels.EXPLORE_LABELS) {
                for (L label : selfLoopLabels) {
                    choices.add(Choice.labelledSingleton(stateId, label));
                }
            } else {
                choices.add(Choice.singleton(stateId));
            }
        }
        assert !choices.isEmpty();
        assert labels != Labels.IGNORE_LABELS
                || choices.stream().map(Choice::label).allMatch(Objects::isNull);
        assert labels != Labels.EXPLORE_LABELS
                || choices.stream()
                        .map(Choice::label)
                        .collect(Collectors.toSet())
                        .equals(explore.actions().stream().map(Action::label).collect(Collectors.toSet()));
        return choices;
    }

    public static <S, L> BuiltSystem<S, L, StateIndexBijection<S>> buildDefault(Generator<S, L> generator) {
        AbstractMutableDenseSystem<Choice<L>> system =
                generator.isDeterministic() ? new DenseDeterministicSystem<>() : new DenseNondeterministicSystem<>();
        return new BuiltSystem<>(
                system,
                build(
                        system,
                        generator,
                        DuplicateStateIndices.UNIQUE_INDICES,
                        DefaultStateBijection.forEmptyDenseSystem(system),
                        (stateInd, explore, idProvider) ->
                                explore(stateInd, explore, KEEP_LOOPS, Labels.EXPLORE_LABELS, idProvider)));
    }

    public static <S, L, IND extends StateIndexer<S>> IND buildLabelled(
            MutableSystem<Choice<L>> model,
            Generator<S, L> generator,
            SelfLoopHandling selfLoops,
            DuplicateStateIndices duplicates,
            IND stateIndices) {
        return build(
                model,
                generator,
                duplicates,
                stateIndices,
                (stateInd, explore, idProvider) ->
                        explore(stateInd, explore, selfLoops, Labels.EXPLORE_LABELS, idProvider));
    }

    public static <S, L, IND extends StateIndexer<S>> IND buildUnlabelled(
            MutableSystem<Choice<L>> model,
            Generator<S, L> generator,
            SelfLoopHandling selfLoops,
            DuplicateStateIndices duplicates,
            IND stateIndices) {
        ExploreMapper<S, L, Choice<L>> mapper = (stateInd, explore, idProvider) ->
                explore(stateInd, explore, selfLoops, Labels.IGNORE_LABELS, idProvider);
        return build(model, generator, duplicates, stateIndices, mapper);
    }

    private static <S, L, IND extends StateIndexer<S>> IND build(
            MutableSystem<Choice<L>> model,
            Generator<S, L> generator,
            DuplicateStateIndices duplicates,
            IND stateIndices,
            ExploreMapper<S, L, Choice<L>> exploreMapper) {
        Stopwatch time = Stopwatch.createStarted();
        long lastLog = 0;
        int statesSinceLog = 0;

        IntSet duplicateCheck = new IntOpenHashSet();
        if (duplicates == DuplicateStateIndices.UNIQUE_INDICES) {
            //noinspection ConstantValue
            assert duplicateCheck.addAll(model.states()) || true; // Assert with side effects!
        } else {
            assert duplicates == DuplicateStateIndices.IGNORE_DUPLICATE_INDICES;
            duplicateCheck.addAll(model.states());
        }

        Queue<S> queue = new ArrayDeque<>(generator.initialStates());
        IntSet initialStates = NatBitSets.set();
        for (S initialState : generator.initialStates()) {
            boolean newState = initialStates.add(stateIndices.addIfAbsent(initialState));
            assert duplicates != DuplicateStateIndices.UNIQUE_INDICES || newState;
        }

        StateIndexer.StateIndexConsumer<S> successorIndices =
                switch (duplicates) {
                    case IGNORE_DUPLICATE_INDICES -> (t, id) -> {
                        if (duplicateCheck.add(id)) {
                            queue.add(t);
                        }
                    };
                    case UNIQUE_INDICES -> (t, id) -> {
                        assert duplicateCheck.add(id);
                        queue.add(t);
                    };
                };

        while (!queue.isEmpty()) {
            S stateObj = queue.poll();
            assert stateIndices.contains(stateObj);
            int stateInd = stateIndices.indexOf(stateObj);
            Explore<S, L> explore = generator.explore(stateObj);
            ToIntFunction<S> indexFunction = t -> stateIndices.addIfAbsent(t, successorIndices);

            var choices = exploreMapper.explore(stateInd, explore, indexFunction);
            assert !choices.isEmpty();
            model.setState(stateInd, explore.actor(), choices);

            if (Logger.isDebugEnabled()) {
                long elapsed = time.elapsed(TimeUnit.MILLISECONDS);
                if (elapsed - lastLog > 2000) {
                    int states = model.stateCount();
                    long overallRate = 1000L * states / elapsed;
                    long currentRate = 1000L * (states - statesSinceLog) / (elapsed - lastLog);
                    Logger.debug("Explored {} states ({}/s, current: {}/s)", states, overallRate, currentRate);
                    statesSinceLog = states;
                    lastLog = elapsed;
                }
            }
        }

        model.setInitialStates(initialStates);
        return stateIndices;
    }

    public static <L, T extends MutableDenseSystem<Choice<L>> & HasInitialStates>
            QuotientModel<Choice<L>, T> buildQuotient(
                    T model, Supplier<T> quotientModelConstructor, List<NatBitSet> equivalence) {
        T quotientModel = quotientModelConstructor.get();
        checkArgument(quotientModel.stateCount() == 0);
        int numStates = model.stateCount();
        IntUnionFind collapseUF = new IntArrayUnionFind(numStates);

        NatBitSet[] stateClass = new NatBitSet[numStates];
        equivalence.forEach(states -> {
            int representative = states.firstInt();
            states.forEach((int state) -> {
                collapseUF.union(representative, state);
                stateClass[state] = states;
            });
        });

        int[] stateToQuotientArray = new int[numStates];
        Arrays.fill(stateToQuotientArray, -1);
        for (int state = 0; state < numStates; state++) {
            int representative = collapseUF.find(state);
            int quotientState = stateToQuotientArray[representative];
            if (quotientState > 0) {
                stateToQuotientArray[state] = quotientState;
            } else {
                int newQuotientState = quotientModel.makeState();
                stateToQuotientArray[representative] = newQuotientState;
                stateToQuotientArray[state] = newQuotientState;
            }
        }
        assert quotientModel.stateCount() == collapseUF.componentCount();

        Multimap<Integer, L> selfLoops = HashMultimap.create();
        IntUnaryOperator stateToQuotientState = i -> stateToQuotientArray[i];
        for (int state = 0; state < numStates; state++) {
            int quotientState = stateToQuotientArray[state];
            List<Choice<L>> choices = new ArrayList<>();
            for (Choice<L> choice : model.choices(state)) {
                Choice<L> quotientDistribution =
                        choice.mapSuccessors(stateToQuotientState).orElseThrow();
                if (quotientDistribution.isOnlySuccessor(quotientState)) {
                    if (selfLoops.put(state, choice.label())) {
                        choices.add(quotientDistribution);
                    }
                } else {
                    choices.add(quotientDistribution);
                }
            }
            quotientModel.setState(state, model.control(state), choices);
        }
        model.initialStates()
                .forEach((int initialState) -> quotientModel.addInitialState(stateToQuotientArray[initialState]));

        NatBitSet[] quotientToClassArray = new NatBitSet[quotientModel.stateCount()];
        for (int state = 0; state < numStates; state++) { // NOPMD
            int quotientState = stateToQuotientArray[state];
            //noinspection ObjectEquality
            assert quotientToClassArray[quotientState] == null
                    || quotientToClassArray[quotientState] == stateClass[state]; // NOPMD
            NatBitSet clazz = stateClass[state];
            if (clazz == null) {
                quotientToClassArray[quotientState] = NatBitSets.singleton(state);
            } else {
                quotientToClassArray[quotientState] = clazz;
            }
            assert quotientToClassArray[quotientState].contains(state);
        }

        IntFunction<NatBitSet> quotientToClass = i -> quotientToClassArray[i];
        return new QuotientModel<>(quotientModel, quotientToClass, stateToQuotientState);
    }

    private interface ExploreMapper<S, L, D extends ProbabilisticHyperEdge> {
        Collection<D> explore(int stateId, Explore<S, L> explore, ToIntFunction<S> idProvider);
    }
}
