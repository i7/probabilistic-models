package de.tum.in.probmodels.explorer;

import static com.google.common.base.Preconditions.checkArgument;

import de.tum.in.probmodels.generator.RewardGenerator;
import de.tum.in.probmodels.model.RewardFunction;
import de.tum.in.probmodels.model.choice.Choice;
import de.tum.in.probmodels.util.FloatUtil;
import de.tum.in.probmodels.values.Bounds;
import it.unimi.dsi.fastutil.doubles.DoubleIterator;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleMaps;
import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;
import java.util.Collection;
import javax.annotation.Nullable;

public class RewardCache<S> implements RewardFunction {
    private final Int2DoubleMap stateRewardCache = new Int2DoubleOpenHashMap();
    private final Int2ObjectMap<Object2DoubleMap<Object>> choiceRewardCache = new Int2ObjectOpenHashMap<>();
    private final RewardGenerator<S> rewards;
    private final boolean choiceRewards;

    public RewardCache(RewardGenerator<S> rewards) {
        this.rewards = rewards;
        stateRewardCache.defaultReturnValue(Double.NaN);
        choiceRewards = rewards.mayHaveChoiceRewards();
    }

    public void exploreReward(S state, int stateId, Collection<? extends Choice<?>> choices) {
        stateRewardCache.put(stateId, rewards.stateReward(state));

        if (choiceRewards) {
            Object2DoubleMap<Object> choiceRewards = new Object2DoubleOpenHashMap<>(choices.size());
            choiceRewards.defaultReturnValue(Double.NaN);
            for (Choice<?> choice : choices) {
                double choiceReward = rewards.actionReward(state, choice.label());
                if (!Double.isNaN(choiceReward) && choiceReward != 0.0) {
                    var oldReward = choiceRewards.put(choice.label(), choiceReward);
                    checkArgument(
                            Double.isNaN(oldReward) || FloatUtil.isEqual(oldReward, choiceReward),
                            "Multiple rewards for label %s in state %s",
                            choice.label(),
                            state);
                }
            }
            if (!choiceRewards.isEmpty()) {
                choiceRewardCache.put(stateId, choiceRewards);
            }
        }
    }

    @Override
    public double stateReward(int state) {
        assert stateRewardCache.containsKey(state);
        return stateRewardCache.get(state);
    }

    @Override
    public double choiceReward(int state, @Nullable Object action) {
        assert stateRewardCache.containsKey(state);
        var choiceRewards = choiceRewardCache.get(state);
        return choiceRewards == null ? 0.0d : choiceRewards.getOrDefault(action, 0.0d);
    }

    @Override
    public boolean nonZeroReward(int state) {
        assert stateRewardCache.containsKey(state);
        if (stateRewardCache.get(state) != 0.0) {
            return true;
        }
        if (!choiceRewards) {
            return false;
        }

        Object2DoubleMap<Object> transitionRewards = choiceRewardCache.get(state);
        if (transitionRewards == null) {
            return false;
        }
        DoubleIterator iterator = transitionRewards.values().iterator();
        while (iterator.hasNext()) {
            if (iterator.nextDouble() != 0.0) {
                return true;
            }
        }
        return false;
    }

    public void setReward(int state, double stateReward) {
        stateRewardCache.put(state, stateReward);
    }

    @Override
    public boolean mayHaveChoiceRewards() {
        return choiceRewards;
    }

    @Override
    public Bounds rewardBounds(IntSet states) {
        double minimal = Double.MAX_VALUE;
        double maximal = Double.MIN_VALUE;
        var iterator = states.iterator();
        while (iterator.hasNext()) {
            int state = iterator.nextInt();
            assert stateRewardCache.containsKey(state);
            double stateReward = stateRewardCache.get(state);
            double choiceMinimal = 0.0;
            double choiceMaximal = 0.0;

            var choiceRewards = choiceRewardCache.getOrDefault(state, Object2DoubleMaps.emptyMap());
            var rewardIterator = choiceRewards.values().iterator();
            while (rewardIterator.hasNext()) {
                var next = rewardIterator.nextDouble();
                if (choiceMaximal < next) {
                    choiceMaximal = next;
                }
                if (choiceMinimal > next) {
                    choiceMinimal = next;
                }
            }
            double stateMinimal = choiceMinimal + stateReward;
            if (minimal > stateMinimal) {
                minimal = stateMinimal;
            }
            double stateMaximal = choiceMaximal + stateReward;
            if (maximal < stateMaximal) {
                maximal = stateMaximal;
            }
        }
        return Bounds.of(minimal, maximal);
    }
}
