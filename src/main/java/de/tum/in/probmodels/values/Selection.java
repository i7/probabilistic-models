package de.tum.in.probmodels.values;

import com.google.common.collect.Iterables;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;

public enum Selection {
    MAX_VALUE,
    MIN_VALUE,
    UNIQUE_VALUE;

    public Bounds select(Collection<Bounds> available) {
        return select(available, Function.identity());
    }

    public <C> Bounds select(Collection<? extends C> available, Function<C, Bounds> bounds) {
        return switch (this) {
            case MAX_VALUE -> Bounds.componentMaximum(available, bounds);
            case MIN_VALUE -> Bounds.componentMinimum(available, bounds);
            case UNIQUE_VALUE -> bounds.apply(Iterables.getOnlyElement(available));
        };
    }

    public <C> double select(Collection<? extends C> available, ToDoubleFunction<C> bounds) {
        return switch (this) {
            case MAX_VALUE -> maximum(available, bounds);
            case MIN_VALUE -> minimum(available, bounds);
            case UNIQUE_VALUE -> bounds.applyAsDouble(Iterables.getOnlyElement(available));
        };
    }

    public static <C> double maximum(Collection<? extends C> available, ToDoubleFunction<C> bounds) {
        assert !available.isEmpty();
        if (available instanceof RandomAccess && available instanceof List<? extends C> l) {
            if (l.size() == 1) {
                return bounds.applyAsDouble(l.get(0));
            }
            int size = l.size();
            double maximum = Double.NEGATIVE_INFINITY;
            //noinspection ForLoopReplaceableByForEach,DuplicatedCode
            for (int i = 0; i < size; i++) {
                double value = bounds.applyAsDouble(l.get(i));
                if (value > maximum) {
                    maximum = value;
                }
            }
            assert Double.isFinite(maximum);
            return maximum;
        }

        if (available.size() == 1) {
            return bounds.applyAsDouble(available.iterator().next());
        }
        double maximum = Double.NEGATIVE_INFINITY;
        for (C a : available) {
            double value = bounds.applyAsDouble(a);
            if (value > maximum) {
                maximum = value;
            }
        }
        assert Double.isFinite(maximum);
        return maximum;
    }

    public static <C> double minimum(Collection<? extends C> available, ToDoubleFunction<C> bounds) {
        assert !available.isEmpty();
        if (available instanceof RandomAccess && available instanceof List<? extends C> l) {
            if (l.size() == 1) {
                return bounds.applyAsDouble(l.get(0));
            }
            int size = l.size();
            double minimum = Double.POSITIVE_INFINITY;
            //noinspection ForLoopReplaceableByForEach,DuplicatedCode
            for (int i = 0; i < size; i++) {
                double value = bounds.applyAsDouble(l.get(i));
                if (value < minimum) {
                    minimum = value;
                }
            }
            assert Double.isFinite(minimum);
            return minimum;
        }

        if (available.size() == 1) {
            return bounds.applyAsDouble(available.iterator().next());
        }
        double minimum = Double.POSITIVE_INFINITY;
        for (C a : available) {
            double value = bounds.applyAsDouble(a);
            if (value < minimum) {
                minimum = value;
            }
        }
        assert Double.isFinite(minimum);
        return minimum;
    }

    public <C> Select<C> selectWithWitness(List<? extends C> available, Function<C, Bounds> bounds) {
        assert !available.isEmpty();
        assert available instanceof RandomAccess;

        int size = available.size();
        Select<C> select;
        if (size == 1) {
            C only = available.get(0);
            select = new Select<>(bounds.apply(only), only);
        } else if (size == 2) {
            C first = available.get(0);
            C second = available.get(1);

            Bounds firstB = bounds.apply(first);
            Bounds secondB = bounds.apply(second);
            double firstUpper = firstB.upperBound();
            double firstLower = firstB.lowerBound();
            double secondUpper = secondB.upperBound();
            double secondLower = secondB.lowerBound();

            select = switch (this) {
                case MAX_VALUE -> {
                    double lower = Math.max(firstLower, secondLower);
                    if (firstUpper > secondUpper) {
                        yield new Select<>(Bounds.of(lower, firstUpper), first);
                    } else {
                        yield new Select<>(Bounds.of(lower, secondUpper), second);
                    }
                }
                case MIN_VALUE -> {
                    double upper = Math.min(firstUpper, secondUpper);
                    if (firstLower < secondLower) {
                        yield new Select<>(Bounds.of(firstLower, upper), first);
                    } else {
                        yield new Select<>(Bounds.of(secondLower, upper), second);
                    }
                }
                case UNIQUE_VALUE -> throw new IllegalArgumentException("Invalid size: " + size);};
        } else {
            select =
                    switch (this) { // NOPMD
                        case MAX_VALUE -> {
                            C bestUpper = null;
                            double maxLower = Double.NEGATIVE_INFINITY;
                            double maxUpper = Double.NEGATIVE_INFINITY;
                            for (int i = 0; i < size; i++) {
                                C choice = available.get(i);
                                Bounds b = bounds.apply(choice);
                                double lowerBound = b.lowerBound();
                                if (lowerBound > maxLower) {
                                    maxLower = lowerBound;
                                }
                                double upperBound = b.upperBound();
                                if (upperBound > maxUpper) {
                                    bestUpper = choice;
                                    maxUpper = upperBound;
                                }
                            }
                            assert Double.isFinite(maxLower) && Double.isFinite(maxUpper);
                            assert bestUpper != null;
                            yield new Select<>(Bounds.of(maxLower, maxUpper), bestUpper);
                        }
                        case MIN_VALUE -> {
                            C bestLower = null;
                            double minLower = Double.POSITIVE_INFINITY;
                            double minUpper = Double.POSITIVE_INFINITY;
                            for (int i = 0; i < size; i++) {
                                C choice = available.get(i);
                                Bounds b = bounds.apply(choice);
                                double lowerBound = b.lowerBound();
                                if (lowerBound < minLower) {
                                    bestLower = choice;
                                    minLower = lowerBound;
                                }
                                double upperBound = b.upperBound();
                                if (upperBound < minUpper) {
                                    minUpper = upperBound;
                                }
                            }
                            assert Double.isFinite(minLower) && Double.isFinite(minUpper);
                            assert bestLower != null;
                            yield new Select<>(Bounds.of(minLower, minUpper), bestLower);
                        }
                        case UNIQUE_VALUE -> throw new IllegalArgumentException("Invalid size: " + size);
                    };
        }

        assert select.optimum().equals(select(available, bounds));
        return select;
    }

    public record Select<C>(Bounds optimum, C witness) {}
}
