package de.tum.in.probmodels.values;

import static de.tum.in.probmodels.util.FloatUtil.isEqual;
import static de.tum.in.probmodels.util.FloatUtil.lessOrEqual;

record RealBounds(double lowerBound, double upperBound) implements Bounds {
    @Override
    public boolean isNaN() {
        return false;
    }

    @Override
    public Bounds withUpper(double upperBound) {
        return Bounds.of(lowerBound, upperBound);
    }

    @Override
    public Bounds withLower(double lowerBound) {
        return Bounds.of(lowerBound, upperBound);
    }

    @Override
    public Bounds shrink(Bounds bounds) {
        return Bounds.of(Math.max(lowerBound, bounds.lowerBound()), Math.min(upperBound, bounds.upperBound()));
    }

    @Override
    public Bounds widen(Bounds bounds) {
        return Bounds.of(Math.min(lowerBound, bounds.lowerBound()), Math.max(upperBound, bounds.upperBound()));
    }

    @Override
    public Bounds smallerUpper(double upperBound) {
        assert lessOrEqual(lowerBound, upperBound);
        return this.upperBound <= upperBound ? this : withUpper(upperBound);
    }

    @Override
    public Bounds largerLower(double lowerBound) {
        assert lessOrEqual(lowerBound, upperBound);
        return this.lowerBound >= lowerBound ? this : withLower(lowerBound);
    }

    @Override
    public String toString() {
        return isEqual(lowerBound(), upperBound())
                ? String.format("=%.5g", average())
                : String.format("[%.5g,%.5g(%.3e)]", lowerBound(), upperBound(), difference());
    }
}
