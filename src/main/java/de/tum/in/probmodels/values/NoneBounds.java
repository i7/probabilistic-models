package de.tum.in.probmodels.values;

public final class NoneBounds implements Bounds {
    @Override
    public boolean isNaN() {
        return true;
    }

    @Override
    public double lowerBound() {
        throw new IllegalArgumentException("NaN");
    }

    @Override
    public double upperBound() {
        throw new IllegalArgumentException("NaN");
    }

    @Override
    public Bounds shrink(Bounds bounds) {
        throw new IllegalArgumentException("NaN");
    }

    @Override
    public Bounds widen(Bounds bounds) {
        throw new IllegalArgumentException("NaN");
    }

    @Override
    public Bounds withUpper(double upperBound) {
        throw new IllegalArgumentException("NaN");
    }

    @Override
    public Bounds withLower(double lowerBound) {
        throw new IllegalArgumentException("NaN");
    }

    @Override
    public Bounds smallerUpper(double upperBound) {
        throw new IllegalArgumentException("NaN");
    }

    @Override
    public Bounds largerLower(double lowerBound) {
        throw new IllegalArgumentException("NaN");
    }
}
