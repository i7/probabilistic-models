package de.tum.in.probmodels.values;

final class BoundsSingletons {
    static final Bounds ZERO = new RealBounds(0.0d, 0.0d);
    static final Bounds UNKNOWN_REACH = new RealBounds(0.0d, 1.0d);
    static final Bounds ONE = new RealBounds(1.0d, 1.0d);
    static final Bounds UNKNOWN = new RealBounds(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);

    private BoundsSingletons() {
        // Empty
    }
}
