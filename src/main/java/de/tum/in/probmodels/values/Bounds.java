package de.tum.in.probmodels.values;

import static de.tum.in.probmodels.util.FloatUtil.isEqual;
import static de.tum.in.probmodels.util.FloatUtil.isOne;
import static de.tum.in.probmodels.util.FloatUtil.isZero;
import static de.tum.in.probmodels.util.FloatUtil.lessOrEqual;

import de.tum.in.probmodels.util.FloatUtil;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;
import java.util.function.Function;

/**
 * A utility class to represent lower and upper bounds.
 */
public sealed interface Bounds permits NoneBounds, RealBounds {
    static Bounds of(double value) {
        return new RealBounds(value, value);
    }

    static Bounds of(double lower, double upper) {
        if (Double.isNaN(lower) || Double.isNaN(upper)) {
            return new NoneBounds();
        }
        if (isEqual(lower, upper)) {
            return of((lower + upper) / 2);
        }
        if (lower > upper) {
            assert isEqual(lower, upper, FloatUtil.WEAK_EPS) : "%f > %f (%g)".formatted(lower, upper, lower - upper);
            return of((lower + upper) / 2);
        }
        return new RealBounds(lower, upper);
    }

    static Bounds reach(double value) {
        assert 0.0d <= value && value <= 1.0d;

        if (isZero(value)) {
            return zero();
        }
        if (isOne(value)) {
            return one();
        }
        return new RealBounds(value, value);
    }

    static Bounds reach(double lower, double upper) {
        if (isEqual(lower, upper)) {
            return reach((lower + upper) / 2);
        }
        assert lower <= upper;
        assert lessOrEqual(0.0, lower) && lessOrEqual(upper, 1.0);
        if (1.0d <= lower) {
            return one();
        }
        if (upper <= 0.0d) {
            return zero();
        }
        if (lower <= 0.0d && 1.0d <= upper) {
            return unknownReach();
        }
        return new RealBounds(lower, upper);
    }

    static Bounds unknown() {
        return BoundsSingletons.UNKNOWN;
    }

    static Bounds zero() {
        return BoundsSingletons.ZERO;
    }

    static Bounds one() {
        return BoundsSingletons.ONE;
    }

    static Bounds unknownReach() {
        return BoundsSingletons.UNKNOWN_REACH;
    }

    static <C> Bounds componentMinimum(Collection<? extends C> available, Function<C, Bounds> bounds) {
        assert !available.isEmpty();
        if (available instanceof RandomAccess && available instanceof List<? extends C> l) {
            if (l.size() == 1) {
                return bounds.apply(l.get(0));
            }
            int size = l.size();
            double minLower = Double.POSITIVE_INFINITY;
            double minUpper = Double.POSITIVE_INFINITY;
            //noinspection ForLoopReplaceableByForEach,DuplicatedCode
            for (int i = 0; i < size; i++) {
                var b = bounds.apply(l.get(i));
                double lowerBound = b.lowerBound();
                if (lowerBound < minLower) {
                    minLower = lowerBound;
                }
                double upperBound = b.upperBound();
                if (upperBound < minUpper) {
                    minUpper = upperBound;
                }
            }
            assert Double.isFinite(minLower) && Double.isFinite(minUpper);
            return of(minLower, minUpper);
        }

        if (available.size() == 1) {
            return bounds.apply(available.iterator().next());
        }
        double minLower = Double.POSITIVE_INFINITY;
        double minUpper = Double.POSITIVE_INFINITY;
        //noinspection DuplicatedCode
        for (C a : available) {
            var b = bounds.apply(a);
            double lowerBound = b.lowerBound();
            if (lowerBound < minLower) {
                minLower = lowerBound;
            }
            double upperBound = b.upperBound();
            if (upperBound < minUpper) {
                minUpper = upperBound;
            }
        }
        assert Double.isFinite(minLower) && Double.isFinite(minUpper);
        return of(minLower, minUpper);
    }

    static <C> Bounds componentMaximum(Collection<? extends C> available, Function<C, Bounds> bounds) {
        assert !available.isEmpty();
        if (available instanceof RandomAccess && available instanceof List<? extends C> l) {
            if (l.size() == 1) {
                return bounds.apply(l.get(0));
            }
            int size = l.size();
            double maxLower = Double.NEGATIVE_INFINITY;
            double maxUpper = Double.NEGATIVE_INFINITY;
            //noinspection ForLoopReplaceableByForEach,DuplicatedCode
            for (int i = 0; i < size; i++) {
                var b = bounds.apply(l.get(i));
                double lowerBound = b.lowerBound();
                if (lowerBound > maxLower) {
                    maxLower = lowerBound;
                }
                double upperBound = b.upperBound();
                if (upperBound > maxUpper) {
                    maxUpper = upperBound;
                }
            }
            assert Double.isFinite(maxLower) && Double.isFinite(maxUpper);
            return of(maxLower, maxUpper);
        }

        if (available.size() == 1) {
            return bounds.apply(available.iterator().next());
        }
        double maxLower = Double.NEGATIVE_INFINITY;
        double maxUpper = Double.NEGATIVE_INFINITY;
        //noinspection DuplicatedCode
        for (C a : available) {
            var b = bounds.apply(a);
            double lowerBound = b.lowerBound();
            if (lowerBound > maxLower) {
                maxLower = lowerBound;
            }
            double upperBound = b.upperBound();
            if (upperBound > maxUpper) {
                maxUpper = upperBound;
            }
        }
        assert Double.isFinite(maxLower) && Double.isFinite(maxUpper);
        return of(maxLower, maxUpper);
    }

    double lowerBound();

    double upperBound();

    boolean isNaN();

    default double difference() {
        return upperBound() - lowerBound();
    }

    default double average() {
        return (lowerBound() + upperBound()) / 2;
    }

    default boolean contains(Bounds other) {
        return lessOrEqual(lowerBound(), other.lowerBound()) && lessOrEqual(other.upperBound(), upperBound());
    }

    default boolean contains(Bounds other, double eps) {
        return lessOrEqual(lowerBound(), other.lowerBound(), eps) && lessOrEqual(other.upperBound(), upperBound(), eps);
    }

    default boolean containsExact(Bounds other) {
        return lowerBound() <= other.lowerBound() && other.upperBound() <= upperBound();
    }

    default boolean contains(double v) {
        return lowerBound() <= v && v <= upperBound();
    }

    default boolean equalsUpTo(Bounds other) {
        return isEqual(lowerBound(), other.lowerBound()) && isEqual(upperBound(), other.upperBound());
    }

    default boolean equalsUpTo(Bounds other, double precision) {
        return isEqual(lowerBound(), other.lowerBound(), precision)
                && isEqual(upperBound(), other.upperBound(), precision);
    }

    /**
     * Returns the intersection of {@code this} with the given bounds.
     */
    Bounds shrink(Bounds bounds);

    /**
     * Returns the union of {@code this} with the given bounds.
     */
    Bounds widen(Bounds bounds);

    Bounds withUpper(double upperBound);

    Bounds withLower(double lowerBound);

    Bounds smallerUpper(double upperBound);

    Bounds largerLower(double lowerBound);
}
