package de.tum.in.probmodels.output;

import com.google.gson.annotations.SerializedName;
import java.time.Duration;

public interface ComponentsStatistics {
    @SerializedName("count")
    long count();

    @SerializedName("non_trivial")
    long nonTrivialCount();

    @SerializedName("min_size")
    long minimumSize();

    @SerializedName("max_size")
    long maximumSize();

    @SerializedName("sum_size")
    long sumSize();

    @SerializedName("avg_size")
    double averageSize();

    record Simple(
            long count, long nonTrivialCount, long minimumSize, long maximumSize, long sumSize, double averageSize)
            implements ComponentsStatistics {}

    record Timed(
            long count,
            long nonTrivialCount,
            long minimumSize,
            long maximumSize,
            long sumSize,
            double averageSize,
            @SerializedName("time") Duration analysisTime)
            implements ComponentsStatistics {}
}
