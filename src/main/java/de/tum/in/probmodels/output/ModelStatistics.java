package de.tum.in.probmodels.output;

import com.google.gson.annotations.SerializedName;
import java.time.Duration;
import javax.annotation.Nullable;

public interface ModelStatistics {
    @SerializedName("states")
    long states();

    @SerializedName("choices")
    long choices();

    @SerializedName("transitions")
    long transitions();

    @SerializedName("components")
    @Nullable
    ComponentsStatistics components();

    ModelStatistics withComponents(ComponentsStatistics components);

    record Simple(long states, long choices, long transitions, @Nullable ComponentsStatistics components)
            implements ModelStatistics {
        @Override
        public Simple withComponents(ComponentsStatistics components) {
            return new Simple(states, choices, transitions, components);
        }
    }

    record Timed(
            long states,
            long choices,
            long transitions,
            @SerializedName("time") Duration constructionDuration,
            @Nullable ComponentsStatistics components)
            implements ModelStatistics {
        @Override
        public Timed withComponents(ComponentsStatistics components) {
            return new Timed(states, choices, transitions, constructionDuration, components);
        }
    }
}
