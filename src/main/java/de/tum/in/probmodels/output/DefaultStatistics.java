package de.tum.in.probmodels.output;

import com.google.common.base.Stopwatch;
import de.tum.in.probmodels.graph.Component;
import de.tum.in.probmodels.graph.ComponentAnalyser;
import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.Hypergraph;
import de.tum.in.probmodels.model.impl.RestrictedGraphView;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.time.Duration;
import java.util.Collection;
import java.util.IntSummaryStatistics;
import java.util.List;
import javax.annotation.Nullable;

public final class DefaultStatistics {
    private DefaultStatistics() {}

    public static <H extends HyperEdge> ModelStatistics statistics(
            Hypergraph<H> system, IntSet states, boolean analyseComponents) {
        long choiceCount = 0L;
        long transitionCount = 0L;
        var iterator = states.intIterator();
        while (iterator.hasNext()) {
            int state = iterator.nextInt();
            var choices = system.choices(state);
            choiceCount += choices.size();
            transitionCount +=
                    choices.stream().mapToInt(HyperEdge::successorCount).sum();
        }

        @Nullable ComponentsStatistics componentsStatistics;
        if (analyseComponents) {
            Stopwatch componentTimer = Stopwatch.createStarted();
            var componentAnalyser = ComponentAnalyser.defaultAnalyser(system);
            List<IntSet> components =
                    componentAnalyser.findComponents(RestrictedGraphView.of(system, states), states).stream()
                            .map(Component::states)
                            .toList();
            componentsStatistics = extract(components, componentTimer.elapsed());
        } else {
            componentsStatistics = null;
        }
        return new ModelStatistics.Simple(states.size(), choiceCount, transitionCount, componentsStatistics);
    }

    public static ComponentsStatistics extract(Collection<? extends IntSet> components, Duration duration) {
        if (components.isEmpty()) {
            return new ComponentsStatistics.Timed(0, 0, 0, 0, 0, 0.0, duration);
        }
        IntSummaryStatistics summaryStatistics =
                components.stream().mapToInt(IntSet::size).summaryStatistics();
        var nonTrivialCount = components.stream().filter(c -> c.size() > 1).count();
        return new ComponentsStatistics.Timed(
                summaryStatistics.getCount(),
                nonTrivialCount,
                summaryStatistics.getMin(),
                summaryStatistics.getMax(),
                summaryStatistics.getSum(),
                summaryStatistics.getAverage(),
                duration);
    }

    public static ComponentsStatistics extract(Collection<? extends IntSet> components) {
        if (components.isEmpty()) {
            return new ComponentsStatistics.Simple(0, 0, 0, 0, 0, 0.0);
        }
        IntSummaryStatistics summaryStatistics =
                components.stream().mapToInt(IntSet::size).summaryStatistics();
        var nonTrivialCount = components.stream().filter(c -> c.size() > 1).count();
        return new ComponentsStatistics.Simple(
                summaryStatistics.getCount(),
                nonTrivialCount,
                summaryStatistics.getMin(),
                summaryStatistics.getMax(),
                summaryStatistics.getSum(),
                summaryStatistics.getAverage());
    }
}
