package de.tum.in.probmodels.util;

import com.google.common.collect.Iterators;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.SplittableRandom;
import java.util.function.IntToDoubleFunction;
import java.util.function.ToDoubleFunction;
import javax.annotation.Nullable;

public final class Sample {
    /*
     * The sample() methods deliberately do not use stable summation: If probabilities significantly differ
     * dramatically, it is fine if smaller probabilities are skewed due to loss of precision. This would only cause
     * issues if we have *many* values with similar probabilities (then, eventually, the total sum would be several
     * orders of magnitude larger and "swallow" the probabilities of trailing successors).
     */
    // TODO one could employ pairwise summation, which uses the same amount of arithmetic operations as naive summation

    public static final SplittableRandom random;

    static {
        String seed = System.getenv("SEED");
        random = seed == null ? new SplittableRandom() : new SplittableRandom(Long.parseLong(seed));
        // random = seed == null ? new XorShift1024StarPhiRandom() : new
        // XorShift1024StarPhiRandom(Long.parseLong(seed));
    }

    private Sample() {
        // Empty
    }

    public static double nextDouble() {
        return random.nextDouble();
    }

    public static int sample(int max) {
        return random.nextInt(max);
    }

    public static int sampleProb(double[] probabilities) {
        assert probabilities.length > 0;
        assert FloatUtil.isEqualRel(FloatUtil.KahanSum.of(probabilities), 1.0);
        assert Arrays.stream(probabilities).noneMatch(FloatUtil::isZero);

        if (probabilities.length == 1) {
            return 0;
        }
        if (probabilities.length == 2) {
            return probabilities[0] < random.nextDouble() ? 1 : 0;
        }
        if (probabilities.length == 3) {
            double sample = random.nextDouble();
            if (probabilities[0] >= sample) {
                return 0;
            }
            return probabilities[0] + probabilities[1] >= sample ? 1 : 2;
        }

        double sample = random.nextDouble();
        for (int i = 0; i < probabilities.length; i++) {
            sample -= probabilities[i];
            if (sample <= 0) {
                assert probabilities[i] > 0.0;
                return i;
            }
        }
        assert FloatUtil.isZero(sample);
        return probabilities.length - 1;
    }

    public static int sample(double[] values) {
        return sample(values, values.length);
    }

    public static int sample(double[] values, int max) {
        assert max <= values.length;
        assert Arrays.stream(values).allMatch(d -> d >= 0.0)
                : String.format("Got negative values: %s", Arrays.toString(values));

        if (max == 0) {
            return -1;
        }
        if (max == 1) {
            return values[0] == 0.0d ? -1 : 0;
        }

        double value = values[0];
        double sum = value;
        boolean allEqual = true;
        for (int i = 1; i < max; i++) {
            double v = values[i];
            allEqual = allEqual && FloatUtil.isEqualRel(value, v);
            sum += v;
        }
        if (allEqual) {
            return value == 0.0 ? -1 : sample(max);
        }

        // TODO Trade space for time: Store prefix sums and search sampledValue with binary search
        //      But probably only for distributions > 12 (or something like that)

        // Sample a random value in [0, sum)
        double sampledValue = random.nextDouble() * sum;
        for (int i = 0; i < max; i++) {
            sampledValue -= values[i];
            if (sampledValue <= 0) {
                assert values[i] > 0.0;
                return i;
            }
        }
        assert FloatUtil.isZero(sampledValue);
        return max - 1;
    }

    public static int sample(Int2DoubleMap distribution) {
        assert FloatUtil.isEqualRel(distribution.values().doubleStream().sum(), 1.0);
        if (distribution.isEmpty()) {
            return -1;
        }
        if (distribution.size() == 1) {
            return distribution.keySet().iterator().nextInt();
        }

        double sampledValue = random.nextDouble();
        Int2DoubleMap.Entry lastEntry = null;
        for (Int2DoubleMap.Entry entry : distribution.int2DoubleEntrySet()) {
            sampledValue -= entry.getDoubleValue();
            if (sampledValue <= 0) {
                return entry.getIntKey();
            }
            lastEntry = entry;
        }
        assert lastEntry != null;
        assert FloatUtil.isZero(sampledValue);
        return lastEntry.getIntKey();
    }

    public static int sampleUniform(IntList values) {
        return sampleUniform(values, values.size());
    }

    public static int sampleUniform(IntList values, int max) {
        assert max <= values.size();
        if (max == 0) {
            return -1;
        }
        if (max == 1) {
            return values.getInt(0);
        }
        if (max == 2) {
            return random.nextBoolean() ? values.getInt(0) : values.getInt(1);
        }
        return values.getInt(random.nextInt(max));
    }

    public static int sampleUniform(IntCollection values) {
        if (values instanceof IntList) {
            return sampleUniform((IntList) values);
        }
        int size = values.size();
        if (size == 0) {
            return -1;
        }
        IntIterator iterator = values.iterator();
        if (size == 1) {
            return iterator.nextInt();
        }
        int index = random.nextInt(size);
        iterator.skip(index);
        return iterator.nextInt();
    }

    public static int sampleUniform(int[] values) {
        return sampleUniform(values, values.length);
    }

    public static int sampleUniform(int[] values, int max) {
        assert max <= values.length;
        if (max == 0) {
            return -1;
        }
        if (max == 1) {
            return values[0];
        }
        if (max == 2) {
            return random.nextBoolean() ? values[0] : values[1];
        }
        return values[random.nextInt(max)];
    }

    @Nullable
    public static <C> C sampleUniform(C[] values, int max) {
        assert max <= values.length;
        if (max == 0) {
            return null;
        }
        if (max == 1) {
            return values[0];
        }
        if (max == 2) {
            return random.nextBoolean() ? values[0] : values[1];
        }
        return values[random.nextInt(max)];
    }

    public static <T> Optional<T> sampleUniform(List<? extends T> values) {
        int size = values.size();
        if (size == 0) {
            return Optional.empty();
        }
        return Optional.of(size == 1 ? values.get(0) : values.get(random.nextInt(size)));
    }

    public static <T> Optional<T> sampleUniform(Collection<? extends T> values) {
        if (values instanceof List<?>) {
            return sampleUniform((List<? extends T>) values);
        }
        int size = values.size();
        if (size == 0) {
            return Optional.empty();
        }
        Iterator<? extends T> iterator = values.iterator();
        if (size == 1) {
            return Optional.of(iterator.next());
        }
        int index = random.nextInt(size);
        return Optional.of(Iterators.get(iterator, index));
    }

    public static int sampleWeighted(IntCollection values, IntToDoubleFunction weights) {
        int size = values.size();
        if (size == 0) {
            return -1;
        }
        if (size == 1) {
            int value = values.iterator().nextInt();
            return weights.applyAsDouble(value) == 0.0 ? -1 : value;
        }

        FloatUtil.KahanSum sum = new FloatUtil.KahanSum();
        double[] weightArray = new double[size];
        IntIterator iterator = values.iterator();
        int index = 0;
        while (iterator.hasNext()) {
            double weight = weights.applyAsDouble(iterator.nextInt());
            weightArray[index] = weight;
            sum.add(weight);
            index += 1;
        }
        double sampledValue = random.nextDouble() * sum.get();
        double partialSum = 0.0d;

        IntIterator resultIterator = values.iterator();
        for (int i = 0; i < size; i++) {
            partialSum += weightArray[i];
            if (partialSum >= sampledValue) {
                resultIterator.skip(i);
                return resultIterator.nextInt();
            }
        }

        throw new AssertionError("Not sampling any value");
    }

    public static <T> Optional<T> sampleWeighted(Collection<? extends T> values, ToDoubleFunction<T> weights) {
        int size = values.size();
        if (size == 0) {
            return Optional.empty();
        }
        if (size == 1) {
            T value = values.iterator().next();
            return weights.applyAsDouble(value) == 0.0 ? Optional.empty() : Optional.of(value);
        }

        FloatUtil.KahanSum sum = new FloatUtil.KahanSum();
        double[] weightArray = new double[size];
        Iterator<? extends T> iterator = values.iterator();
        int index = 0;
        while (iterator.hasNext()) {
            double weight = weights.applyAsDouble(iterator.next());
            weightArray[index] = weight;
            sum.add(weight);
            index += 1;
        }

        FloatUtil.KahanSum sampledValue = new FloatUtil.KahanSum(random.nextDouble() * sum.get());
        Iterator<? extends T> resultIterator = values.iterator();
        for (int i = 0; i < size; i++) {
            var next = resultIterator.next();
            assert FloatUtil.isEqualRel(weights.applyAsDouble(next), weightArray[i]);
            if (sampledValue.add(-weightArray[i]) <= 0.0) {
                return Optional.of(next);
            }
        }

        throw new AssertionError("Not sampling any value");
    }

    @Nullable
    public static <C> C getOptimal(List<? extends C> objects, ToDoubleFunction<C> scoreFunction) {
        return getOptimal(objects, scoreFunction, false);
    }

    @Nullable
    public static <C> C getOptimal(List<? extends C> objects, ToDoubleFunction<C> scoreFunction, boolean safe) {
        // Note: We cannot do early stopping here, since we need to sample uniformly among *all options* with optimal
        // value, greedily choosing the first won't work
        assert !objects.isEmpty();
        int count = objects.size();
        if (count == 1) {
            C object = objects.get(0);
            if (safe) {
                assert !Double.isNaN(scoreFunction.applyAsDouble(object));
                return object;
            }
            return Double.isNaN(scoreFunction.applyAsDouble(object)) ? null : object;
        }
        if (count == 2) {
            C first = objects.get(0);
            double firstScore = scoreFunction.applyAsDouble(first);
            C second = objects.get(1);
            double secondScore = scoreFunction.applyAsDouble(second);

            if (safe) {
                assert !Double.isNaN(firstScore) && !Double.isNaN(secondScore);
            } else { // NOPMD
                if (Double.isNaN(firstScore)) { // NOPMD
                    return Double.isNaN(secondScore) ? null : second;
                }
                if (Double.isNaN(secondScore)) { // NOPMD
                    return first;
                }
            }
            if (FloatUtil.isEqual(firstScore, secondScore)) {
                return random.nextBoolean() ? first : second;
            }
            return firstScore > secondScore ? first : second;
        }

        int i = 0;
        double currentBestScore = Double.NaN;
        double smallestBestScore = Double.NaN;
        C firstObject = null;
        while (i < count) {
            C object = objects.get(i);
            i += 1;

            double score = scoreFunction.applyAsDouble(object);
            if (safe) {
                assert !Double.isNaN(score);
            } else {
                if (Double.isNaN(score)) {
                    continue;
                }
            }
            firstObject = object;
            currentBestScore = score;
            smallestBestScore = score;
            break; // NOPMD
        }
        if (firstObject == null) {
            assert !safe;
            return null;
        }
        int stackSize = Math.min(count - i + 1, 8);
        int stackTop = 1;

        @SuppressWarnings("unchecked")
        C[] bestObject = (C[]) new Object[stackSize];
        double[] bestScores = new double[stackSize];
        bestObject[0] = firstObject;
        bestScores[0] = currentBestScore;
        assert !Double.isNaN(currentBestScore) && !Double.isNaN(smallestBestScore);

        while (i < count) {
            assert stackTop > 0;

            C object = objects.get(i);
            i += 1;

            double score = scoreFunction.applyAsDouble(object);
            if (!safe && Double.isNaN(score)) {
                continue;
            }
            if (currentBestScore < score) {
                if (FloatUtil.lessOrEqual(score, currentBestScore)) {
                    if (stackTop == stackSize) {
                        stackSize *= 2;
                        bestObject = Arrays.copyOf(bestObject, stackSize);
                        bestScores = Arrays.copyOf(bestScores, stackSize);
                    }
                    bestObject[stackTop] = object;
                    bestScores[stackTop] = score;
                    stackTop += 1;
                    if (score < smallestBestScore) {
                        smallestBestScore = score;
                    }
                } else {
                    bestObject[0] = object;
                    bestScores[0] = score;
                    stackTop = 1;
                    smallestBestScore = score;
                }
                currentBestScore = score;
            } else {
                if (FloatUtil.isEqual(score, currentBestScore)) {
                    if (stackTop == stackSize) {
                        stackSize *= 2;
                        bestObject = Arrays.copyOf(bestObject, stackSize);
                        bestScores = Arrays.copyOf(bestScores, stackSize);
                    }
                    bestObject[stackTop] = object;
                    bestScores[stackTop] = score;
                    stackTop += 1;
                    if (score < smallestBestScore) {
                        smallestBestScore = score;
                    }
                }
            }
        }
        double smallestScore = smallestBestScore;
        double bestScore = currentBestScore;

        assert Arrays.stream(bestScores, 0, stackTop).min().orElseThrow() == smallestBestScore;
        assert Arrays.stream(bestObject, 0, stackTop)
                .toList()
                .equals(objects.stream()
                        .filter(o -> {
                            double score = scoreFunction.applyAsDouble(o);
                            return smallestScore <= score && score <= bestScore;
                        })
                        .toList());
        assert stackTop > 0;

        C choice;
        if (stackTop == 1) {
            choice = bestObject[0];
        } else if (FloatUtil.isEqual(smallestBestScore, bestScore)) {
            choice = sampleUniform(bestObject, stackTop);
        } else {
            // We have an upper bound on the best objects, compute precisely now
            int cleanIndex = 0;
            for (int index = 0; index < stackTop; index++) {
                if (FloatUtil.lessOrEqual(bestScore, bestScores[index])) {
                    assert cleanIndex <= index;
                    if (cleanIndex < index) {
                        bestObject[cleanIndex] = bestObject[index];
                    }
                    cleanIndex += 1;
                }
            }
            assert 0 < cleanIndex && cleanIndex < stackTop;
            assert Arrays.stream(bestObject, 0, cleanIndex)
                    .toList()
                    .equals(objects.stream()
                            .filter(o -> FloatUtil.isEqual(scoreFunction.applyAsDouble(o), bestScore))
                            .toList());
            choice = sampleUniform(bestObject, cleanIndex);
        }

        // There has to be a witness for the bestValue
        assert choice != null;
        assert FloatUtil.isEqual(currentBestScore, scoreFunction.applyAsDouble(choice));
        return choice;
    }

    public static long nextLong(long limit) {
        return random.nextLong(limit);
    }
}
