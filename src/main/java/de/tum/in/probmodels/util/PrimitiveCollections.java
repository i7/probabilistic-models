package de.tum.in.probmodels.util;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.naturals.unionfind.IntUnionFind;
import it.unimi.dsi.fastutil.ints.Int2ObjectAVLTreeMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntList;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public final class PrimitiveCollections {
    private PrimitiveCollections() {}

    public static int onlyElement(IntCollection collection) {
        if (collection.size() != 1) {
            throw new NoSuchElementException();
        }
        return collection instanceof IntList list
                ? list.getInt(0)
                : collection.iterator().nextInt();
    }

    public static List<NatBitSet> unionFindPartition(IntIterator iterator, IntUnionFind unionFind) {
        Int2ObjectMap<NatBitSet> unionRootMap = new Int2ObjectAVLTreeMap<>();
        List<NatBitSet> foundBitSets = new ArrayList<>();
        while (iterator.hasNext()) {
            int state = iterator.nextInt();
            unionRootMap
                    .computeIfAbsent(unionFind.find(state), s -> {
                        NatBitSet result = NatBitSets.set();
                        foundBitSets.add(result);
                        return result;
                    })
                    .set(state);
        }
        return foundBitSets;
    }
}
