package de.tum.in.probmodels.util;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.Collection;
import java.util.function.Function;

public final class NatBitSets2 {
    private NatBitSets2() {}

    public static <S> IntSet lazyUnion(Collection<S> sets, Function<S, IntSet> map) {
        if (sets.size() == 1) {
            return map.apply(sets.iterator().next());
        }
        return union(sets, map);
    }

    public static <S> NatBitSet union(Collection<S> sets, Function<S, IntSet> map) {
        if (sets.isEmpty()) {
            return NatBitSets.emptySet();
        }
        var iterator = sets.iterator();
        var union = NatBitSets.ensureModifiable(NatBitSets.copyOf(map.apply(iterator.next())));
        while (iterator.hasNext()) {
            union.or(map.apply(iterator.next()));
        }
        return union;
    }

    public static NatBitSet lazyIntersection(NatBitSet one, NatBitSet other) {
        if (other.containsAll(one)) {
            return one;
        }
        NatBitSet copy = NatBitSets.modifiableCopyOf(one);
        copy.retainAll(other);
        return copy;
    }
}
