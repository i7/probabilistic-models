package de.tum.in.probmodels.util;

public record Precision(double bound, boolean relativeError) {}
