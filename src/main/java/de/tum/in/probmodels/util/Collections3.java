package de.tum.in.probmodels.util;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Predicate;
import javax.annotation.Nullable;

public final class Collections3 { // NOPMD
    private Collections3() {}

    @Nullable
    public static <C> List<C> copyOnWriteFilter(List<C> list, Predicate<? super C> filter) {
        List<C> filtered = null;
        ListIterator<C> iterator = list.listIterator();
        while (iterator.hasNext()) {
            int index = iterator.nextIndex();
            C element = iterator.next();
            if (filter.test(element)) {
                if (filtered != null) {
                    filtered.add(element);
                }
            } else {
                if (filtered == null) {
                    filtered = new ArrayList<>(list.subList(0, index));
                }
            }
        }
        return filtered;
    }

    public static <C> List<C> copyFiltered(List<C> list, Predicate<C> filter) {
        if (list.isEmpty()) {
            return List.of();
        }
        List<C> filtered = new ArrayList<>();
        for (C element : list) {
            if (filter.test(element)) {
                filtered.add(element);
            }
        }
        return filtered.isEmpty() ? List.of() : filtered;
    }
}
