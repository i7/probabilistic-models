package de.tum.in.probmodels.util;

import it.unimi.dsi.fastutil.doubles.DoubleIterator;
import java.util.Arrays;

/**
 * Utility methods, mostly for dealing with floating point definitions.
 */
public final class FloatUtil {
    public static final double STRONG_EPS = 1.0e-15;
    public static final double WEAK_EPS = 1.0e-6;

    private FloatUtil() {
        // Empty
    }

    public static boolean isEqual(double d1, double d2) {
        return isZero(d1 - d2);
    }

    public static boolean isEqual(double d1, double d2, double eps) {
        return isZero(d1 - d2, eps);
    }

    public static boolean isEqualNonZero(double d1, double d2) {
        return d1 == 0.0 ? d2 == 0.0 : isZero(d1 - d2);
    }

    public static boolean isEqualRel(double approx, double reference) {
        return reference == 0.0 ? approx == 0.0 : isOne(approx / reference);
    }

    public static boolean isOne(double d) {
        return isOne(d, STRONG_EPS);
    }

    public static boolean isOne(double d, double eps) {
        assert !Double.isNaN(d);
        return 1.0 - eps <= d && d <= 1.0 + eps;
    }

    public static boolean isZero(double d) {
        assert !Double.isNaN(d);
        return isZero(d, STRONG_EPS);
    }

    public static boolean isZero(double d, double eps) {
        assert !Double.isNaN(d);
        return -eps <= d && d <= eps;
    }

    public static boolean lessOrEqual(double d1, double d2) {
        return lessOrEqual(d1, d2, STRONG_EPS);
    }

    public static boolean lessOrEqual(double d1, double d2, double eps) {
        assert !Double.isNaN(d1) && !Double.isNaN(d2);
        return d1 - d2 <= eps;
    }

    public static boolean strictlyLess(double d1, double d2) {
        return strictlyLess(d1, d2, STRONG_EPS);
    }

    public static boolean strictlyLess(double d1, double d2, double eps) {
        assert !Double.isNaN(d1) && !Double.isNaN(d2);
        return d1 - d2 <= -eps;
    }

    public static final class KahanSum {
        private double x;
        private double s;

        public KahanSum(double v) {
            this.s = v;
            this.x = 0.0;
        }

        public KahanSum() {
            this(0.0);
        }

        public static double of(double a, double b, double c) {
            double ab = a + b;
            double compensation = (ab - a) - b;
            double sum = ab + c - compensation;
            assert isEqual(Arrays.stream(new double[] {a, b, c}).sum(), sum, WEAK_EPS);
            return sum;
        }

        public static double of(double... values) {
            return of(values, values.length);
        }

        public static double of(DoubleIterator iterator) {
            if (!iterator.hasNext()) {
                return 0.0;
            }
            double s = iterator.nextDouble();
            double x = 0.0;
            while (iterator.hasNext()) {
                double y = iterator.nextDouble() - x;
                double z = s + y;
                x = (z - s) - y;
                s = z;
            }
            return s;
        }

        public static double of(double[] values, int to) {
            assert to <= values.length;

            if (to == 0) {
                return 0.0;
            }
            if (to == 1) {
                return values[0];
            }
            if (to == 2) {
                return values[0] + values[1];
            }
            if (to == 3) {
                return of(values[0], values[1], values[2]);
            }
            double sum = values[0];
            double x = 0.0;
            for (int i = 1; i < to; i++) {
                double v = values[i];
                if (v == 0.0) {
                    continue;
                }
                double y = v - x;
                double z = sum + y;
                x = (z - sum) - y;
                sum = z;
            }
            assert isEqual(sum, Arrays.stream(values, 0, to).sum(), WEAK_EPS);
            return sum;
        }

        public double add(double v) {
            assert Double.isFinite(v) : v;
            if (v == 0.0) {
                return s;
            }
            double y = v - x;
            double z = s + y;
            x = (z - s) - y;
            s = z;
            return s;
        }

        public double add(double... v) {
            if (v.length == 0) {
                return s;
            }
            if (v.length == 1) {
                return add(v[0]);
            }
            double x = this.x;
            double s = this.s;
            for (double value : v) {
                assert Double.isFinite(value) : Arrays.toString(v);
                double y = value - x;
                double z = s + y;
                x = (z - s) - y;
                s = z;
            }
            this.x = x;
            this.s = s;
            return s;
        }

        public double get() {
            return s;
        }

        @Override
        public String toString() {
            return String.format("%.10g", s);
        }
    }
}
