package de.tum.in.probmodels.cli;

import static picocli.CommandLine.Command;
import static picocli.CommandLine.Option;

import de.tum.in.probmodels.impl.prism.PrismProblemInstance;
import de.tum.in.probmodels.output.ModelStatistics;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

@SuppressWarnings("PMD.ImmutableField")
@Command(name = "model", mixinStandardHelpOptions = true, description = "Print statistics for given model")
public class PrismModelStatistics extends DefaultCli<ModelStatistics> {
    @Option(
            names = {"-m", "--model"},
            required = true,
            description = "Path to model file")
    private Path modelPath;

    @Option(
            names = {"-c", "--const"},
            description = "Model constants",
            split = ",")
    private List<String> constants = List.of();

    @Option(names = "--components", description = "Analyse components", negatable = true)
    private boolean components = false;

    @Override
    protected ModelStatistics run() throws IOException {
        return PrismProblemInstance.of(modelPath, null, constants, null).modelStatistics(components);
    }
}
