package de.tum.in.probmodels.cli;

import java.util.Optional;
import java.util.function.Supplier;

public enum StatisticsVerbosity {
    NONE,
    REGULAR,
    ALL;

    public boolean doMore() {
        return this == ALL;
    }

    public boolean none() {
        return this == NONE;
    }

    public boolean any() {
        return this != NONE;
    }

    public <S> Optional<S> ifAny(Supplier<S> statistics) {
        return this == NONE ? Optional.empty() : Optional.of(statistics.get());
    }
}
