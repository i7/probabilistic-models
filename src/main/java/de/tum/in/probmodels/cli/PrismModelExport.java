package de.tum.in.probmodels.cli;

import static picocli.CommandLine.Command;
import static picocli.CommandLine.Option;

import de.tum.in.probmodels.explorer.ModelBuilder;
import de.tum.in.probmodels.generator.Generator;
import de.tum.in.probmodels.impl.prism.PrismProblemInstance;
import de.tum.in.probmodels.model.ModelUtil;
import guru.nidi.graphviz.attribute.Color;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.Callable;
import parser.State;

@SuppressWarnings("PMD.ImmutableField")
@Command(name = "export", mixinStandardHelpOptions = true, description = "Export model to dot file")
public class PrismModelExport implements Callable<Void> {
    @Option(
            names = {"-m", "--model"},
            required = true,
            description = "Path to model file")
    private Path modelPath;

    @Option(
            names = {"-c", "--const"},
            description = "Model constants",
            split = ",")
    private List<String> constants = List.of();

    @Option(
            names = {"-d", "--destination"},
            required = true,
            description = "Path to destination")
    private Path exportPath;

    @Override
    public Void call() throws IOException {
        var export = PrismProblemInstance.of(modelPath, null, constants, null);
        Generator<State, Object> generator = export.standardModel();
        var built = ModelBuilder.buildDefault(generator);
        ModelUtil.modelToDotFile(exportPath, built.model(), built.stateIndices()::getState, s -> true, (s, n) -> {
            if (built.model().isInitialState(s)) {
                n.add(Color.BLUE);
            }
        });
        return null;
    }
}
