package de.tum.in.probmodels.cli;

import static picocli.CommandLine.Option;

import com.google.common.base.Stopwatch;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.Callable;
import javax.annotation.Nullable;
import org.tinylog.Level;
import org.tinylog.Logger;
import org.tinylog.configuration.Configuration;
import picocli.CommandLine;

@SuppressWarnings("PMD.ImmutableField")
public abstract class DefaultCli<R> implements Callable<Integer> {
    @Option(
            names = {"-O", "--output"},
            description = "Output")
    private String output = "-";

    @Option(
            names = {"--pp", "--pretty-print"},
            description = "Pretty-Print JSON output")
    private boolean prettyPrint;

    @Option(
            names = {"--more-statistics", "--stats"},
            description = "Gather more statistics")
    private boolean moreStatistics = false;

    @Nullable
    @CommandLine.ArgGroup
    private LogConfig logConfig;

    private static final class LogConfig {
        @Nullable
        @Option(names = "--log", description = "Set logging level")
        private Level level;

        @Option(names = "--quiet", description = "Disable logging")
        private boolean quiet = false;
    }

    @Nullable
    protected abstract R run() throws IOException;

    protected void customizeGson(GsonBuilder builder) { // NOPMD
    }

    protected final StatisticsVerbosity statistics() {
        if (logConfig != null && logConfig.quiet) {
            return StatisticsVerbosity.NONE;
        }
        return moreStatistics ? StatisticsVerbosity.ALL : StatisticsVerbosity.REGULAR;
    }

    @Override
    public final Integer call() throws IOException {
        if (logConfig != null) {
            if (logConfig.quiet) {
                Configuration.set("writer.level", Level.ERROR.name());
            } else if (logConfig.level != null) {
                Configuration.set("writer.level", logConfig.level.name());
            }
        }

        R statistics = run();
        if (statistics == null) {
            Logger.warn("Expected some statistics but got null!");
            return 0;
        }
        GsonBuilder builder = new GsonBuilder();
        if (prettyPrint) {
            builder.setPrettyPrinting();
        }
        customizeGson(builder);
        builder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        builder.registerTypeAdapter(Duration.class, new DurationTypeAdapter());
        builder.registerTypeAdapter(Stopwatch.class, new StopwatchTypeAdapter());
        builder.registerTypeAdapterFactory(new OptionalTypeAdapterFactory());
        builder.serializeSpecialFloatingPointValues();
        Gson gson = builder.create();

        Logger.info(() -> gson.toJson(statistics, statistics.getClass()));
        if ("-".equals(output)) {
            gson.toJson(statistics, statistics.getClass(), System.out);
        } else {
            try (BufferedWriter writer = Files.newBufferedWriter(
                    Path.of(output),
                    StandardCharsets.UTF_8,
                    StandardOpenOption.WRITE,
                    StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING)) {
                gson.toJson(statistics, statistics.getClass(), writer);
            }
        }
        return 0;
    }

    private static class DurationTypeAdapter extends TypeAdapter<Duration> {
        @Override
        public void write(JsonWriter out, Duration value) throws IOException {
            out.value(value.toMillis());
        }

        @Override
        public Duration read(JsonReader in) throws IOException {
            return Duration.ofMillis(in.nextLong());
        }
    }

    private static class StopwatchTypeAdapter implements JsonSerializer<Stopwatch> {
        @Override
        public JsonElement serialize(Stopwatch src, Type typeOfSrc, JsonSerializationContext context) {
            var elapsed = src.elapsed();
            if (elapsed.isZero()) {
                return JsonNull.INSTANCE;
            }
            return context.serialize(elapsed);
        }
    }

    public static class OptionalTypeAdapterFactory implements TypeAdapterFactory {
        public static class OptionalTypeAdapter<E> extends TypeAdapter<Optional<E>> {
            private final TypeAdapter<E> adapter;

            public OptionalTypeAdapter(TypeAdapter<E> adapter) {
                this.adapter = adapter;
            }

            @Override
            public void write(JsonWriter out, Optional<E> value) throws IOException {
                if (value.isPresent()) {
                    adapter.write(out, value.get());
                } else {
                    out.nullValue();
                }
            }

            @Override
            public Optional<E> read(JsonReader in) throws IOException {
                JsonToken peek = in.peek();
                if (peek != JsonToken.NULL) {
                    return Optional.ofNullable(adapter.read(in));
                }
                in.nextNull();
                return Optional.empty();
            }
        }

        @SuppressWarnings({"unchecked", "rawtypes"})
        @Nullable
        @Override
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
            Class<T> rawType = (Class<T>) type.getRawType();
            if (rawType != Optional.class) {
                return null;
            }
            final ParameterizedType parameterizedType = (ParameterizedType) type.getType();
            final Type actualType = parameterizedType.getActualTypeArguments()[0];
            final TypeAdapter<?> adapter = gson.getAdapter(TypeToken.get(actualType));
            return new OptionalTypeAdapter(adapter);
        }
    }
}
