package de.tum.in.probmodels.primitives;

import java.util.function.IntUnaryOperator;

public record IntArrayIntFunction(int[] array) implements IntUnaryOperator {
    @Override
    public int applyAsInt(int i) {
        return array[i];
    }
}
