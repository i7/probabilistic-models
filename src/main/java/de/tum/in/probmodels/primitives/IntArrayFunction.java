package de.tum.in.probmodels.primitives;

import java.util.function.IntFunction;

public record IntArrayFunction<O>(O[] array) implements IntFunction<O> {
    @Override
    public O apply(int i) {
        return array[i];
    }
}
