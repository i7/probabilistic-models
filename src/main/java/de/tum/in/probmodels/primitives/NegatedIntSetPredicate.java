package de.tum.in.probmodels.primitives;

import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.function.IntPredicate;

public record NegatedIntSetPredicate(IntSet set) implements IntPredicate {
    @Override
    public boolean test(int i) {
        return !set.contains(i);
    }

    @Override
    public IntPredicate negate() {
        return new IntSetPredicate(set);
    }
}
