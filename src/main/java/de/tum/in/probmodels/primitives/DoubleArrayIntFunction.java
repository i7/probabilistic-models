package de.tum.in.probmodels.primitives;

import java.util.function.IntToDoubleFunction;

public record DoubleArrayIntFunction(double[] array) implements IntToDoubleFunction {
    @Override
    public double applyAsDouble(int i) {
        return array[i];
    }
}
