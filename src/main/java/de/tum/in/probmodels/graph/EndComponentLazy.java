package de.tum.in.probmodels.graph;

import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.Hypergraph;
import de.tum.in.probmodels.util.NatBitSets2;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.Collection;
import java.util.List;
import java.util.PrimitiveIterator;
import java.util.stream.IntStream;

record EndComponentLazy<C extends HyperEdge, S extends Hypergraph<C>>(
        S system, IntSet states, Int2ObjectMap<List<C>> choices) implements Component<C> {
    EndComponentLazy(S system, IntSet states, Int2ObjectMap<List<C>> choices) {
        this.system = system;
        this.states = states;
        this.choices = choices;

        // Every state has at least one action
        assert states.intStream().mapToObj(this::choices).noneMatch(Collection::isEmpty);
        // Component is stable, all successors under all choices are subset of the state set
        assert states.intStream()
                .mapToObj(this::choices)
                .flatMap(Collection::stream)
                .map(HyperEdge::successors)
                .allMatch(states::containsAll);
        assert states.containsAll(choices.keySet());
    }

    @Override
    public List<C> choices(int state) {
        assert states.contains(state);
        List<C> stateChoices = this.choices.get(state);
        return stateChoices == null ? system.choices(state) : stateChoices;
    }

    @Override
    public IntSet successors(int state) {
        assert states.contains(state);
        List<C> allowedChoices = choices.get(state);
        if (allowedChoices == null) {
            return system.successors(state);
        }
        assert !allowedChoices.isEmpty();
        return NatBitSets2.lazyUnion(allowedChoices, HyperEdge::successors);
    }

    @Override
    public IntStream successorsStream(int state) {
        assert states.contains(state);
        List<C> allowedChoices = choices.get(state);
        if (allowedChoices == null) {
            return system.successorsStream(state);
        }
        return NatBitSets2.lazyUnion(allowedChoices, HyperEdge::successors).intStream();
    }

    @Override
    public PrimitiveIterator.OfInt successorsIterator(int state) {
        assert states.contains(state) : "State %d not in EC %s".formatted(state, states);
        List<C> allowedChoices = choices.get(state);
        if (allowedChoices == null) {
            return system.successorsIterator(state);
        }
        return NatBitSets2.lazyUnion(allowedChoices, HyperEdge::successors).intIterator();
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj
                || (obj instanceof EndComponentLazy<?, ?> component)
                        && states.equals(component.states)
                        && choices.equals(component.choices);
    }

    @Override
    public int hashCode() {
        return states.hashCode() + choices.hashCode();
    }

    @Override
    public String toString() {
        return "EC[%s]".formatted(states);
    }
}
