package de.tum.in.probmodels.graph;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.Hypergraph;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.List;

/**
 * A BSCC of a (deterministic) transition system.
 */
record BsccEager<C extends HyperEdge>(NatBitSet states, Int2ObjectMap<C> choiceMap) implements Component<C> {
    public static <C extends HyperEdge> BsccEager<C> of(Hypergraph<C> function, IntSet states) {
        Int2ObjectMap<C> choiceMap = new Int2ObjectOpenHashMap<>(states.size());
        states.forEach((int s) -> choiceMap.put(s, function.onlyChoice(s)));
        return new BsccEager<>(states instanceof NatBitSet set ? set : NatBitSets.copyOf(states), choiceMap);
    }

    public BsccEager {
        assert choiceMap.values().stream().map(HyperEdge::successors).allMatch(states::containsAll);
        assert choiceMap.keySet().equals(states);
    }

    @Override
    public boolean contains(int state) {
        return choiceMap.containsKey(state);
    }

    @Override
    public List<C> choices(int state) {
        assert states.contains(state);
        return List.of(choiceMap.get(state));
    }

    @Override
    public C onlyChoice(int state) {
        return choiceMap.get(state);
    }

    @Override
    public IntSet successors(int state) {
        return choiceMap.get(state).successors();
    }

    @Override
    public String toString() {
        return "BSCC[%s]".formatted(states.size() <= 10 ? states : "<%d>".formatted(states.size()));
    }
}
