package de.tum.in.probmodels.graph;

import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.Hypergraph;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.List;

/**
 * A BSCC of a (deterministic) transition system.
 */
record BsccLazy<C extends HyperEdge, S extends Hypergraph<C>>(S system, IntSet states) implements Component<C> {
    @Override
    public List<C> choices(int state) {
        assert system.choices(state).stream().map(HyperEdge::successors).allMatch(states::containsAll);
        return system.choices(state);
    }

    @Override
    public C onlyChoice(int state) {
        assert system.choices(state).size() == 1;
        return system.choices(state).get(0);
    }

    @Override
    public IntSet successors(int state) {
        assert states.containsAll(system.successors(state));
        return system.successors(state);
    }

    @Override
    public String toString() {
        return "BSCC[%s]".formatted(states.size() <= 10 ? states : "<%d>".formatted(states.size()));
    }
}
