package de.tum.in.probmodels.graph;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.Hypergraph;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.tinylog.Logger;

public final class BsccComponentAnalyser<H extends HyperEdge> implements ComponentAnalyser<H, Component<H>> {
    @Override
    public List<? extends IntSet> findComponentStates(Hypergraph<H> graph, IntCollection initial) {
        Logger.trace("Starting BSCC states search");

        List<NatBitSet> bsccStates = new ArrayList<>();
        SccDecomposition.computeSccs(graph::successorsIterator, initial, false, scc -> {
            var iterator = scc.iterator();
            while (iterator.hasNext()) {
                if (!graph.allSuccessorsIn(iterator.nextInt(), scc)) {
                    return true;
                }
            }
            bsccStates.add(scc);
            return true;
        });
        return bsccStates;
    }

    @Override
    public List<Component<H>> findComponents(Hypergraph<H> graph, IntCollection initial, ComponentCopyType copy) {
        Logger.trace("Starting BSCC search");

        List<Component<H>> bsccs = new ArrayList<>();
        SccDecomposition.computeSccs(graph::successorsIterator, initial, false, scc -> {
            var iterator = scc.iterator();
            while (iterator.hasNext()) {
                if (!graph.allSuccessorsIn(iterator.nextInt(), scc)) {
                    return true;
                }
            }
            bsccs.add(makeCanonicalComponent(graph, scc, copy));
            return true;
        });
        return bsccs;
    }

    @Override
    public Component<H> makeCanonicalComponent(Hypergraph<H> model, IntSet states, ComponentCopyType copy) {
        assert !states.isEmpty();
        assert states.intStream().allMatch(s -> model.allSuccessorsIn(s, states))
                : "Consistency error: " + states + "\n"
                        + states.intStream()
                                .filter(s -> !model.allSuccessorsIn(s, states))
                                .mapToObj(s -> s + " -> " + model.successors(s))
                                .collect(Collectors.joining("\n"));
        return switch (copy) {
            case LAZY -> new BsccLazy<>(model, states);
            case EAGER -> BsccEager.of(model, states);
        };
    }
}
