package de.tum.in.probmodels.graph;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.Hypergraph;
import de.tum.in.probmodels.util.Collections3;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMaps;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import org.tinylog.Logger;

public class EndComponentAnalyser<H extends HyperEdge> implements ComponentAnalyser<H, Component<H>> {
    // TODO Tailored state search

    @Override
    public List<Component<H>> findComponents(Hypergraph<H> model, IntCollection initial, ComponentCopyType copy) {
        Logger.trace("Starting MECs search");

        List<Component<H>> mecs = computeMaximalComponents(model, initial, copy);
        if (mecs.isEmpty()) {
            Logger.trace("Found no MECs");
            return Collections.emptyList();
        }
        if (Logger.isTraceEnabled()) {
            Logger.trace(
                    "Found {} MECs:\n{}",
                    mecs.size(),
                    mecs.stream().map(Objects::toString).collect(Collectors.joining("\n")));
        }

        return mecs;
    }

    @Override
    public Component<H> makeCanonicalComponent(Hypergraph<H> graph, IntSet states, ComponentCopyType copy) {
        return switch (copy) {
            case LAZY -> new LazyMaximalEndComponent<>(graph, states);
            case EAGER -> Objects.requireNonNull(createMaximal(graph, states, copy));
        };
    }

    @Nullable
    private static <H extends HyperEdge> Component<H> createMaximalSingleton(
            Hypergraph<H> system, int state, ComponentCopyType copy) {
        List<H> choices = system.choices(state);
        if (choices.isEmpty()) {
            return null;
        }
        return switch (copy) {
            case EAGER -> {
                List<H> allowedActions = Collections3.copyFiltered(choices, c -> c.isOnlySuccessor(state));
                yield allowedActions.isEmpty()
                        ? null
                        : EndComponentEager.of(
                                NatBitSets.singleton(state), Int2ObjectMaps.singleton(state, allowedActions));
            }
            case LAZY -> {
                List<H> allowedActions = Collections3.copyOnWriteFilter(choices, c -> c.isOnlySuccessor(state));
                yield allowedActions == null || !allowedActions.isEmpty()
                        ? new EndComponentLazy<>(
                                system, NatBitSets.singleton(state), Int2ObjectMaps.singleton(state, allowedActions))
                        : null;
            }
        };
    }

    @Nullable
    private static <H extends HyperEdge> Component<H> createMaximal(
            Hypergraph<H> system, IntSet states, ComponentCopyType copy) {
        if (states.size() == 1) {
            int state = states instanceof NatBitSet set
                    ? set.firstInt()
                    : states.intIterator().nextInt();
            return createMaximalSingleton(system, state, copy);
        }

        Int2ObjectMap<List<H>> subActions = new Int2ObjectOpenHashMap<>(states.size());
        NatBitSet stateSet = NatBitSets.ensureModifiable(NatBitSets.copyOf(states));

        // TODO Use predecessor relation?
        boolean changed;
        do {
            changed = false;

            NatBitSet toRemove = NatBitSets.set();
            IntIterator stateIterator = stateSet.iterator();
            while (stateIterator.hasNext()) {
                int state = stateIterator.nextInt();
                List<H> stateRestriction = subActions.get(state);
                Predicate<H> allowedChoice = choice ->
                        stateSet.containsAll(choice.successors()) && !toRemove.intersects(choice.successors());
                if (stateRestriction == null) {
                    List<H> choices = system.choices(state);
                    if (choices.isEmpty()) {
                        toRemove.set(state);
                        changed = true;
                    } else {
                        List<H> actionCopy =
                                switch (copy) {
                                    case LAZY -> Collections3.copyOnWriteFilter(choices, allowedChoice);
                                    case EAGER -> Collections3.copyFiltered(choices, allowedChoice);
                                };
                        if (actionCopy != null) {
                            if (actionCopy.isEmpty()) {
                                assert system.choicesStream(state).noneMatch(allowedChoice);
                                toRemove.set(state);
                            } else {
                                subActions.put(state, actionCopy);
                            }
                            changed = true;
                        }
                    }
                } else {
                    if (stateRestriction.removeIf(allowedChoice.negate())) {
                        changed = true;
                        if (stateRestriction.isEmpty()) {
                            toRemove.set(state);
                            subActions.remove(state);
                        }
                    }
                }
            }
            stateSet.andNot(toRemove);
            assert copy != ComponentCopyType.EAGER || stateSet.equals(subActions.keySet());
        } while (changed);

        if (stateSet.isEmpty()) {
            return null;
        }

        return switch (copy) {
            case EAGER -> EndComponentEager.of(stateSet, subActions);
            case LAZY -> new EndComponentLazy<>(system, stateSet, subActions);
        };
    }

    private static <H extends HyperEdge> List<Component<H>> computeMaximalComponents(
            Hypergraph<H> system, IntCollection initial, ComponentCopyType copy) {
        List<Component<H>> mecs = new ArrayList<>();

        Deque<Component<H>> workList = new ArrayDeque<>();
        SccDecomposition.computeSccs(system::successorsIterator, initial, false, scc -> {
            if (scc.size() == 1) {
                var component = createMaximalSingleton(system, scc.firstInt(), copy);
                if (component == null) {
                    assert !SccDecomposition.isBscc(system::successorsIterator, scc);
                } else {
                    mecs.add(component);
                }
            } else {
                var component = createMaximal(system, scc, copy);
                if (component != null) {
                    workList.add(component);
                }
            }
            return true;
        });

        List<Component<H>> preMecs = new ArrayList<>();

        while (!workList.isEmpty()) {
            Component<H> mec = workList.remove();
            preMecs.clear();

            SccDecomposition.computeSccs(mec::successorsIterator, mec.states(), false, scc -> {
                var component = createMaximal(system, scc, copy);
                if (component != null) {
                    preMecs.add(component);
                }
                return true;
            });

            if (preMecs.size() == 1) {
                Component<H> refinedEc = preMecs.get(0);
                assert !refinedEc.states().isEmpty();
                if (mec.equals(refinedEc)) {
                    mecs.add(refinedEc);
                    continue;
                }
            }
            workList.addAll(preMecs);
        }
        assert mecs.size() == Set.copyOf(mecs).size();

        return mecs;
    }
}
