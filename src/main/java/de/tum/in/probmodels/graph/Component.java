package de.tum.in.probmodels.graph;

import de.tum.in.probmodels.model.HasStateSet;
import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.Hypergraph;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.List;
import java.util.stream.IntStream;

/**
 * A stable component of a hypergraph (e.g. probabilistic systems)
 */
public interface Component<H extends HyperEdge> extends Hypergraph<H>, HasStateSet {
    /**
     * The states in the component.
     */
    @Override
    IntSet states();

    default int size() {
        return states().size();
    }

    default IntStream stateStream() {
        return states().intStream();
    }

    default boolean contains(int state) {
        return states().contains(state);
    }

    /**
     * The "stable" choices of this component, i.e. (i) the support of each choice is a subset of {@link #states()} and
     * (ii) the set of choices is not empty.
     */
    @Override
    List<H> choices(int state);

    default boolean isStable(H choice) {
        return choice.isSubsetOf(states());
    }

    default boolean areStable(Iterable<H> choices) {
        for (H choice : choices) {
            if (!isStable(choice)) {
                return false;
            }
        }
        return true;
    }
}
