package de.tum.in.probmodels.graph;

import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.Hypergraph;
import de.tum.in.probmodels.util.NatBitSets2;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.PrimitiveIterator;
import java.util.function.IntFunction;
import java.util.stream.IntStream;
import javax.annotation.Nullable;

/**
 * A lazy end component, computing the set of stable actions on the fly. Sometimes (e.g. when only interested in the
 * quotient model), we do not require the "internal" actions of a component and there is no need to compute them.
 */
final class LazyMaximalEndComponent<C extends HyperEdge, S extends Hypergraph<C>> implements Component<C> {
    private final S system;
    private final IntSet states;
    private final Int2ObjectMap<List<C>> restrictions;

    public LazyMaximalEndComponent(S system, IntSet states) {
        assert !states.isEmpty();
        assert states.intStream().mapToObj(system::choices).noneMatch(List::isEmpty);
        assert states.intStream()
                .mapToObj(system::choices)
                .allMatch(choices -> choices.stream().map(HyperEdge::successors).anyMatch(states::containsAll));
        this.system = system;
        this.states = states;
        this.restrictions = new Int2ObjectOpenHashMap<>(states.size());
    }

    private List<C> compute(int state) {
        assert states.contains(state);
        return restrictions.computeIfAbsent(state, (IntFunction<? extends List<C>>) s -> {
            List<C> modelChoices = system.choices(s);
            ListIterator<C> iterator = modelChoices.listIterator();
            @Nullable List<C> copy = null;
            while (iterator.hasNext()) {
                int index = iterator.nextIndex();
                C choice = iterator.next();
                // TODO Would like "isSubsetOf" here
                if (states.containsAll(choice.successors())) {
                    if (copy != null) {
                        copy.add(choice);
                    }
                } else {
                    if (copy == null) {
                        copy = new ArrayList<>(modelChoices.subList(0, index));
                        assert copy.stream().map(HyperEdge::successors).allMatch(states::containsAll);
                    }
                }
            }
            assert copy == null || !copy.isEmpty()
                    : "No transition of %d (%s) element of states %s".formatted(s, system.choices(s), states);
            assert (copy == null ? system.choices(s) : copy)
                    .stream().map(HyperEdge::successors).allMatch(states::containsAll);
            return copy == null ? List.of() : copy;
        });
    }

    @Override
    public List<C> choices(int state) {
        List<C> choices = compute(state);
        assert (choices.isEmpty() ? system.choices(state) : choices)
                .stream().map(HyperEdge::successors).allMatch(states::containsAll);
        return choices.isEmpty() ? system.choices(state) : choices;
    }

    @Override
    public IntSet successors(int state) {
        List<C> allowedChoices = compute(state);
        if (allowedChoices.isEmpty()) {
            return system.successors(state);
        }
        return NatBitSets2.lazyUnion(allowedChoices, HyperEdge::successors);
    }

    @Override
    public IntStream successorsStream(int state) {
        List<C> allowedChoices = compute(state);
        if (allowedChoices.isEmpty()) {
            return system.successorsStream(state);
        }
        return NatBitSets2.lazyUnion(allowedChoices, HyperEdge::successors).intStream();
    }

    @Override
    public PrimitiveIterator.OfInt successorsIterator(int state) {
        List<C> allowedChoices = compute(state);
        if (allowedChoices.isEmpty()) {
            return system.successorsIterator(state);
        }
        return NatBitSets2.lazyUnion(allowedChoices, HyperEdge::successors).intIterator();
    }

    @Override
    public String toString() {
        return "EC[%s]".formatted(states);
    }

    public S system() {
        return system;
    }

    @Override
    public IntSet states() {
        return states;
    }

    @Override
    public boolean equals(Object obj) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int hashCode() {
        throw new UnsupportedOperationException();
    }
}
