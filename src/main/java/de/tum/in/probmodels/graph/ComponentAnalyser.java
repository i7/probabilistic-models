package de.tum.in.probmodels.graph;

import com.google.common.collect.Lists;
import de.tum.in.probmodels.model.HasInitialStates;
import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.Hypergraph;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.List;

/**
 * Algorithms to find {@link Component components}.
 */
public interface ComponentAnalyser<H extends HyperEdge, C extends Component<H>> {
    enum ComponentCopyType {
        LAZY,
        EAGER
    }

    static <H extends HyperEdge> ComponentAnalyser<H, ? extends Component<H>> defaultAnalyser(
            Hypergraph<? extends H> graph) {
        return graph.isDeterministic() ? new BsccComponentAnalyser<>() : new EndComponentAnalyser<>();
    }

    default <S extends Hypergraph<H> & HasInitialStates> List<? extends IntSet> findComponentStates(S model) {
        return findComponentStates(model, model.initialStates());
    }

    default List<? extends IntSet> findComponentStates(Hypergraph<H> graph, IntCollection initial) {
        return Lists.transform(findComponents(graph, initial, ComponentCopyType.LAZY), Component::states);
    }

    default <S extends Hypergraph<H> & HasInitialStates> List<C> findComponents(S model) {
        return findComponents(model, model.initialStates(), ComponentCopyType.EAGER);
    }

    default <S extends Hypergraph<H> & HasInitialStates> List<C> findComponents(S model, ComponentCopyType copy) {
        return findComponents(model, model.initialStates(), copy);
    }

    default List<C> findComponents(Hypergraph<H> graph, IntCollection initial) {
        return findComponents(graph, initial, ComponentCopyType.EAGER);
    }

    List<C> findComponents(Hypergraph<H> model, IntCollection initial, ComponentCopyType copy);

    /**
     * Create a canonical component for the given set of states (i.e. a BSCC or MEC). It is required that such a
     * component actually exists for the given set of states. Otherwise, the result is undefined.
     */
    default C makeCanonicalComponent(Hypergraph<H> model, IntSet states) {
        return makeCanonicalComponent(model, states, ComponentCopyType.EAGER);
    }

    C makeCanonicalComponent(Hypergraph<H> model, IntSet states, ComponentCopyType copy);
}
