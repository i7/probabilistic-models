package de.tum.in.probmodels.graph;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.util.NatBitSets2;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.Collection;
import java.util.List;

record EndComponentEager<E extends HyperEdge>(
        NatBitSet states, Int2ObjectMap<List<E>> choices, Int2ObjectMap<IntSet> successorCache)
        implements Component<E> {
    public static <E extends HyperEdge> EndComponentEager<E> of(IntSet states, Int2ObjectMap<List<E>> choices) {
        assert states.equals(choices.keySet());
        // Every state has at least one action
        assert states.intStream().mapToObj(choices::get).noneMatch(Collection::isEmpty);
        // Component is stable, all successors under all choices are subset of the state set
        assert states.intStream()
                .mapToObj(choices::get)
                .flatMap(Collection::stream)
                .map(HyperEdge::successors)
                .allMatch(states::containsAll);

        return new EndComponentEager<>(
                states instanceof NatBitSet set ? set : NatBitSets.copyOf(states),
                choices,
                new Int2ObjectOpenHashMap<>());
    }

    @Override
    public List<E> choices(int state) {
        assert states.contains(state);
        return choices.get(state);
    }

    @Override
    public IntSet successors(int state) {
        assert states.contains(state) : states;
        return successorCache.computeIfAbsent(
                state, s -> NatBitSets2.lazyUnion(choices.get(state), HyperEdge::successors));
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj
                || (obj instanceof EndComponentEager<?> component)
                        && states.equals(component.states)
                        && choices.equals(component.choices);
    }

    @Override
    public int hashCode() {
        return states.hashCode() + choices.hashCode();
    }

    @Override
    public String toString() {
        return "EC[%s]".formatted(states);
    }
}
