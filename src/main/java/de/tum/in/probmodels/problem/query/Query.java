package de.tum.in.probmodels.problem.query;

public interface Query {
    Optimization optimization();
}
