package de.tum.in.probmodels.problem.property;

import java.util.OptionalInt;
import java.util.function.Function;

public record DefaultUntilQuantity<S>(
        int minimalSteps, OptionalInt maximalSteps, Function<S, UntilType> type, boolean isNegated)
        implements UntilQuantity<S> {}
