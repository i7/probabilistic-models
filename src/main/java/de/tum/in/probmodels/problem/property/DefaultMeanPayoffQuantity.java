package de.tum.in.probmodels.problem.property;

import de.tum.in.probmodels.generator.RewardGenerator;
import de.tum.in.probmodels.values.Bounds;
import java.util.Optional;

public record DefaultMeanPayoffQuantity<S>(RewardGenerator<S> reward, Optional<Bounds> rewardBounds)
        implements MeanPayoffQuantity<S> {
    @Override
    public DefaultMeanPayoffQuantity<S> withBounds(Bounds rewardBounds) {
        return new DefaultMeanPayoffQuantity<>(reward, Optional.of(rewardBounds));
    }
}
