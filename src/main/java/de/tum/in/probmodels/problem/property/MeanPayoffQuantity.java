package de.tum.in.probmodels.problem.property;

import de.tum.in.probmodels.values.Bounds;

public interface MeanPayoffQuantity<S> extends RewardBasedQuantity<S> {
    MeanPayoffQuantity<S> withBounds(Bounds rewardBounds);
}
