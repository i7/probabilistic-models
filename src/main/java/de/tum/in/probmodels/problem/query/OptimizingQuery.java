package de.tum.in.probmodels.problem.query;

public record OptimizingQuery(Optimization optimization) implements Query {
    @Override
    public String toString() {
        return optimization.toString();
    }
}
