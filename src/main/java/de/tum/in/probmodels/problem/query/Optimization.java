package de.tum.in.probmodels.problem.query;

import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.values.Selection;

public enum Optimization {
    MAX_VALUE,
    MIN_VALUE,
    UNIQUE_VALUE,
    MAX_MIN_VALUE,
    MIN_MAX_VALUE;

    public Selection selection(Actor actor) {
        assert !singlePlayer() || actor == Actor.PROTAGONIST;
        return switch (this) {
            case MAX_VALUE -> Selection.MAX_VALUE;
            case MIN_VALUE -> Selection.MIN_VALUE;
            case UNIQUE_VALUE -> Selection.UNIQUE_VALUE;
            case MAX_MIN_VALUE -> actor.isProtagonist() ? Selection.MAX_VALUE : Selection.MIN_VALUE;
            case MIN_MAX_VALUE -> actor.isProtagonist() ? Selection.MIN_VALUE : Selection.MAX_VALUE;
        };
    }

    public boolean singlePlayer() {
        return this == MAX_VALUE || this == MIN_VALUE || this == UNIQUE_VALUE;
    }

    @Override
    public String toString() {
        return switch (this) {
            case MAX_VALUE -> "max";
            case MIN_VALUE -> "min";
            case UNIQUE_VALUE -> "uniq";
            case MAX_MIN_VALUE -> "max_min";
            case MIN_MAX_VALUE -> "min_max";
        };
    }

    public Actor maximizer() {
        return switch (this) {
            case MAX_VALUE, UNIQUE_VALUE, MAX_MIN_VALUE -> Actor.PROTAGONIST;
            case MIN_VALUE, MIN_MAX_VALUE -> Actor.ANTAGONIST;
        };
    }

    public Optimization complement() {
        return switch (this) {
            case MAX_VALUE -> MIN_VALUE;
            case MIN_VALUE -> MAX_VALUE;
            case UNIQUE_VALUE -> UNIQUE_VALUE;
            case MAX_MIN_VALUE -> MIN_MAX_VALUE;
            case MIN_MAX_VALUE -> MAX_MIN_VALUE;
        };
    }
}
