package de.tum.in.probmodels.problem.query;

public enum Comparison {
    GREATER_OR_EQUAL,
    GREATER_THAN,
    LESS_OR_EQUAL,
    LESS_THAN;

    @Override
    public String toString() {
        return switch (this) {
            case GREATER_OR_EQUAL -> ">=";
            case GREATER_THAN -> ">";
            case LESS_OR_EQUAL -> "<=";
            case LESS_THAN -> "<";
        };
    }
}
