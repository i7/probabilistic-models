package de.tum.in.probmodels.problem.verdict;

public interface BoundHandler<R> extends BoundVerdict, BoundInterpreter<R> {}
