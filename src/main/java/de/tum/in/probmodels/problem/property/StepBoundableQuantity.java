package de.tum.in.probmodels.problem.property;

import java.util.OptionalInt;

public interface StepBoundableQuantity<S> extends Quantity<S> {
    int minimalSteps();

    OptionalInt maximalSteps();

    default boolean hasStepBounds() {
        return minimalSteps() > 0 || maximalSteps().isPresent();
    }
}
