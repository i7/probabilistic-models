package de.tum.in.probmodels.problem.verdict;

import de.tum.in.probmodels.values.Bounds;

public interface BoundVerdict {
    boolean isSolved(Bounds bounds);
}
