package de.tum.in.probmodels.problem.verdict;

import de.tum.in.probmodels.values.Bounds;

public interface BoundInterpreter<R> {
    R interpret(Bounds bounds);
}
