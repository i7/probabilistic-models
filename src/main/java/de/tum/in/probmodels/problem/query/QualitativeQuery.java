package de.tum.in.probmodels.problem.query;

public record QualitativeQuery(Comparison comparison, double threshold, Optimization optimization) implements Query {
    @Override
    public String toString() {
        return "%s%s%.5g".formatted(optimization, comparison, threshold);
    }
}
