package de.tum.in.probmodels.problem.verdict;

import de.tum.in.probmodels.util.Precision;
import de.tum.in.probmodels.values.Bounds;

public record QuantitativeVerdict(double precision, boolean relativeError) implements BoundHandler<Double> {
    public static QuantitativeVerdict of(Precision precision) {
        return new QuantitativeVerdict(precision.bound(), precision.relativeError());
    }

    @Override
    public boolean isSolved(Bounds bounds) {
        if (relativeError) {
            return bounds.upperBound() <= bounds.lowerBound() * (1 + precision)
                    && bounds.lowerBound() >= bounds.upperBound() * (1 - precision);
        }
        return bounds.difference() < 2 * precision;
    }

    @Override
    public Double interpret(Bounds bounds) {
        return bounds.average();
    }

    @Override
    public String toString() {
        return precision + (relativeError ? "(rel)" : "(abs)");
    }
}
