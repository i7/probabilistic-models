package de.tum.in.probmodels.problem.verdict;

import de.tum.in.probmodels.problem.query.Comparison;
import de.tum.in.probmodels.values.Bounds;

public record QualitativeVerdict(Comparison comparison, double threshold) implements BoundHandler<Boolean> {
    @Override
    public boolean isSolved(Bounds bounds) {
        return switch (comparison) {
            case GREATER_OR_EQUAL, LESS_THAN -> threshold <= bounds.lowerBound() || bounds.upperBound() < threshold;
            case GREATER_THAN, LESS_OR_EQUAL -> threshold < bounds.lowerBound() || bounds.upperBound() <= threshold;
        };
    }

    @Override
    public Boolean interpret(Bounds bounds) {
        assert isSolved(bounds);

        return switch (comparison) {
            case GREATER_OR_EQUAL -> threshold <= bounds.lowerBound();
            case GREATER_THAN -> threshold < bounds.lowerBound();
            case LESS_OR_EQUAL -> bounds.upperBound() <= threshold;
            case LESS_THAN -> bounds.upperBound() < threshold;
        };
    }

    @Override
    public String toString() {
        return comparison + "/" + threshold;
    }
}
