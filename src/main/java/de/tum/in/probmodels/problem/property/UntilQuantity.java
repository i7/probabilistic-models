package de.tum.in.probmodels.problem.property;

import java.util.function.Function;

public interface UntilQuantity<S> extends StepBoundableQuantity<S> {
    Function<S, UntilType> type();

    boolean isNegated();
}
