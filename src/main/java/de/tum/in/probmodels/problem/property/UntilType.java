package de.tum.in.probmodels.problem.property;

public enum UntilType {
    GOAL,
    SINK,
    SAFE
}
