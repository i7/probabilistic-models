package de.tum.in.probmodels.problem;

import de.tum.in.probmodels.generator.Generator;
import de.tum.in.probmodels.problem.property.Quantity;
import de.tum.in.probmodels.problem.query.Query;

public record Problem<S, L>(String name, Generator<S, L> model, Query query, Quantity<S> quantity) {
    @Override
    public String toString() {
        return "%s:%s[%s]".formatted(name, query, quantity);
    }

    public Problem<S, L> withQuantity(Quantity<S> quantity) {
        return new Problem<>(name, model, query, quantity);
    }
}
