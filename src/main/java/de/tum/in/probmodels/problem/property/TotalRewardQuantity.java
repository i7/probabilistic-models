package de.tum.in.probmodels.problem.property;

import java.util.function.Function;

public interface TotalRewardQuantity<S> extends RewardBasedQuantity<S>, StepBoundableQuantity<S> {
    Function<S, UntilType> reachability();
}
