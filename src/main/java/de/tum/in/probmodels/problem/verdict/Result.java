package de.tum.in.probmodels.problem.verdict;

import de.tum.in.probmodels.values.Bounds;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.annotation.Nullable;

public interface Result<S, R> {
    Collection<S> states();

    @Nullable
    R get(S state);

    default Map<S, R> asMap() {
        Map<S, R> map = new HashMap<>();
        states().forEach(s -> {
            R value = get(s);
            if (value != null) {
                map.put(s, value);
            }
        });
        return map;
    }

    static <S, R> Result<S, R> of(Map<S, R> results) {
        if (results.size() == 1) {
            var entry = results.entrySet().iterator().next();
            return new SingletonResult<>(entry.getKey(), entry.getValue());
        }
        return new MapResult<>(results);
    }

    static <S, R> Result<S, R> of(S state, R result) {
        return new SingletonResult<>(state, result);
    }

    static <S, R> Result<S, R> of(
            Collection<S> states, BoundInterpreter<R> interpreter, Function<S, Bounds> stateBounds) {
        return of(states.stream()
                .collect(Collectors.toMap(Function.identity(), s -> interpreter.interpret(stateBounds.apply(s)))));
    }

    default String format() {
        Collection<S> states = states();

        StringBuilder builder = new StringBuilder(states.size() * 20);
        if (states.size() > 1) {
            for (S state : states) {
                R value = get(state);
                String format = value instanceof Double ? "%s %.10g%n" : "%s %s%n";
                builder.append(format.formatted(state, value)).append('\n');
            }
        } else {
            S state = states.iterator().next();
            R value = get(state);
            String format = value instanceof Double ? "%s %.10g%n" : "%s %s%n";
            builder.append(format.formatted(state, value)).append('\n');
        }
        return builder.toString();
    }

    record MapResult<S, R>(Map<S, R> results) implements Result<S, R> {
        public MapResult(Map<S, R> results) {
            this.results = Map.copyOf(results);
        }

        @Override
        public Collection<S> states() {
            return results.keySet();
        }

        @Override
        public R get(S state) {
            return results.get(state);
        }

        @Override
        public Map<S, R> asMap() {
            return Map.copyOf(this.results);
        }
    }

    record SingletonResult<S, R>(S state, R result) implements Result<S, R> {
        @Override
        public Collection<S> states() {
            return Collections.singleton(state);
        }

        @Override
        public R get(S state) {
            return this.state.equals(state) ? result : null;
        }

        @Override
        public Map<S, R> asMap() {
            return Map.of(state, result);
        }
    }
}
