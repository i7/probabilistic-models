package de.tum.in.probmodels.problem.property;

import de.tum.in.probmodels.generator.RewardGenerator;
import de.tum.in.probmodels.values.Bounds;
import java.util.Optional;

public interface RewardBasedQuantity<S> extends Quantity<S> {
    RewardGenerator<S> reward();

    // TODO These should go somewhere else
    Optional<Bounds> rewardBounds();
}
