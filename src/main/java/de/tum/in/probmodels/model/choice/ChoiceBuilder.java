package de.tum.in.probmodels.model.choice;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.probmodels.util.FloatUtil;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntIterator;
import java.util.Optional;
import javax.annotation.Nullable;

public final class ChoiceBuilder<L> {
    // TODO Should we use Kahan sum here?
    private final Int2DoubleMap map = new Int2DoubleOpenHashMap();
    private final NatBitSet support = NatBitSets.set();

    @Nullable
    private final L label;

    public ChoiceBuilder(@Nullable L label) {
        this.label = label;
    }

    public void add(int state, double probability) {
        assert Double.isFinite(probability);

        double old = map.getOrDefault(state, Double.NaN);
        if (Double.isNaN(old)) {
            map.put(state, probability);
            support.set(state);
        } else {
            double newValue = probability + old;
            if (newValue <= 0.0d) {
                support.clear(state);
                map.remove(state);
            } else {
                map.put(state, newValue);
            }
        }
    }

    public void set(int state, double probability) {
        assert Double.isFinite(probability) && probability >= 0.0;

        if (probability == 0.0d) {
            map.remove(state);
            support.clear(state);
        } else {
            map.put(state, probability);
            support.set(state);
        }
    }

    public boolean isEmpty() {
        return support.isEmpty();
    }

    public Optional<Choice<L>> scaled() {
        if (support.isEmpty()) {
            return Optional.empty();
        }
        int size = support.size();
        if (size == 1) {
            int successor = support.firstInt();
            return Optional.of(SingletonChoice.of(label, successor));
        }

        int[] keys = new int[size];
        double[] probabilities = new double[size];

        int index = 0;
        FloatUtil.KahanSum sum = new FloatUtil.KahanSum();
        IntIterator keyIterator = support.iterator();
        while (keyIterator.hasNext()) {
            int key = keyIterator.nextInt();
            double probability = map.get(key);
            keys[index] = key;
            probabilities[index] = probability;
            index += 1;
            sum.add(probability);
        }
        double totalSum = sum.get();
        if (!FloatUtil.isOne(totalSum)) {
            for (int i = 0; i < probabilities.length; i++) {
                probabilities[i] /= totalSum;
            }
        }
        return Optional.of(ArrayChoice.of(label, keys, probabilities, support));
    }

    public Optional<Choice<L>> build() {
        if (support.isEmpty()) {
            return Optional.empty();
        }
        assert FloatUtil.isOne(map.values().doubleStream().sum());

        int size = support.size();
        if (size == 1) {
            int successor = support.firstInt();
            return Optional.of(SingletonChoice.of(label, successor));
        }

        int[] keys = new int[size];
        double[] probabilities = new double[size];

        int index = 0;
        IntIterator keyIterator = support.iterator();
        while (keyIterator.hasNext()) {
            int key = keyIterator.nextInt();
            double probability = map.get(key);
            keys[index] = key;
            probabilities[index] = probability;
            index += 1;
        }
        return Optional.of(ArrayChoice.of(label, keys, probabilities, support));
    }
}
