package de.tum.in.probmodels.model.choice;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.probmodels.model.LabelledWeightedHyperEdge;
import de.tum.in.probmodels.model.MappableHyperEdge;
import de.tum.in.probmodels.model.ProbabilisticHyperEdge;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.List;
import javax.annotation.Nullable;

/**
 * The default implementation for (labelled) probabilistic edges.
 *
 * @param <L>
 */
public sealed interface Choice<L>
        extends LabelledWeightedHyperEdge<L>, ProbabilisticHyperEdge, MappableHyperEdge<Choice<L>>
        permits AbstractChoice {
    static <L> ChoiceBuilder<L> builder() {
        return new ChoiceBuilder<>(null);
    }

    static <L> ChoiceBuilder<L> labelledBuilder(@Nullable L label) {
        return new ChoiceBuilder<>(label);
    }

    static <L> Choice<L> singleton(int successor) {
        return new SingletonChoice.Unlabelled<>(successor);
    }

    static <L> Choice<L> labelledSingleton(int successor, @Nullable L label) {
        return SingletonChoice.of(label, successor);
    }

    static <L> List<Choice<L>> onlySingleton(int successor) {
        return List.of(new SingletonChoice.Unlabelled<>(successor));
    }

    @Override
    NatBitSet successors();

    @Override
    default boolean someSuccessorIn(IntSet states) {
        return successors().intersects(states);
    }

    @Override
    <K> Choice<K> withoutLabel();

    @Override
    <K> Choice<K> withLabel(K label);
}
