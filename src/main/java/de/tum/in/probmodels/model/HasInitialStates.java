package de.tum.in.probmodels.model;

import de.tum.in.probmodels.util.PrimitiveCollections;
import it.unimi.dsi.fastutil.ints.IntSet;

public interface HasInitialStates {
    IntSet initialStates();

    default int initialStateCount() {
        return initialStates().size();
    }

    default int onlyInitialState() {
        return PrimitiveCollections.onlyElement(initialStates());
    }

    default boolean isInitialState(int state) {
        return initialStates().contains(state);
    }
}
