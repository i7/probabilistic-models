package de.tum.in.probmodels.model.index;

import java.util.Set;

public interface StateIndexer<S> {
    int addState(S state);

    default int addIfAbsent(S state) {
        return contains(state) ? indexOf(state) : addState(state);
    }

    default int addIfAbsent(S state, StateIndexConsumer<S> action) {
        if (contains(state)) {
            return indexOf(state);
        }
        int id = addState(state);
        action.accept(state, id);
        return id;
    }

    int indexOf(S state);

    boolean contains(S state);

    void forEach(StateIndexConsumer<S> consumer);

    Set<S> states();

    interface StateIndexConsumer<S> {
        void accept(S state, int index);
    }
}
