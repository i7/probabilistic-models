package de.tum.in.probmodels.model;

import java.util.stream.IntStream;

public interface MutableDenseSystem<H extends HyperEdge> extends DenseSystem<H>, MutableSystem<H> {
    int makeState();

    @SuppressWarnings("UnusedReturnValue")
    IntStream makeStates(int count);
}
