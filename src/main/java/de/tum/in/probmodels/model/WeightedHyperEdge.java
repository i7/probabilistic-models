package de.tum.in.probmodels.model;

import de.tum.in.probmodels.values.Bounds;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.IntFunction;
import java.util.function.IntPredicate;
import java.util.function.IntToDoubleFunction;
import javax.annotation.Nullable;

public interface WeightedHyperEdge extends Iterable<Int2DoubleMap.Entry>, HyperEdge {
    @FunctionalInterface
    interface WeightedEdgeConsumer {
        void accept(int state, double weight);
    }

    @FunctionalInterface
    interface WeightFunction {
        double accept(int state, double weight);
    }

    /**
     * Return the weight of this successor.
     */
    double get(int successor);

    default double minimumWeight() {
        var iterator = successors().iterator();
        double minimum = Double.POSITIVE_INFINITY;
        while (iterator.hasNext()) {
            int successor = iterator.nextInt();
            double weight = get(successor);
            if (weight < minimum) {
                minimum = weight;
            }
        }
        return minimum;
    }

    default double maximumWeight() {
        var iterator = successors().iterator();
        double maximum = Double.NEGATIVE_INFINITY;
        while (iterator.hasNext()) {
            int successor = iterator.nextInt();
            double weight = get(successor);
            if (weight > maximum) {
                maximum = weight;
            }
        }
        return maximum;
    }

    /**
     * Sum over all successors with weights specified by the array, i.e. {@code sum(s in support) w(s) * array(s)}.
     */
    default double sumWeighted(double[] array) {
        return sumWeighted(i -> array[i]);
    }

    /**
     * Sum over all successors with weights specified by {@code f}, i.e. {@code sum(s in support) w(s) * f(s)}.
     */
    double sumWeighted(IntToDoubleFunction f);

    default Bounds sumWeightedBounds(IntFunction<Bounds> f) {
        return Bounds.of(sumWeighted(s -> f.apply(s).lowerBound()), sumWeighted(s -> f.apply(s)
                .upperBound()));
    }

    default double[] sumWeightedExceptJacobi(double[][] array, int state) {
        double[] result = new double[array.length];
        Arrays.setAll(result, i -> sumWeightedExceptJacobi(s -> array[i][s], state));
        return result;
    }

    /**
     * Sum over all successors except the given state, with weights specified by {@code f}, and re-weigh according to
     * the Jacobi rule, i.e. {@code (sum(s in support, s != state) w(s) * f(s)) / w(state)} (or the
     * {@link #sumWeighted(IntToDoubleFunction) regular sum} if {@code p(state) == 0.0}). Returns {@link Double#NaN NaN}
     * if the state is the only successor.
     */
    default double sumWeightedExceptJacobi(IntToDoubleFunction f, int state) {
        return sumWeightedExceptJacobi(f, s -> s == state);
    }

    /**
     * Sum over all successors except the given states, with weights specified by {@code f}, and re-weigh according to
     * the Jacobi rule, i.e. {@code (sum(s in support, s not in except) w(s) * f(s)) / sum(s in except) w(s)} (or the
     * {@link #sumWeighted(IntToDoubleFunction) regular sum} if {@code sum(s in except) w(s) == 0.0}). Returns
     * {@link Double#NaN NaN} if the states are the only successors.
     */
    double sumWeightedExceptJacobi(IntToDoubleFunction f, IntPredicate except);

    default Optional<Bounds> sumWeightedExceptJacobiBounds(IntFunction<Bounds> f, int state) {
        return sumWeightedExceptJacobiBounds(f, s -> s == state);
    }

    default Optional<Bounds> sumWeightedExceptJacobiBounds(IntFunction<Bounds> f, IntPredicate except) {
        double lower = sumWeightedExceptJacobi(s -> f.apply(s).lowerBound(), except);
        double upper = sumWeightedExceptJacobi(s -> f.apply(s).upperBound(), except);
        assert Double.isNaN(lower) == Double.isNaN(upper);
        return Double.isNaN(lower) ? Optional.empty() : Optional.of(Bounds.of(lower, upper));
    }

    /**
     * Performs an action for each successor-probability pair.
     */
    void forEachTransition(WeightedEdgeConsumer action);

    default void forEachTransitionObject(BiConsumer<Integer, Double> action) {
        forEachTransition(action::accept);
    }

    /**
     * Samples a successor according to the weights.
     */
    int sample();

    /**
     * Samples a successor according to the weights except the given state. Returns -1 if s is the only successor.
     */
    default int sampleExcept(int state) {
        return sampleWeighted((s, p) -> s == state ? 0.0 : p);
    }

    /**
     * Samples a successor according to the weights except the given states. Returns -1 if the excluded states are
     * the only successors. If {@code except} is null, returns {@link #sample()}.
     */
    default int sampleExcept(@Nullable IntPredicate except) {
        return except == null ? sample() : sampleWeighted((s, p) -> except.test(s) ? 0.0 : p);
    }

    /**
     * Samples a successor, rescaling weights according to the given function. Returns -1 if all states are weighted
     * 0.0.
     */
    int sampleWeighted(WeightFunction weights);

    /**
     * Samples a successor except the given state, rescaling weights according to the given function. Returns -1 if the
     * only state with non-zero weights is the given state.
     */
    default int sampleWeightedExcept(WeightFunction weights, int state) {
        return sampleWeighted((s, p) -> s == state ? 0.0 : weights.accept(s, p));
    }

    /**
     * Samples a successor except the given states, rescaling weights according to the given function. Returns -1 if
     * the only states with non-zero weights are the given states.
     */
    default int sampleWeightedExcept(WeightFunction weights, IntPredicate except) {
        return sampleWeighted((s, p) -> except.test(s) ? 0.0 : weights.accept(s, p));
    }
}
