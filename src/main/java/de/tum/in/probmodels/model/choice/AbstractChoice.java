package de.tum.in.probmodels.model.choice;

abstract sealed class AbstractChoice<L> implements Choice<L> permits ArrayChoice, SingletonChoice {
    @Override
    public abstract int hashCode();

    @Override
    public abstract boolean equals(Object obj);

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("{");
        forEachTransition((t, p) -> builder.append(t).append(": ").append(p).append(", "));
        int length = builder.length();
        builder.delete(length - 2, length);
        builder.append('}');
        L label = label();
        if (label != null) {
            builder.append('@').append(label);
        }
        return builder.toString();
    }
}
