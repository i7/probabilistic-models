package de.tum.in.probmodels.model.choice;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.probmodels.util.FloatUtil;
import de.tum.in.probmodels.util.Sample;
import de.tum.in.probmodels.values.Bounds;
import it.unimi.dsi.fastutil.ints.AbstractInt2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntIterators;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.IntConsumer;
import java.util.function.IntFunction;
import java.util.function.IntPredicate;
import java.util.function.IntToDoubleFunction;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.annotation.Nullable;

abstract sealed class ArrayChoice<L> extends AbstractChoice<L> {
    protected final int[] successors;
    protected final double[] probabilities;
    protected final NatBitSet support;
    private int lazyHash = 0;

    @SuppressWarnings({"PMD.ArrayIsStoredDirectly", "AssignmentOrReturnOfFieldWithMutableType"})
    private ArrayChoice(int[] successors, double[] probabilities, NatBitSet support) {
        assert successors.length > 1;
        assert successors.length == probabilities.length
                : String.format("Size mismatch: %d vs %d", successors.length, probabilities.length);
        assert IntStream.range(0, successors.length - 1).allMatch(i -> successors[i] < successors[i + 1])
                : String.format("Non-ordered successors: %s", Arrays.toString(successors));
        assert Arrays.stream(successors).boxed().collect(Collectors.toSet()).equals(support)
                : String.format("Support mismatch: %s vs %s", support, Arrays.toString(successors));
        assert Arrays.stream(successors).allMatch(i -> i >= 0)
                : String.format("Negative successors: %s", Arrays.toString(successors));
        assert Arrays.stream(probabilities).allMatch(d -> d > 0.0)
                : String.format("Non-positive probabilities: %s", Arrays.toString(probabilities));
        assert FloatUtil.isOne(Arrays.stream(probabilities).sum())
                : String.format(
                        "Sum of probabilities is not one: %f",
                        Arrays.stream(probabilities).sum());

        this.successors = successors;
        this.probabilities = probabilities;
        this.support = support;
    }

    static <L> ArrayChoice<L> of(@Nullable L label, int[] successors, double[] probabilities, NatBitSet support) {
        return label == null
                ? new ArrayChoice.ArrayChoiceUnlabelled<>(successors, probabilities, support)
                : new ArrayChoice.ArrayChoiceLabelled<>(label, successors, probabilities, support);
    }

    @Override
    public double get(int successor) {
        int index = Arrays.binarySearch(successors, successor);
        return index >= 0 ? probabilities[index] : 0.0d;
    }

    @Override
    public boolean isOnlySuccessor(int successor) {
        return false;
    }

    @SuppressWarnings("AssignmentOrReturnOfFieldWithMutableType")
    @Override
    public NatBitSet successors() {
        return support;
    }

    @Override
    public IntIterator successorIterator() {
        return IntIterators.wrap(successors);
    }

    @Override
    public void forEachSuccessor(IntConsumer action) {
        for (int successor : successors) {
            action.accept(successor);
        }
    }

    @Override
    public double sumWeighted(double[] array) {
        int length = successors.length;
        int[] successors = this.successors;
        double[] probabilities = this.probabilities;

        if (length == 2) {
            return array[successors[0]] * probabilities[0] + array[successors[1]] * probabilities[1];
        }
        if (length == 3) {
            return FloatUtil.KahanSum.of(
                    array[successors[0]] * probabilities[0],
                    array[successors[1]] * probabilities[1],
                    array[successors[2]] * probabilities[2]);
        }
        FloatUtil.KahanSum sum = new FloatUtil.KahanSum(array[successors[0]] * probabilities[0]);
        for (int i = 1; i < length - 1; i++) {
            sum.add(array[successors[i]] * probabilities[i]);
        }
        return sum.add(array[successors[length - 1]] * probabilities[length - 1]);
    }

    @Override
    public double sumWeighted(IntToDoubleFunction f) {
        int length = successors.length;
        int[] successors = this.successors;
        double[] probabilities = this.probabilities;

        if (length == 2) {
            return f.applyAsDouble(successors[0]) * probabilities[0]
                    + f.applyAsDouble(successors[1]) * probabilities[1];
        }
        if (length == 3) {
            return FloatUtil.KahanSum.of(
                    f.applyAsDouble(successors[0]) * probabilities[0],
                    f.applyAsDouble(successors[1]) * probabilities[1],
                    f.applyAsDouble(successors[2]) * probabilities[2]);
        }
        FloatUtil.KahanSum sum = new FloatUtil.KahanSum(f.applyAsDouble(successors[0]) * probabilities[0]);
        for (int i = 1; i < length - 1; i++) {
            sum.add(f.applyAsDouble(successors[i]) * probabilities[i]);
        }
        return sum.add(f.applyAsDouble(successors[length - 1]) * probabilities[length - 1]);
    }

    @Override
    public Bounds sumWeightedBounds(IntFunction<Bounds> f) {
        int length = successors.length;
        int[] successors = this.successors;
        double[] probabilities = this.probabilities;

        if (length == 2) {
            Bounds one = f.apply(successors[0]);
            Bounds other = f.apply(successors[1]);
            double pOne = probabilities[0];
            double pOther = probabilities[1];
            return Bounds.of(
                    one.lowerBound() * pOne + other.lowerBound() * pOther,
                    one.upperBound() * pOne + other.upperBound() * pOther);
        }
        FloatUtil.KahanSum lower = new FloatUtil.KahanSum();
        FloatUtil.KahanSum upper = new FloatUtil.KahanSum();
        for (int i = 0; i < successors.length; i++) {
            Bounds bounds = f.apply(successors[i]);
            double p = probabilities[i];
            lower.add(bounds.lowerBound() * p);
            upper.add(bounds.upperBound() * p);
        }
        return Bounds.of(lower.get(), upper.get());
    }

    @Override
    public double[] sumWeightedExceptJacobi(double[][] array, int state) {
        int length = successors.length;
        int[] successors = this.successors;
        double[] probabilities = this.probabilities;

        if (length == 2) {
            double[] result = new double[array.length];

            int one = successors[0];
            int other = successors[1];
            if (one == state) {
                for (int j = 0; j < array.length; j++) {
                    result[j] = array[j][other];
                }
            } else if (other == state) {
                for (int j = 0; j < array.length; j++) {
                    result[j] = array[j][one];
                }
            } else {
                double pOne = probabilities[0];
                double pOther = probabilities[1];
                assert FloatUtil.isOne(pOne + pOther);
                for (int j = 0; j < array.length; j++) {
                    result[j] = array[j][one] * pOne + array[j][other] * pOther;
                }
            }
            return result;
        }

        double factor = Double.NaN;
        FloatUtil.KahanSum[] valueSum = new FloatUtil.KahanSum[array.length];
        Arrays.setAll(valueSum, i -> new FloatUtil.KahanSum());
        for (int i = 0; i < length; i++) {
            int s = successors[i];
            double p = probabilities[i];
            if (s == state) {
                assert Double.isNaN(factor);
                factor = p;
            } else {
                for (int j = 0; j < array.length; j++) {
                    valueSum[j].add(array[j][s] * p);
                }
            }
        }

        assert Double.isNaN(factor) || factor < 1.0;
        double[] result = new double[array.length];
        for (int j = 0; j < result.length; j++) {
            double sum = valueSum[j].get();
            result[j] = Double.isNaN(factor) ? sum : sum / (1 - factor);
        }
        return result;
    }

    @Override
    public double sumWeightedExceptJacobi(IntToDoubleFunction f, int state) {
        int length = successors.length;
        int[] successors = this.successors;
        double[] probabilities = this.probabilities;

        if (length == 2) {
            int one = successors[0];
            int other = successors[1];
            if (one == state) {
                return f.applyAsDouble(other);
            }
            if (other == state) {
                return f.applyAsDouble(one);
            }
            double pOne = probabilities[0];
            double pOther = probabilities[1];
            assert FloatUtil.isOne(pOne + pOther);
            return f.applyAsDouble(one) * pOne + f.applyAsDouble(other) * pOther;
        }

        double factor = Double.NaN;
        FloatUtil.KahanSum valueSum = new FloatUtil.KahanSum();
        for (int i = 0; i < successors.length; i++) {
            int s = successors[i];
            double p = probabilities[i];
            if (s == state) {
                assert Double.isNaN(factor);
                factor = p;
            } else {
                valueSum.add(f.applyAsDouble(s) * p);
            }
        }

        assert Double.isNaN(factor) || factor < 1.0;
        double sum = valueSum.get();
        return Double.isNaN(factor) ? sum : sum / (1.0 - factor);
    }

    @Override
    public double sumWeightedExceptJacobi(IntToDoubleFunction f, IntPredicate except) {
        int length = successors.length;
        int[] successors = this.successors;
        double[] probabilities = this.probabilities;

        if (length == 2) {
            int one = successors[0];
            int other = successors[1];
            if (except.test(one)) {
                return except.test(other) ? -1 : f.applyAsDouble(other);
            }
            if (except.test(other)) {
                return except.test(one) ? -1 : f.applyAsDouble(one);
            }
            double pOne = probabilities[0];
            double pOther = probabilities[1];
            assert FloatUtil.isOne(pOne + pOther);
            return f.applyAsDouble(one) * pOne + f.applyAsDouble(other) * pOther;
        }

        boolean any = false;
        FloatUtil.KahanSum valueSum = new FloatUtil.KahanSum();
        FloatUtil.KahanSum factorSum = new FloatUtil.KahanSum();
        for (int i = 0; i < successors.length; i++) {
            int s = successors[i];
            double p = probabilities[i];
            if (except.test(s)) {
                factorSum.add(p);
            } else {
                any = true;
                valueSum.add(f.applyAsDouble(s) * p);
            }
        }
        if (!any) {
            return Double.NaN;
        }
        double sum = valueSum.get();
        double factor = factorSum.get();
        return factor == 0.0 ? sum : sum / (1 - factor);
    }

    @Override
    public Optional<Bounds> sumWeightedExceptJacobiBounds(IntFunction<Bounds> f, IntPredicate except) {
        int length = successors.length;
        int[] successors = this.successors;
        double[] probabilities = this.probabilities;

        if (length == 2) {
            int one = successors[0];
            int other = successors[1];
            if (except.test(one)) {
                return except.test(other) ? Optional.empty() : Optional.of(f.apply(other));
            }
            if (except.test(other)) {
                return except.test(one) ? Optional.empty() : Optional.of(f.apply(one));
            }
            Bounds bOne = f.apply(one);
            double pOne = probabilities[0];
            Bounds bOther = f.apply(other);
            double pOther = probabilities[1];
            assert FloatUtil.isOne(pOne + pOther);
            return Optional.of(Bounds.of(
                    bOne.lowerBound() * pOne + bOther.lowerBound() * pOther,
                    bOne.upperBound() * pOne + bOther.upperBound() * pOther));
        }

        boolean any = false;
        FloatUtil.KahanSum upperSum = new FloatUtil.KahanSum();
        FloatUtil.KahanSum lowerSum = new FloatUtil.KahanSum();
        FloatUtil.KahanSum factorSum = new FloatUtil.KahanSum();
        for (int i = 0; i < length; i++) {
            int s = successors[i];
            double p = probabilities[i];
            if (except.test(s)) {
                factorSum.add(p);
            } else {
                any = true;
                Bounds bounds = f.apply(s);
                lowerSum.add(bounds.lowerBound() * p);
                upperSum.add(bounds.upperBound() * p);
            }
        }
        if (!any) {
            return Optional.empty();
        }
        double factor = factorSum.get();
        if (factor == 0.0) {
            return Optional.of(Bounds.of(lowerSum.get(), upperSum.get()));
        }
        double div = 1.0 - factor;
        return Optional.of(Bounds.of(lowerSum.get() / div, upperSum.get() / div));
    }

    @Override
    public int sample() {
        return successors[Sample.sampleProb(probabilities)];
    }

    @Override
    public int sampleWeighted(WeightFunction weights) {
        return sampleWeightedExcept(weights, s -> false);
    }

    @Override
    public int sampleWeightedExcept(WeightFunction weights, IntPredicate except) {
        int length = successors.length;
        int[] successors = this.successors;
        double[] probabilities = this.probabilities;

        double[] weightArray = new double[length];
        for (int i = 0; i < length; i++) {
            if (except.test(successors[i])) {
                continue;
            }
            double weight = weights.accept(successors[i], probabilities[i]);
            assert weight >= 0.0 : String.format("Negative weight for %d(%f)", successors[i], probabilities[i]);
            weightArray[i] = weight;
        }

        int sample = Sample.sample(weightArray);
        assert sample == -1 || weights.accept(successors[sample], probabilities[sample]) > 0.0
                : "Sampled %d from array %s".formatted(sample, Arrays.toString(weightArray));
        assert sample == -1 || !except.test(successors[sample]);
        return sample == -1 ? -1 : successors[sample];
    }

    @Override
    public Optional<Choice<L>> mapSuccessors(IntUnaryOperator map) {
        int[] successors = this.successors;
        double[] probabilities = this.probabilities;
        boolean remove = false;

        ChoiceBuilder<L> builder = null;
        for (int i = 0; i < successors.length; i++) {
            int key = map.applyAsInt(successors[i]);
            if (builder == null) {
                if (key == successors[i]) {
                    continue;
                }
                builder = new ChoiceBuilder<>(label());
                for (int j = 0; j < i; j++) {
                    builder.add(successors[j], probabilities[j]);
                }
            }
            if (key >= 0) {
                builder.add(key, probabilities[i]);
            } else {
                remove = true;
            }
        }
        if (builder == null) {
            return Optional.of(this);
        }
        return remove ? builder.scaled() : builder.build();
    }

    @Override
    public Optional<Choice<L>> removeSuccessor(int state) {
        int position = Arrays.binarySearch(successors, state);
        if (position < 0) {
            return Optional.of(this);
        }
        int[] successors = this.successors;
        int size = successors.length;

        if (size == 2) {
            assert successors[position] == state;
            return Optional.of(makeSingleton(successors[1 - position]));
        }

        double[] probabilities = this.probabilities;

        int[] newSuccessors = new int[size - 1];
        System.arraycopy(successors, 0, newSuccessors, 0, position);
        System.arraycopy(successors, position + 1, newSuccessors, position, size - position - 1);
        double[] newProbabilities = new double[size - 1];
        double scale = 1.0 - probabilities[position];
        for (int i = 0; i < position; i++) {
            newProbabilities[i] = probabilities[i] / scale;
        }
        for (int i = position; i < probabilities.length - 1; i++) {
            newProbabilities[i] = probabilities[i + 1] / scale;
        }
        NatBitSet support = NatBitSets.modifiableCopyOf(this.support);
        support.clear(position);

        return Optional.of(makeWith(newSuccessors, newProbabilities, support));
    }

    protected abstract SingletonChoice<L> makeSingleton(int successor);

    protected abstract ArrayChoice<L> makeWith(int[] newSuccessors, double[] newProbabilities, NatBitSet support);

    @Override
    public Optional<Choice<L>> removeSuccessors(IntSet remove) {
        return support.intersects(remove) ? removeSuccessors(remove::contains) : Optional.of(this);
    }

    @Override
    public Optional<Choice<L>> removeSuccessors(IntPredicate remove) {
        int[] successors = this.successors;
        double[] probabilities = this.probabilities;
        int size = successors.length;
        int newIndex = 0;
        int[] newSuccessors = new int[size];
        double[] newProbabilities = new double[size];

        for (int i = 0; i < size; i++) {
            int successor = successors[i];
            if (remove.test(successor)) {
                continue;
            }
            newSuccessors[newIndex] = successor;
            newProbabilities[newIndex] = probabilities[i];
            newIndex += 1;
        }
        if (newIndex == 0) {
            return Optional.empty();
        }
        if (newIndex == 1) {
            return Optional.of(makeSingleton(newSuccessors[0]));
        }
        if (newIndex == size) {
            return Optional.of(this);
        }
        int[] newSuccessorsCopy = Arrays.copyOf(newSuccessors, newIndex);
        var support = NatBitSets.set(newIndex, newSuccessorsCopy[newIndex - 1]);
        for (int successor : newSuccessorsCopy) {
            support.set(successor);
        }

        double scale = FloatUtil.KahanSum.of(newProbabilities, newIndex);
        double[] newProbabilitiesCopy = new double[newIndex];
        for (int i = 0; i < newIndex; i++) {
            newProbabilitiesCopy[i] = newProbabilities[i] / scale;
        }
        assert support.intStream().noneMatch(remove);
        return Optional.of(makeWith(newSuccessorsCopy, newProbabilitiesCopy, support));
    }

    @Override
    public void forEachTransition(WeightedEdgeConsumer action) {
        int[] successors = this.successors;
        double[] probabilities = this.probabilities;
        for (int i = 0; i < successors.length; i++) {
            action.accept(successors[i], probabilities[i]);
        }
    }

    @Override
    public boolean anySuccessorMatches(IntPredicate predicate) {
        for (int successor : successors) {
            if (predicate.test(successor)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean allSuccessorsMatch(IntPredicate predicate) {
        for (int successor : successors) {
            if (!predicate.test(successor)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Iterator<Int2DoubleMap.Entry> iterator() {
        return new EntryIterator(successors, probabilities);
    }

    @SuppressWarnings("NonFinalFieldReferencedInHashCode")
    @Override
    public final int hashCode() {
        if (lazyHash == 0) {
            lazyHash = computeHash();
        }
        return lazyHash;
    }

    @Override
    public abstract boolean equals(Object obj);

    final boolean compareDistribution(ArrayChoice<?> other) {
        if (this.lazyHash != 0 && other.lazyHash != 0 && this.lazyHash != other.lazyHash) {
            return false;
        }
        if (!support.equals(other.successors())) {
            return false;
        }
        assert Arrays.equals(successors, other.successors);
        for (int i = 0; i < successors.length; i++) {
            if (!FloatUtil.isEqualRel(probabilities[i], other.probabilities[i])) {
                return false;
            }
        }
        return true;
    }

    abstract int computeHash();

    private static class EntryIterator implements Iterator<Int2DoubleMap.Entry> {
        private final int[] keys;
        private final double[] probabilities;
        private int index = 0;

        @SuppressWarnings("PMD.ArrayIsStoredDirectly")
        EntryIterator(int[] keys, double[] probabilities) {
            this.keys = keys;
            this.probabilities = probabilities;
        }

        @Override
        public boolean hasNext() {
            return index < keys.length;
        }

        @Override
        public Int2DoubleMap.Entry next() {
            if (index == keys.length) {
                throw new NoSuchElementException();
            }
            var entry = new AbstractInt2DoubleMap.BasicEntry(keys[index], probabilities[index]);
            index += 1;
            return entry;
        }
    }

    @SuppressWarnings("PMD.OverrideBothEqualsAndHashcode")
    static final class ArrayChoiceUnlabelled<L> extends ArrayChoice<L> {
        ArrayChoiceUnlabelled(int[] successors, double[] probabilities, NatBitSet support) {
            super(successors, probabilities, support);
        }

        @Nullable
        @Override
        public L label() {
            return null;
        }

        @SuppressWarnings("unchecked")
        @Override
        public <K> Choice<K> withoutLabel() {
            return (Choice<K>) this;
        }

        @Override
        public <K> Choice<K> withLabel(K label) {
            return new ArrayChoiceLabelled<>(label, successors, probabilities, support);
        }

        @Override
        protected SingletonChoice<L> makeSingleton(int successor) {
            return new SingletonChoice.Unlabelled<>(successor);
        }

        @Override
        protected ArrayChoice<L> makeWith(int[] newSuccessors, double[] newProbabilities, NatBitSet support) {
            return new ArrayChoiceUnlabelled<>(newSuccessors, newProbabilities, support);
        }

        @Override
        public boolean equals(Object obj) {
            return obj == this
                    || obj instanceof ArrayChoice.ArrayChoiceUnlabelled<?> array && compareDistribution(array);
        }

        @Override
        int computeHash() {
            return support.hashCode();
        }
    }

    @SuppressWarnings("PMD.OverrideBothEqualsAndHashcode")
    static final class ArrayChoiceLabelled<L> extends ArrayChoice<L> {
        private final L label;

        ArrayChoiceLabelled(L label, int[] successors, double[] probabilities, NatBitSet support) {
            super(successors, probabilities, support);
            //noinspection ConstantValue
            assert label != null;
            this.label = label;
        }

        @Override
        public L label() {
            return label;
        }

        @Override
        public <K> Choice<K> withoutLabel() {
            return new ArrayChoiceUnlabelled<>(successors, probabilities, support);
        }

        @SuppressWarnings("unchecked")
        @Override
        public <K> Choice<K> withLabel(K label) {
            return label == this.label // NOPMD
                    ? (Choice<K>) this
                    : new ArrayChoiceLabelled<>(label, successors, probabilities, support);
        }

        @Override
        protected SingletonChoice<L> makeSingleton(int successor) {
            return new SingletonChoice.Labelled<>(label, successor);
        }

        @Override
        protected ArrayChoice<L> makeWith(int[] newSuccessors, double[] newProbabilities, NatBitSet support) {
            return new ArrayChoiceLabelled<>(label, newSuccessors, newProbabilities, support);
        }

        @Override
        public boolean equals(Object obj) {
            return obj == this
                    || obj instanceof ArrayChoice.ArrayChoiceLabelled<?> array
                            && Objects.equals(label, array.label)
                            && compareDistribution(array);
        }

        @Override
        int computeHash() {
            return support.hashCode() ^ label.hashCode();
        }
    }
}
