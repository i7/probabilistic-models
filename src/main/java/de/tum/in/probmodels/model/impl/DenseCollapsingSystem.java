package de.tum.in.probmodels.model.impl;

import static de.tum.in.probmodels.explorer.SelfLoopHandling.INLINE_LOOPS;
import static de.tum.in.probmodels.explorer.SelfLoopHandling.KEEP_LOOPS;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.naturals.unionfind.IntArrayUnionFind;
import de.tum.in.naturals.unionfind.IntUnionFind;
import de.tum.in.probmodels.explorer.SelfLoopHandling;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.ControlledSystem;
import de.tum.in.probmodels.model.EdgeMapper;
import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.HyperSystem;
import de.tum.in.probmodels.model.MappableMapper;
import de.tum.in.probmodels.model.MutableSystem;
import de.tum.in.probmodels.model.choice.Choice;
import de.tum.in.probmodels.util.NatBitSets2;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;

public class DenseCollapsingSystem<H extends HyperEdge> implements MutableSystem<H> {
    private final NatBitSet initialStates = NatBitSets.set();
    private final NatBitSet states = NatBitSets.set();
    private final NatBitSet definedStates = NatBitSets.set();

    private final NatBitSet antagonistStates = NatBitSets.set();
    // TODO Do we always want to track this or only compute this once an antagonist appears?
    private final NatBitSet protagonistStates = NatBitSets.set();

    private final Int2ObjectMap<List<H>> choices = new Int2ObjectOpenHashMap<>();
    private final Int2ObjectMap<NatBitSet> successors = new Int2ObjectOpenHashMap<>();
    private final Int2ObjectMap<NatBitSet> predecessors = new Int2ObjectOpenHashMap<>();
    private final IntUnionFind collapseUF = new IntArrayUnionFind(1024);
    private final EdgeMapper<H> mapper;
    private final IntFunction<H> singleton;

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static <L, K extends L> DenseCollapsingSystem<Choice<L>> lazyCopy(HyperSystem<Choice<K>> system) {
        if (system instanceof DenseCollapsingSystem<?> collapsing) {
            return (DenseCollapsingSystem<Choice<L>>) collapsing;
        }
        DenseCollapsingSystem<Choice<L>> internal = choiceBased();
        internal.addStates(system.states());
        if (system instanceof ControlledSystem<Choice<K>> controlled) {
            system.forEachState(s -> internal.setState(s, controlled.control(s), (Collection) system.choices(s)));
        } else {
            system.forEachState(s -> internal.setState(s, Actor.PROTAGONIST, (Collection) system.choices(s)));
        }
        internal.setInitialStates(system.initialStates());
        assert internal.checkConsistency();
        return internal;
    }

    public static <L> DenseCollapsingSystem<Choice<L>> choiceBased() {
        return new DenseCollapsingSystem<Choice<L>>(MappableMapper.instance(), Choice::singleton);
    }

    public DenseCollapsingSystem(EdgeMapper<H> mapper, IntFunction<H> singleton) {
        this.mapper = mapper;
        this.singleton = singleton;
    }

    @Override
    public NatBitSet states() {
        assert states.containsAll(definedStates);
        return states;
    }

    @Override
    public NatBitSet initialStates() {
        assert areDefinedStates(initialStates);
        return initialStates;
    }

    @Override
    public boolean isInitialState(int state) {
        assert isDefinedState(state);
        return initialStates.contains(state);
    }

    @Override
    public List<H> choices(int state) {
        assert isDefinedState(state);
        var choices = this.choices.get(state);
        assert !choices.isEmpty();
        assert choices.stream().allMatch(d -> d.isSubsetOf(states)) : state + " " + choices;
        return choices;
    }

    @Override
    public Actor control(int state) {
        assert isDefinedState(state);
        return antagonistStates.contains(state) ? Actor.ANTAGONIST : Actor.PROTAGONIST;
    }

    public EdgeMapper<H> mapper() {
        return mapper;
    }

    @Override
    public boolean hasAntagonistControlledStates() {
        return !antagonistStates.isEmpty();
    }

    @Override
    public NatBitSet antagonistControlledStates() {
        assert states.containsAll(antagonistStates);
        assert antagonistStates.intStream().allMatch(s -> choices(s).size() > 1);
        return antagonistStates;
    }

    @Override
    public NatBitSet protagonistControlledStates() {
        assert states.containsAll(protagonistStates);
        assert protagonistStates.intStream().allMatch(s -> choices(s).size() > 1);
        return protagonistStates;
    }

    @Override
    public NatBitSet successors(int state) {
        assert isDefinedState(state);
        return successors.get(state);
    }

    public int collapse(IntSet set, Actor control, SelfLoopHandling selfLoops) {
        collapse(Set.of(set), Function.identity(), s -> control, selfLoops);
        return representative(set.iterator().nextInt());
    }

    @Override
    public boolean isDefinedState(int state) {
        assert definedStates.contains(state) == successors.containsKey(state);
        assert successors.containsKey(state) == choices.containsKey(state);
        return definedStates.contains(state);
    }

    @Override
    public boolean areDefinedStates(IntSet states) {
        return definedStates.containsAll(states);
    }

    @Override
    public NatBitSet definedStates() {
        return definedStates;
    }

    public int representative(int state) {
        return collapseUF.applyAsInt(state);
    }

    public boolean equal(int state, int other) {
        return collapseUF.connected(state, other);
    }

    @Override
    public boolean containsState(int state) {
        assert predecessors.containsKey(state) == states.contains(state);
        return states.contains(state);
    }

    public int makeAbsorbing(IntSet set) {
        assert !set.isEmpty();
        assert areDefinedStates(set);

        NatBitSet successors = NatBitSets.set();
        NatBitSet predecessors = NatBitSets.set();

        var iterator = set.intIterator();
        int first = iterator.nextInt();
        successors.or(this.successors.remove(first));
        predecessors.or(this.predecessors.remove(first));
        choices.remove(first);
        while (iterator.hasNext()) {
            int s = iterator.nextInt();
            collapseUF.union(first, s);
            successors.or(this.successors.remove(s));
            predecessors.or(this.predecessors.remove(s));
            choices.remove(s);
        }
        successors.removeAll(set);
        predecessors.removeAll(set);

        int representative = collapseUF.find(first);
        antagonistStates.andNot(set);
        protagonistStates.andNot(set);
        if (initialStates.removeAll(set)) {
            initialStates.set(representative);
        }

        this.states.andNot(set);
        this.states.set(representative);
        this.definedStates.andNot(set);
        this.definedStates.set(representative);
        H selfLoop = singleton.apply(representative);
        assert selfLoop.isOnlySuccessor(representative);
        choices.put(representative, List.of(selfLoop));

        IntIterator successorIterator = successors.intIterator();
        while (successorIterator.hasNext()) {
            this.predecessors.get(successorIterator.nextInt()).andNot(set);
        }

        if (set.size() > 1) {
            IntIterator predecessorIterator = predecessors.intIterator();
            while (predecessorIterator.hasNext()) {
                int predecessor = predecessorIterator.nextInt();
                NatBitSet predecessorSuccessors = this.successors.get(predecessor);
                predecessorSuccessors.andNot(set);
                predecessorSuccessors.set(representative);
                remapChoices(predecessor, set);
            }
        }

        this.successors.put(representative, NatBitSets.singleton(representative));
        predecessors.set(representative);
        this.predecessors.put(representative, predecessors);

        assert checkConsistency();
        return representative;
    }

    public <C> void collapse(
            Collection<C> sets,
            Function<C, ? extends IntSet> statesMap,
            Function<C, Actor> control,
            SelfLoopHandling selfLoops) {
        NatBitSet removedStates = NatBitSets.set();
        NatBitSet newRepresentatives = NatBitSets.set();

        for (C set : sets) {
            var states = statesMap.apply(set);
            assert areDefinedStates(states);

            int representative;
            if (states.size() == 1) {
                representative = states.intIterator().nextInt();
            } else {
                var iterator = states.intIterator();
                int first = iterator.nextInt();
                while (iterator.hasNext()) {
                    collapseUF.union(first, iterator.nextInt());
                }
                representative = collapseUF.find(first);
            }

            assert states.contains(representative);
            assert containsStates(states);

            newRepresentatives.set(representative);

            if (states.size() > 1) {
                removedStates.or(states);
                removedStates.clear(representative);
                if (initialStates.intersects(states)) {
                    initialStates.andNot(states);
                    initialStates.set(representative);
                }
            }

            assert states.intStream()
                            .map(this::representative)
                            .boxed()
                            .distinct()
                            .count()
                    == 1L;
        }
        states.andNot(removedStates);
        definedStates.andNot(removedStates);
        antagonistStates.andNot(removedStates);
        protagonistStates.andNot(removedStates);
        assert states.containsAll(antagonistStates) && states.containsAll(protagonistStates);
        assert states.intStream().allMatch(s -> representative(s) == s);

        NatBitSet unstableStates = NatBitSets.set();
        for (C set : sets) {
            var states = statesMap.apply(set);
            int representative = representative(states.intIterator().nextInt());

            Collection<H> collapsedChoices = new HashSet<>();
            NatBitSet successors = NatBitSets.set();
            NatBitSet predecessors = NatBitSets.set();
            IntUnaryOperator map =
                    switch (selfLoops) {
                        case INLINE_LOOPS -> s -> {
                            int r = representative(s);
                            return r == representative ? -1 : r;
                        };
                        case KEEP_LOOPS -> this::representative;
                    };

            var iterator = states.intIterator();
            while (iterator.hasNext()) {
                int state = iterator.nextInt();
                for (H choice : choices.remove(state)) {
                    if (choice.someSuccessorIn(removedStates)) {
                        mapper.map(choice, map).ifPresent(collapsedChoices::add);
                    } else {
                        assert this.states.containsAll(choice.successors());
                        if (selfLoops == INLINE_LOOPS) {
                            mapper.remove(choice, representative).ifPresent(collapsedChoices::add);
                        } else {
                            collapsedChoices.add(choice);
                        }
                    }
                }
                successors.or(this.successors.remove(state));
                predecessors.or(this.predecessors.remove(state));
            }

            if (selfLoops == KEEP_LOOPS) {
                boolean selfLoop = successors.removeAll(states);
                assert selfLoop == predecessors.intersects(states);
                predecessors.andNot(states);
                if (collapsedChoices.isEmpty()) {
                    collapsedChoices.add(singleton.apply(representative));
                    selfLoop = true;
                }
                if (selfLoop) {
                    successors.set(representative);
                    predecessors.set(representative);
                }
                assert selfLoop == collapsedChoices.stream().anyMatch(c -> c.hasSuccessor(representative));
            } else {
                successors.andNot(states);
                predecessors.andNot(states);
                if (collapsedChoices.isEmpty()) {
                    collapsedChoices.add(singleton.apply(representative));
                    successors.set(representative);
                    predecessors.set(representative);
                } else {
                    assert collapsedChoices.stream().noneMatch(c -> c.hasSuccessor(representative));
                }

                assert (collapsedChoices.size() == 1
                                && collapsedChoices.iterator().next().isOnlySuccessor(representative))
                        || collapsedChoices.stream().noneMatch(c -> c.hasSuccessor(representative));
            }
            assert collapsedChoices.stream().noneMatch(c -> c.someSuccessorIn(removedStates))
                    : collapsedChoices.stream()
                            .filter(c -> c.someSuccessorIn(removedStates))
                            .map(Object::getClass)
                            .map(Objects::toString)
                            .collect(Collectors.toSet());
            assert !collapsedChoices.isEmpty();

            if (collapsedChoices.size() == 1) {
                protagonistStates.clear(representative);
                antagonistStates.clear(representative);
            } else {
                boolean protagonist = control.apply(set) == Actor.PROTAGONIST;
                protagonistStates.set(representative, protagonist);
                antagonistStates.set(representative, !protagonist);
            }

            this.choices.put(representative, new ArrayList<>(collapsedChoices));
            this.successors.put(representative, successors);
            this.predecessors.put(representative, predecessors);
            unstableStates.or(predecessors);

            if (states.size() > 1) {
                predecessors.forEach((int s) -> {
                    NatBitSet predecessorsSuccessors = this.successors.get(s);
                    predecessorsSuccessors.andNot(states);
                    predecessorsSuccessors.set(representative);
                });
                successors.forEach((int s) -> {
                    NatBitSet successorsPredecessors = this.predecessors.get(s);
                    successorsPredecessors.andNot(states);
                    successorsPredecessors.set(representative);
                });
            }
        }

        unstableStates.andNot(removedStates);
        unstableStates.andNot(newRepresentatives);
        if (!removedStates.isEmpty()) {
            var unstableIterator = unstableStates.intIterator();
            while (unstableIterator.hasNext()) {
                var state = unstableIterator.nextInt();
                assert containsState(state);
                assert state == representative(state);
                assert choices.get(state).stream()
                        .anyMatch(c -> c.someSuccessorIn(removedStates) || c.someSuccessorIn(newRepresentatives));
                remapChoices(state, removedStates);
            }
        }

        assert removedStates.intStream().noneMatch(this::isDefinedState);
        assert checkConsistency();
    }

    private void remapChoices(int state, IntSet modifiedStates) {
        var choices = this.choices.get(state);
        assert !choices.isEmpty();

        List<H> newChoices = new ArrayList<>();
        Set<H> collapsedChoices = new HashSet<>();

        for (H choice : choices) {
            if (!choice.someSuccessorIn(modifiedStates)) {
                assert mapper.map(choice, this::representative).orElseThrow().equals(choice);
                newChoices.add(choice);
                continue;
            }
            var mapped = mapper.map(choice, this::representative).orElseThrow();
            if (collapsedChoices.add(mapped)) {
                newChoices.add(mapped);
            }
        }

        assert !newChoices.isEmpty();
        assert newChoices.size() <= choices.size();
        if (newChoices.size() == 1) {
            protagonistStates.clear(state);
            antagonistStates.clear(state);
        }
        this.choices.put(state, newChoices);
    }

    public boolean checkConsistency() {
        assert states.containsAll(choices.keySet());
        assert states.intStream().allMatch(s -> s == representative(s));
        assert states.equals(predecessors.keySet());
        assert definedStates.intStream().mapToObj(this::choices).noneMatch(Collection::isEmpty);

        assert successors.keySet().intStream().allMatch(this::containsState);

        assert predecessors.keySet().containsAll(successors.keySet());
        assert predecessors.keySet().intStream().allMatch(this::containsState);

        assert choices.values().stream()
                .flatMap(Collection::stream)
                .map(HyperEdge::successors)
                .allMatch(states::containsAll);

        Int2ObjectMap<NatBitSet> successors = new Int2ObjectOpenHashMap<>();
        for (var entry : choices.int2ObjectEntrySet()) {
            successors.put(entry.getIntKey(), NatBitSets2.union(entry.getValue(), HyperEdge::successors));
        }
        assert successors.keySet().equals(this.successors.keySet());
        assert successors.equals(this.successors);

        Int2ObjectMap<NatBitSet> predecessors = new Int2ObjectOpenHashMap<>();
        for (var entry : successors.int2ObjectEntrySet()) {
            entry.getValue().forEach((int t) -> predecessors
                    .computeIfAbsent(t, k -> NatBitSets.set())
                    .set(entry.getIntKey()));
        }

        states.forEach((int s) -> {
            NatBitSet pre = this.predecessors.getOrDefault(s, NatBitSets.set());
            NatBitSet computed = predecessors.getOrDefault(s, NatBitSets.set());
            assert Objects.equals(pre, computed) : s + "\n" + pre + "\n" + computed;
        });

        assert states.containsAll(protagonistStates);
        assert states.containsAll(antagonistStates);
        assert !protagonistStates.intersects(antagonistStates);
        NatBitSet statesWithChoice = NatBitSets.set();
        this.choices.int2ObjectEntrySet().forEach(e -> {
            if (e.getValue().size() > 1) {
                statesWithChoice.set(e.getIntKey());
            }
        });
        assert statesWithChoice.containsAll(protagonistStates);
        assert statesWithChoice.containsAll(antagonistStates);
        statesWithChoice.andNot(protagonistStates);
        statesWithChoice.andNot(antagonistStates);
        assert statesWithChoice.isEmpty();

        return true;
    }

    private void ensureCapacity() {
        int ufSize = collapseUF.size();
        int stateCount = this.states.lastInt() + 1;
        if (ufSize < stateCount) {
            collapseUF.add(Math.max(stateCount - ufSize, ufSize));
        }
    }

    @Override
    public void addState(int state) {
        this.states.set(state);
        this.predecessors.put(state, NatBitSets.set());
        ensureCapacity();
    }

    @Override
    public void addStates(Collection<Integer> states) {
        if (this.states.addAll(states)) {
            states.forEach(i -> predecessors.putIfAbsent((int) i, NatBitSets.set()));
            ensureCapacity();
        }
    }

    @Override
    public void addInitialState(int state) {
        assert isDefinedState(state);
        this.initialStates.set(state);
    }

    @Override
    public void setInitialStates(Collection<Integer> initialStates) {
        assert initialStates.stream().allMatch(this::isDefinedState);
        this.initialStates.clear();
        this.initialStates.addAll(initialStates);
    }

    @Override
    public void setState(int state, Actor control, Collection<? extends H> actions) {
        assert states.contains(state);
        assert representative(state) == state;
        assert actions.stream().map(HyperEdge::successors).allMatch(this::containsStates);
        assert !actions.isEmpty();

        var successors = NatBitSets2.union(actions, H::successors);
        assert states.containsAll(successors);

        @SuppressWarnings("unchecked")
        List<H> choices = actions instanceof List<?> list ? (List<H>) list : new ArrayList<>(actions);
        var oldChoices = this.choices.put(state, choices);
        var oldSuccessors = this.successors.put(state, successors);
        boolean newState = oldSuccessors == null;
        assert newState == (oldChoices == null);
        assert newState == !definedStates.contains(state);

        boolean protagonist = control == Actor.PROTAGONIST;
        var successorIterator = successors.intIterator();
        if (newState) {
            definedStates.set(state);
            if (actions.size() > 1) {
                (protagonist ? protagonistStates : antagonistStates).set(state);
            }
            while (successorIterator.hasNext()) {
                this.predecessors.get(successorIterator.nextInt()).set(state);
            }
        } else {
            // It could happen that the owner of the state is changed or the number of actions!
            boolean hasChoice = actions.size() > 1;
            protagonistStates.set(state, hasChoice && protagonist);
            antagonistStates.set(state, hasChoice && !protagonist);

            while (successorIterator.hasNext()) {
                var successor = successorIterator.nextInt();
                if (oldSuccessors.contains(successor)) {
                    assert predecessors.get(successor).contains(state);
                } else {
                    this.predecessors.get(successor).set(state);
                }
            }

            oldSuccessors.andNot(successors);
            oldSuccessors.forEach((int successor) -> {
                var oldSuccessorPredecessors = this.predecessors.get(successor);
                if (oldSuccessorPredecessors != null) {
                    oldSuccessorPredecessors.clear(state);
                }
            });
        }

        assert actions.stream()
                .map(H::successors)
                .flatMapToInt(IntSet::intStream)
                .mapToObj(this.predecessors::get)
                .allMatch(pre -> pre.contains(state));
        assert this.predecessors.get(state).intStream().allMatch(this::containsState);
        assert this.predecessors
                .get(state)
                .intStream()
                .mapToObj(this.choices::get)
                .allMatch(cs -> cs.stream().map(H::successors).anyMatch(suc -> suc.contains(state)));
    }

    @Override
    public boolean allSuccessorsIn(int state, IntSet states) {
        assert isDefinedState(state);
        return states.containsAll(successors.get(state));
    }

    public NatBitSet predecessors(int state) {
        assert containsState(state);
        return this.predecessors.get(state);
    }
}
