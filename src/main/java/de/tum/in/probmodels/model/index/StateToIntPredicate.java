package de.tum.in.probmodels.model.index;

import java.util.function.IntFunction;
import java.util.function.IntPredicate;
import java.util.function.Predicate;

public record StateToIntPredicate<S>(Predicate<S> target, IntFunction<S> translation) implements IntPredicate {
    @Override
    public boolean test(int value) {
        return target.test(translation.apply(value));
    }

    @Override
    public String toString() {
        return target.toString();
    }
}
