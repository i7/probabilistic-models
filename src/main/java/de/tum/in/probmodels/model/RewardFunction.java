package de.tum.in.probmodels.model;

import de.tum.in.probmodels.values.Bounds;
import it.unimi.dsi.fastutil.ints.IntSet;
import javax.annotation.Nullable;

public interface RewardFunction {
    double stateReward(int state);

    double choiceReward(int state, @Nullable Object action);

    boolean nonZeroReward(int state);

    default boolean mayHaveChoiceRewards() {
        return true;
    }

    Bounds rewardBounds(IntSet states);
}
