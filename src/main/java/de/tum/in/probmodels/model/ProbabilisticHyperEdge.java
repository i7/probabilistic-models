package de.tum.in.probmodels.model;

/*
 * Marker interface, indicating that all weights are positive and sum up to one.
 */
public interface ProbabilisticHyperEdge extends WeightedHyperEdge {}
