package de.tum.in.probmodels.model.impl;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.HyperSystem;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;

public record QuotientModel<H extends HyperEdge, T extends HyperSystem<? super H>>(
        T model, IntFunction<NatBitSet> quotientToStates, IntUnaryOperator stateToQuotient) {}
