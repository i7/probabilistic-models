package de.tum.in.probmodels.model;

import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.PrimitiveIterator;
import java.util.function.IntConsumer;
import java.util.stream.IntStream;

public interface HasStateSet {
    IntSet states();

    default int stateCount() {
        return states().size();
    }

    default PrimitiveIterator.OfInt statesIterator() {
        return states().intIterator();
    }

    default IntStream statesStream() {
        return states().intStream();
    }

    default void forEachState(IntConsumer action) {
        statesIterator().forEachRemaining(action);
    }

    default boolean containsState(int state) {
        return states().contains(state);
    }

    default boolean containsStates(IntSet states) {
        return states().containsAll(states);
    }
}
