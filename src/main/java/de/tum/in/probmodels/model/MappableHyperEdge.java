package de.tum.in.probmodels.model;

import de.tum.in.probmodels.primitives.IntSetPredicate;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.Optional;
import java.util.function.IntPredicate;
import java.util.function.IntUnaryOperator;

public interface MappableHyperEdge<H> extends HyperEdge {
    /**
     * Creates a new edge by re-mapping successors according to the given map. This map may return -1 to remove
     * a state and map multiple states to the same new successor. Returns {@link Optional#empty()} if all successors
     * are removed.
     */
    Optional<H> mapSuccessors(IntUnaryOperator map);

    default Optional<H> removeSuccessor(int state) {
        return mapSuccessors(s -> s == state ? -1 : s);
    }

    default Optional<H> removeSuccessors(IntSet remove) {
        return removeSuccessors(remove::contains);
    }

    default Optional<H> removeSuccessors(IntPredicate remove) {
        if (remove instanceof IntSetPredicate set) {
            return removeSuccessors(set.set());
        }
        return mapSuccessors(s -> remove.test(s) ? -1 : s);
    }
}
