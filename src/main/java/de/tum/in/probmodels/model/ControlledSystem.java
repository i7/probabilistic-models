package de.tum.in.probmodels.model;

import it.unimi.dsi.fastutil.ints.IntSet;

public interface ControlledSystem<H extends HyperEdge> extends HyperSystem<H>, HasControl {
    @Override
    default Actor control(int state) {
        return hasAntagonistControlledStates() && antagonistControlledStates().contains(state)
                ? Actor.ANTAGONIST
                : Actor.PROTAGONIST;
    }

    boolean hasAntagonistControlledStates();

    IntSet antagonistControlledStates();

    IntSet protagonistControlledStates();
}
