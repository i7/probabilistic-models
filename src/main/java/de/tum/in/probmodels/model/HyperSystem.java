package de.tum.in.probmodels.model;

import java.util.List;

public interface HyperSystem<H extends HyperEdge> extends Hypergraph<H>, HasStateSet, HasInitialStates {
    /**
     * The set of choices available in this state. Never empty.
     */
    @Override
    List<H> choices(int state);

    default void forEachChoice(StateChoicesConsumer<? super H> action) {
        forEachState(s -> choices(s).forEach(c -> action.accept(s, c)));
    }

    default long choiceCount() {
        var iterator = states().intIterator();
        long count = 0;
        while (iterator.hasNext()) {
            count += choiceCount(iterator.nextInt());
        }
        return count;
    }

    default long transitionCount() {
        var iterator = states().intIterator();
        long count = 0;
        while (iterator.hasNext()) {
            for (H choice : choices(iterator.nextInt())) {
                count += choice.successorCount();
            }
        }
        return count;
    }

    @FunctionalInterface
    interface StateChoicesConsumer<H> {
        void accept(int state, H choice);
    }
}
