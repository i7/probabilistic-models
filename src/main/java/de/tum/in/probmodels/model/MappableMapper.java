package de.tum.in.probmodels.model;

import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.Optional;
import java.util.function.IntPredicate;
import java.util.function.IntUnaryOperator;

public final class MappableMapper<M extends MappableHyperEdge<M>> implements EdgeMapper<M> {
    private static final MappableMapper<?> INSTANCE = new MappableMapper<>();

    @SuppressWarnings("unchecked")
    public static <M extends MappableHyperEdge<M>> MappableMapper<M> instance() {
        return (MappableMapper<M>) INSTANCE;
    }

    private MappableMapper() {}

    @Override
    public Optional<M> map(M edge, IntUnaryOperator map) {
        return edge.mapSuccessors(map);
    }

    @Override
    public Optional<M> remove(M edge, int state) {
        return edge.removeSuccessor(state);
    }

    @Override
    public Optional<M> remove(M edge, IntSet remove) {
        return edge.removeSuccessors(remove);
    }

    @Override
    public Optional<M> remove(M edge, IntPredicate remove) {
        return edge.removeSuccessors(remove);
    }
}
