package de.tum.in.probmodels.model;

import com.google.common.collect.Iterables;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;

public interface HasChoice<C> {
    List<C> choices(int state);

    default C onlyChoice(int state) {
        return Iterables.getOnlyElement(choices(state));
    }

    default Optional<C> onlyChoiceIfPresent(int state) {
        var choices = choices(state);
        return choices.isEmpty() ? Optional.empty() : Optional.of(Iterables.getOnlyElement(choices));
    }

    default boolean isDeterministic() {
        return false;
    }

    default int choiceCount(int state) {
        return choices(state).size();
    }

    default Stream<? extends C> choicesStream(int state) {
        return choices(state).stream();
    }

    default Iterator<? extends C> choicesIterator(int state) {
        return choices(state).iterator();
    }

    default void forEachChoice(int state, Consumer<C> action) {
        choices(state).forEach(action);
    }
}
