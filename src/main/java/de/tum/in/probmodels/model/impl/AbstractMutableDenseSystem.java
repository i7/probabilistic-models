package de.tum.in.probmodels.model.impl;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.MutableDenseSystem;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.Collection;
import java.util.stream.IntStream;

/**
 * A dense system, i.e. all states from 0 to n. Moreover, the system is mutable, i.e. states and actions can be added.
 */
public abstract class AbstractMutableDenseSystem<H extends HyperEdge> implements MutableDenseSystem<H> {
    private final IntSet initialStates;
    private int stateCount = 0;
    private final NatBitSet states = NatBitSets.set();

    public AbstractMutableDenseSystem() {
        initialStates = new IntOpenHashSet();
    }

    protected AbstractMutableDenseSystem(AbstractMutableDenseSystem<? extends H> copy) {
        stateCount = copy.stateCount;
        initialStates = new IntOpenHashSet(copy.initialStates);
    }

    @Override
    public void addState(int state) {
        if (state >= stateCount) {
            makeStates(state - stateCount + 1);
        }
    }

    /**
     * Ensures that all states are present in this system. May create additional states.
     */
    @Override
    public void addStates(Collection<Integer> states) {
        int maximum = states instanceof IntCollection intCollection
                ? intCollection.intStream().max().orElse(0)
                : states.stream().mapToInt(Integer::intValue).max().orElse(0);
        if (maximum >= stateCount) {
            makeStates(maximum - stateCount + 1);
        }
    }

    /**
     * Creates a new state.
     */
    @Override
    public int makeState() {
        states.set(stateCount);
        stateCount += 1;
        return stateCount - 1;
    }

    /**
     * Creates several new states.
     */
    @Override
    public IntStream makeStates(int count) {
        states.set(stateCount, stateCount + count);
        stateCount += count;
        return IntStream.range(stateCount - count, stateCount);
    }

    /**
     * The number of states in this system.
     */
    @Override
    public int stateCount() {
        return stateCount;
    }

    @Override
    public IntSet states() {
        // TODO This is not really needed with newer versions of naturals-util
        return states;
    }

    /**
     * Add a state as initial state. This state has to be present in the model.
     */
    @Override
    public void addInitialState(int state) {
        assert containsState(state);
        initialStates.add(state);
    }

    /**
     * Sets the set of initial state. All states have to be present in the model.
     */
    @Override
    public void setInitialStates(Collection<Integer> initialStates) {
        assert initialStates.stream().allMatch(this::containsState);
        this.initialStates.clear();
        this.initialStates.addAll(initialStates);
    }

    @SuppressWarnings("AssignmentOrReturnOfFieldWithMutableType")
    @Override
    public IntSet initialStates() {
        return initialStates;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
