package de.tum.in.probmodels.model.impl;

import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.Hypergraph;
import de.tum.in.probmodels.util.Collections3;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.List;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import javax.annotation.Nullable;

public final class RestrictedGraphView<H extends HyperEdge> implements Hypergraph<H> {
    @Nullable
    private final IntFunction<Predicate<? super H>> restriction;

    @Nullable
    private final IntSet stateRestriction;

    private final Hypergraph<H> system;

    public static <H extends HyperEdge> Hypergraph<H> of(
            Hypergraph<H> graph, @Nullable IntFunction<Predicate<? super H>> restriction) {
        return restriction == null ? graph : new RestrictedGraphView<>(graph, restriction, null);
    }

    public static <H extends HyperEdge> Hypergraph<H> of(Hypergraph<H> graph, IntSet stateRestriction) {
        return new RestrictedGraphView<>(graph, null, stateRestriction);
    }

    private RestrictedGraphView(
            Hypergraph<H> system,
            @Nullable IntFunction<Predicate<? super H>> restriction,
            @Nullable IntSet stateRestriction) {
        this.restriction = restriction;
        this.system = system;
        this.stateRestriction = stateRestriction;
    }

    @Override
    public boolean isDeterministic() {
        return system.isDeterministic();
    }

    @Override
    public List<H> choices(int state) {
        assert stateRestriction == null || stateRestriction.contains(state);
        List<H> systemChoices = system.choices(state);
        if (systemChoices.isEmpty()) {
            return List.of();
        }
        Predicate<? super H> choiceFilter;
        if (restriction == null) {
            assert stateRestriction != null;
            choiceFilter = c -> c.isSubsetOf(stateRestriction);
        } else {
            Predicate<? super H> transitionFilter = restriction.apply(state);
            if (stateRestriction == null) {
                choiceFilter = transitionFilter;
            } else {
                choiceFilter = c -> c.isSubsetOf(stateRestriction) && transitionFilter.test(c);
            }
        }

        List<H> filter = Collections3.copyOnWriteFilter(systemChoices, choiceFilter);
        return filter == null ? systemChoices : filter;
    }
}
