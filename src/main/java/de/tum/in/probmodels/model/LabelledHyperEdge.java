package de.tum.in.probmodels.model;

import javax.annotation.Nullable;

public interface LabelledHyperEdge<L> extends HyperEdge {
    @Nullable
    L label();

    <K> LabelledHyperEdge<K> withoutLabel();

    <K> LabelledHyperEdge<K> withLabel(K label);
}
