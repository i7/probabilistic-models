package de.tum.in.probmodels.model;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.probmodels.util.FloatUtil;
import guru.nidi.graphviz.attribute.Arrow;
import guru.nidi.graphviz.attribute.Color;
import guru.nidi.graphviz.attribute.Label;
import guru.nidi.graphviz.attribute.Shape;
import guru.nidi.graphviz.attribute.Size;
import guru.nidi.graphviz.attribute.Style;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.Factory;
import guru.nidi.graphviz.model.Link;
import guru.nidi.graphviz.model.MutableNode;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntIterators;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.PrimitiveIterator;
import java.util.function.BiConsumer;
import java.util.function.IntFunction;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import javax.annotation.Nullable;

public final class ModelUtil {
    private ModelUtil() {}

    public static <H extends HyperEdge> IntFunction<? extends PrimitiveIterator.OfInt> restrictedIterator(
            Hypergraph<H> system, @Nullable IntFunction<Predicate<? super H>> transitionRestriction) {
        if (transitionRestriction == null) {
            return system::successorsIterator;
        }
        if (system.isDeterministic()) {
            return s -> {
                var optional = system.onlyChoiceIfPresent(s);
                if (optional.isEmpty()) {
                    return IntIterators.EMPTY_ITERATOR;
                }
                var distribution = optional.get();
                var filter = transitionRestriction.apply(s);
                return filter.test(distribution) ? distribution.successorIterator() : IntIterators.EMPTY_ITERATOR;
            };
        }
        return s -> {
            var choices = system.choices(s);
            if (choices.isEmpty()) {
                return IntIterators.EMPTY_ITERATOR;
            }
            var filter = transitionRestriction.apply(s);
            if (choices.size() == 1) {
                var distribution = choices.get(0);
                return filter.test(distribution) ? distribution.successorIterator() : IntIterators.EMPTY_ITERATOR;
            }

            NatBitSet union = NatBitSets.set();
            for (H choice : choices) {
                if (filter.test(choice)) {
                    union.or(choice.successors());
                }
            }
            return union.intIterator();
        };
    }

    public static <H extends WeightedHyperEdge, SYS extends Hypergraph<H> & HasStateSet> void modelToDotFile(
            Path destination,
            SYS model,
            IntFunction<Object> label,
            IntPredicate stateFilter,
            @Nullable BiConsumer<Integer, MutableNode> stateCustomizer) {
        var graph = Factory.mutGraph().setDirected(true);
        graph.nodeAttrs().add(Shape.BOX);

        for (int state : model.states()) {
            if (!stateFilter.test(state)) {
                continue;
            }

            String graphStateLabel = state
                    + Optional.ofNullable(label.apply(state)).map(s -> " " + s).orElse("");

            var node = Factory.mutNode(String.valueOf(state))
                    .add(Label.of(graphStateLabel))
                    .add(Style.FILLED)
                    .add(Color.GRAY.fill());
            if (stateCustomizer != null) {
                stateCustomizer.accept(state, node);
            }

            int actionIndex = 0;
            for (H choice : model.choices(state)) {
                Object actionLabel = (choice instanceof LabelledHyperEdge<?> labelled) ? labelled.label() : null;
                Label graphActionLabel = Label.of(actionIndex
                        + Optional.ofNullable(actionLabel).map(s -> ":" + s).orElse(""));
                if (choice.successorCount() == 1) {
                    IntIterator iterator = choice.successorIterator();
                    node.linkTo(Factory.node(String.valueOf(iterator.nextInt())))
                            .with(graphActionLabel);
                    assert !iterator.hasNext();
                } else {
                    var actionNode = Factory.mutNode(String.format("a%d_%d", state, actionIndex))
                            .add(Shape.POINT)
                            .add(Size.mode(Size.Mode.FIXED).size(0.1, 0.1));
                    node.addLink(Link.to(actionNode).with(Arrow.NONE).with(graphActionLabel));
                    choice.forEachTransition((target, probability) -> {
                        Link link = Link.to(Factory.node(String.valueOf(target)));
                        if (!FloatUtil.isEqual(probability, 1.0d)) {
                            link.with(Label.of(String.format("%.3f", probability)));
                        }
                        actionNode.addLink(link);
                    });

                    graph.add(actionNode);
                }
                actionIndex += 1;
            }
            graph.add(node);
        }
        try (OutputStream out = Files.newOutputStream(destination)) {
            Graphviz.fromGraph(graph).render(Format.DOT).toOutputStream(out);
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }
}
