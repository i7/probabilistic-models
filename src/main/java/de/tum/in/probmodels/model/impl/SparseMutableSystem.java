package de.tum.in.probmodels.model.impl;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.HyperEdge;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.Collection;
import java.util.List;

public class SparseMutableSystem<H extends HyperEdge> extends AbstractMutableSystem<H> {
    private final Int2ObjectMap<List<H>> transitions;
    private final NatBitSet antagonistControlledStates;
    private final NatBitSet protagonistControlledStates;

    public SparseMutableSystem() {
        transitions = new Int2ObjectOpenHashMap<>(1024);
        this.antagonistControlledStates = NatBitSets.set();
        this.protagonistControlledStates = NatBitSets.set();
    }

    @Override
    public List<H> choices(int state) {
        assert containsState(state);
        var choices = transitions.get(state);
        assert choices != null;
        return choices;
    }

    @Override
    public Actor control(int state) {
        return antagonistControlledStates.contains(state) ? Actor.ANTAGONIST : Actor.PROTAGONIST;
    }

    @Override
    public boolean hasAntagonistControlledStates() {
        return !antagonistControlledStates.isEmpty();
    }

    @Override
    public IntSet antagonistControlledStates() {
        return antagonistControlledStates;
    }

    @Override
    public IntSet protagonistControlledStates() {
        return protagonistControlledStates;
    }

    @Override
    public void setState(int state, Actor actor, Collection<? extends H> choices) {
        assert containsState(state);
        assert choices.stream().map(H::successors).allMatch(states()::containsAll);
        assert !choices.isEmpty() : "Empty action sets are not allowed";
        transitions.put(state, List.copyOf(choices));
        antagonistControlledStates.set(state, choices.size() > 1 && actor == Actor.ANTAGONIST);
        protagonistControlledStates.set(state, choices.size() > 1 && actor == Actor.PROTAGONIST);
    }

    @Override
    public boolean isDefinedState(int state) {
        return transitions.containsKey(state);
    }
}
