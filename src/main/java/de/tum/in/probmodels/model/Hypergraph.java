package de.tum.in.probmodels.model;

import com.google.common.collect.Lists;
import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import it.unimi.dsi.fastutil.ints.IntConsumer;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.Collection;
import java.util.List;
import java.util.PrimitiveIterator;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;

@FunctionalInterface
public interface Hypergraph<H extends HyperEdge> extends HasChoice<H> {
    default List<IntSet> choiceSuccessors(int state) {
        return Lists.transform(choices(state), HyperEdge::successors);
    }

    default IntSet successors(int state) {
        Collection<IntSet> sets = choiceSuccessors(state);
        if (sets.size() == 1) {
            return sets.iterator().next();
        }
        NatBitSet union = NatBitSets.set();
        sets.forEach(union::or);
        return union;
    }

    default int uniqueSuccessorCount(int state) {
        return successors(state).size();
    }

    default IntStream successorsStream(int state) {
        return successors(state).intStream();
    }

    default PrimitiveIterator.OfInt successorsIterator(int state) {
        return successors(state).iterator();
    }

    default void forEachSuccessor(int state, IntConsumer action) {
        for (H choice : choices(state)) {
            choice.forEachSuccessor(action);
        }
    }

    default void forEachUniqueSuccessor(int state, IntConsumer action) {
        successors(state).forEach(action);
    }

    default boolean anySuccessorMatches(int state, IntPredicate predicate) {
        for (H choice : choices(state)) {
            if (choice.anySuccessorMatches(predicate)) {
                return false;
            }
        }
        return true;
    }

    default boolean someSuccessorIn(int state, IntSet states) {
        for (H choice : choices(state)) {
            if (choice.someSuccessorIn(states)) {
                return true;
            }
        }
        return false;
    }

    default boolean allSuccessorsMatch(int state, IntPredicate predicate) {
        for (H choice : choices(state)) {
            if (!choice.allSuccessorsMatch(predicate)) {
                return false;
            }
        }
        return true;
    }

    default boolean allSuccessorsIn(int state, IntSet states) {
        for (H choice : choices(state)) {
            if (!choice.isSubsetOf(states)) {
                return false;
            }
        }
        return true;
    }

    default boolean anySupportIn(int state, IntSet states) {
        for (H choice : choices(state)) {
            if (choice.isSubsetOf(states)) {
                return true;
            }
        }
        return false;
    }
}
