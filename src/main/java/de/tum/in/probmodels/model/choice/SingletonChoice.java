package de.tum.in.probmodels.model.choice;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.probmodels.model.ProbabilisticHyperEdge;
import de.tum.in.probmodels.util.FloatUtil;
import de.tum.in.probmodels.values.Bounds;
import it.unimi.dsi.fastutil.HashCommon;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleMaps;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntIterators;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.function.IntConsumer;
import java.util.function.IntFunction;
import java.util.function.IntPredicate;
import java.util.function.IntToDoubleFunction;
import java.util.function.IntUnaryOperator;
import javax.annotation.Nullable;

abstract sealed class SingletonChoice<L> extends AbstractChoice<L> {
    protected final int successor;

    private SingletonChoice(int successor) {
        this.successor = successor;
    }

    static <L> SingletonChoice<L> of(@Nullable L label, int successor) {
        return label == null ? new Unlabelled<>(successor) : new Labelled<>(label, successor);
    }

    @Override
    public double get(int successor) {
        return successor == this.successor ? 1.0 : 0.0;
    }

    @Override
    public boolean isOnlySuccessor(int successor) {
        return successor == this.successor;
    }

    @Override
    public NatBitSet successors() {
        return NatBitSets.singleton(successor);
    }

    @Override
    public IntIterator successorIterator() {
        return IntIterators.singleton(successor);
    }

    @Override
    public void forEachSuccessor(IntConsumer action) {
        action.accept(successor);
    }

    @Override
    public double sumWeighted(double[] array) {
        return array[successor];
    }

    @Override
    public double sumWeighted(IntToDoubleFunction f) {
        return f.applyAsDouble(successor);
    }

    @Override
    public Bounds sumWeightedBounds(IntFunction<Bounds> f) {
        return f.apply(successor);
    }

    @Override
    public double[] sumWeightedExceptJacobi(double[][] array, int state) {
        double[] result = new double[array.length];
        if (state == successor) {
            return result;
        }
        for (int j = 0; j < result.length; j++) {
            double v = array[j][successor];
            assert FloatUtil.lessOrEqual(0.0, v) : String.format("Negative weight %f for %d", v, successor);
            result[j] = v;
        }
        return result;
    }

    @Override
    public double sumWeightedExceptJacobi(IntToDoubleFunction f, int state) {
        double v = state == successor ? Double.NaN : f.applyAsDouble(successor);
        assert Double.isNaN(v) || FloatUtil.lessOrEqual(0.0, v)
                : String.format("Negative weight %f for %d", v, successor);
        return v;
    }

    @Override
    public double sumWeightedExceptJacobi(IntToDoubleFunction f, IntPredicate except) {
        double v = except.test(successor) ? Double.NaN : f.applyAsDouble(successor);
        assert Double.isNaN(v) || FloatUtil.lessOrEqual(0.0, v)
                : String.format("Negative weight %f for %d", v, successor);
        return v;
    }

    @Override
    public Optional<Bounds> sumWeightedExceptJacobiBounds(IntFunction<Bounds> f, int state) {
        return state == successor ? Optional.empty() : Optional.of(f.apply(successor));
    }

    @Override
    public Optional<Bounds> sumWeightedExceptJacobiBounds(IntFunction<Bounds> f, IntPredicate except) {
        return except.test(successor) ? Optional.empty() : Optional.of(f.apply(successor));
    }

    @Override
    public int sample() {
        return successor;
    }

    @Override
    public int sampleExcept(int state) {
        return successor == state ? -1 : successor;
    }

    @Override
    public int sampleWeighted(WeightFunction weights) {
        return weights.accept(successor, 1.0) == 0.0 ? -1 : successor;
    }

    @Override
    public int sampleWeightedExcept(WeightFunction weights, int state) {
        return successor == state ? -1 : sampleWeighted(weights);
    }

    @Override
    public int sampleWeightedExcept(WeightFunction weights, @Nullable IntPredicate except) {
        return except == null || !except.test(successor) ? sampleWeighted(weights) : -1;
    }

    @Override
    public Optional<Choice<L>> mapSuccessors(IntUnaryOperator map) {
        int mapped = map.applyAsInt(successor);
        if (mapped == successor) {
            return Optional.of(this);
        }
        return mapped < 0 ? Optional.empty() : Optional.of(withSuccessor(mapped));
    }

    @Override
    public Optional<Choice<L>> removeSuccessor(int state) {
        return state == successor ? Optional.empty() : Optional.of(this);
    }

    @Override
    public Optional<Choice<L>> removeSuccessors(IntSet remove) {
        return remove.contains(successor) ? Optional.empty() : Optional.of(this);
    }

    @Override
    public Optional<Choice<L>> removeSuccessors(IntPredicate remove) {
        return remove.test(successor) ? Optional.empty() : Optional.of(this);
    }

    @Override
    public void forEachTransition(WeightedEdgeConsumer action) {
        action.accept(successor, 1.0);
    }

    @Override
    public boolean hasSuccessor(int successor) {
        return successor == this.successor;
    }

    @Override
    public int successorCount() {
        return 1;
    }

    @Override
    public boolean anySuccessorMatches(IntPredicate predicate) {
        return predicate.test(successor);
    }

    @Override
    public boolean allSuccessorsMatch(IntPredicate predicate) {
        return predicate.test(successor);
    }

    @Override
    public boolean isSubsetOf(IntSet set) {
        return set.contains(successor);
    }

    @Override
    public Iterator<Int2DoubleMap.Entry> iterator() {
        return Int2DoubleMaps.singleton(successor, 1.0).int2DoubleEntrySet().iterator();
    }

    protected abstract SingletonChoice<L> withSuccessor(int successor);

    static final class Unlabelled<L> extends SingletonChoice<L> {
        Unlabelled(int successor) {
            super(successor);
        }

        @Nullable
        @Override
        public L label() {
            return null;
        }

        @Override
        protected SingletonChoice<L> withSuccessor(int successor) {
            return new Unlabelled<>(successor);
        }

        @SuppressWarnings("unchecked")
        @Override
        public <K> Choice<K> withoutLabel() {
            return (Choice<K>) this;
        }

        @Override
        public <K> Choice<K> withLabel(K label) {
            return new Labelled<>(label, successor);
        }

        @Override
        public boolean equals(Object o) {
            return this == o || o instanceof SingletonChoice.Unlabelled<?> other && other.successor == successor;
        }

        @Override
        public int hashCode() {
            return HashCommon.murmurHash3(successor);
        }
    }

    static final class Labelled<L> extends SingletonChoice<L> {
        private final L label;

        Labelled(L label, int successor) {
            super(successor);
            //noinspection ConstantValue
            assert label != null;
            this.label = label;
        }

        @Override
        public L label() {
            return label;
        }

        @Override
        public <K> Choice<K> withoutLabel() {
            return new Unlabelled<>(successor);
        }

        @SuppressWarnings("unchecked")
        @Override
        public <K> Choice<K> withLabel(K label) {
            return label == this.label // NOPMD
                    ? (Choice<K>) this
                    : new Labelled<>(label, successor);
        }

        @Override
        public boolean equals(Object o) {
            assert !(o instanceof ProbabilisticHyperEdge) || o instanceof Choice<?>;
            return this == o
                    || o instanceof SingletonChoice.Labelled<?> other
                            && successor == other.successor
                            && Objects.equals(label(), other.label());
        }

        @Override
        public int hashCode() {
            return HashCommon.murmurHash3(successor) ^ label().hashCode();
        }

        @Override
        protected SingletonChoice<L> withSuccessor(int successor) {
            return new Labelled<>(label, successor);
        }
    }
}
