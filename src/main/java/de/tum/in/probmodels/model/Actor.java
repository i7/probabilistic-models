package de.tum.in.probmodels.model;

public enum Actor {
    // TODO Add third option for single-choice states -- This will require a lot of reworking

    PROTAGONIST,
    ANTAGONIST;

    public boolean isProtagonist() {
        return this == PROTAGONIST;
    }

    public Actor opponent() {
        return this == PROTAGONIST ? ANTAGONIST : PROTAGONIST;
    }
}
