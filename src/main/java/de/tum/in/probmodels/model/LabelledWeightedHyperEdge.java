package de.tum.in.probmodels.model;

public interface LabelledWeightedHyperEdge<L> extends WeightedHyperEdge, LabelledHyperEdge<L> {}
