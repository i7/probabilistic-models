package de.tum.in.probmodels.model.impl;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.HyperEdge;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class DenseNondeterministicSystem<H extends HyperEdge> extends AbstractMutableDenseSystem<H> {
    private List<H>[] transitions;
    private final NatBitSet antagonistControlledStates;
    private final NatBitSet protagonistControlledStates;

    @SuppressWarnings("unchecked")
    public DenseNondeterministicSystem() {
        transitions = new List[1024];
        this.antagonistControlledStates = NatBitSets.set();
        this.protagonistControlledStates = NatBitSets.set();
    }

    @SuppressWarnings("unchecked")
    public DenseNondeterministicSystem(DenseNondeterministicSystem<? extends H> copy) {
        super(copy);
        transitions = new List[copy.transitions.length];
        Arrays.setAll(transitions, i -> new ArrayList<>(copy.transitions[i]));
        this.antagonistControlledStates = NatBitSets.modifiableCopyOf(copy.antagonistControlledStates);
        this.protagonistControlledStates = NatBitSets.modifiableCopyOf(copy.protagonistControlledStates);
    }

    private void ensureSpace(int required) {
        if (transitions.length <= required) {
            transitions = Arrays.copyOf(transitions, Math.max(transitions.length * 2, required + 1));
        }
    }

    @Override
    public List<H> choices(int state) {
        assert containsState(state);
        List<H> choices = transitions.length <= state ? null : transitions[state];
        assert choices != null;
        return choices;
    }

    @Override
    public boolean isDefinedState(int state) {
        return state < transitions.length && transitions[state] != null;
    }

    @Override
    public Actor control(int state) {
        return antagonistControlledStates.contains(state) ? Actor.ANTAGONIST : Actor.PROTAGONIST;
    }

    @Override
    public boolean hasAntagonistControlledStates() {
        return !antagonistControlledStates.isEmpty();
    }

    @Override
    public IntSet antagonistControlledStates() {
        return antagonistControlledStates;
    }

    @Override
    public IntSet protagonistControlledStates() {
        return protagonistControlledStates;
    }

    @Override
    public boolean isDeterministic() {
        return false;
    }

    @Override
    public void setState(int state, Actor actor, Collection<? extends H> choices) {
        assert containsState(state);
        assert choices.stream().map(H::successors).allMatch(states()::containsAll);
        assert !choices.isEmpty() : "Empty action sets are not allowed";
        ensureSpace(state);
        transitions[state] = List.copyOf(choices);
        antagonistControlledStates.set(state, choices.size() > 1 && actor == Actor.ANTAGONIST);
        protagonistControlledStates.set(state, choices.size() > 1 && actor == Actor.PROTAGONIST);
    }
}
