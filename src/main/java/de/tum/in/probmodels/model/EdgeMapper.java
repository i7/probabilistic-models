package de.tum.in.probmodels.model;

import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.Optional;
import java.util.function.IntPredicate;
import java.util.function.IntUnaryOperator;

public interface EdgeMapper<H extends HyperEdge> {
    /**
     * Creates a new edge by re-mapping successors according to the given map. This map may return -1 to remove
     * a state and map multiple states to the same new successor. Returns {@code Optional.absent()} if all successors
     * are removed.
     */
    Optional<? extends H> map(H edge, IntUnaryOperator map);

    default Optional<? extends H> remove(H edge, int state) {
        return remove(edge, IntSet.of(state));
    }

    default Optional<? extends H> remove(H edge, IntSet remove) {
        return remove(edge, remove::contains);
    }

    default Optional<? extends H> remove(H edge, IntPredicate remove) {
        return map(edge, s -> remove.test(s) ? -1 : s);
    }
}
