package de.tum.in.probmodels.model.impl;

import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.HyperEdge;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.ints.IntSets;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class DenseDeterministicSystem<H extends HyperEdge> extends AbstractMutableDenseSystem<H> {
    private Object[] transitions;

    public DenseDeterministicSystem() {
        transitions = new Object[1024];
    }

    public DenseDeterministicSystem(DenseDeterministicSystem<? extends H> copy) {
        super(copy);
        transitions = Arrays.copyOf(copy.transitions, copy.transitions.length);
    }

    private void ensureSpace(int required) {
        if (transitions.length <= required) {
            transitions = Arrays.copyOf(transitions, Math.max(transitions.length * 2, required + 1));
        }
    }

    @SuppressWarnings("unchecked")
    public H choice(int state) {
        assert containsState(state);
        H choice = transitions.length < state ? null : (H) transitions[state];
        assert choice != null;
        return choice;
    }

    @Override
    public List<H> choices(int state) {
        return List.of(choice(state));
    }

    @Override
    public Actor control(int state) {
        return Actor.PROTAGONIST;
    }

    @Override
    public boolean hasAntagonistControlledStates() {
        return false;
    }

    @Override
    public IntSet antagonistControlledStates() {
        return IntSets.emptySet();
    }

    @Override
    public IntSet protagonistControlledStates() {
        return IntSets.emptySet();
    }

    @Override
    public IntSet successors(int state) {
        assert containsState(state);
        return choice(state).successors();
    }

    @Override
    public boolean someSuccessorIn(int state, IntSet states) {
        assert containsState(state);
        return choice(state).someSuccessorIn(states);
    }

    @Override
    public boolean allSuccessorsIn(int state, IntSet states) {
        assert containsState(state);
        return choice(state).isSubsetOf(states);
    }

    @Override
    public boolean isDeterministic() {
        return true;
    }

    @Override
    public void setState(int state, Actor actor, Collection<? extends H> choices) {
        assert containsState(state);
        assert choices.stream().map(HyperEdge::successors).allMatch(states()::containsAll);
        assert choices.size() == 1
                : "Deterministic model may only have exactly one choice per state, received %s".formatted(choices);
        assert actor == Actor.PROTAGONIST;
        ensureSpace(state);
        transitions[state] = choices.iterator().next();
    }

    @Override
    public boolean isDefinedState(int state) {
        return state < transitions.length && transitions[state] != null;
    }

    @Override
    public int choiceCount(int state) {
        assert containsState(state);
        return 1;
    }
}
