package de.tum.in.probmodels.model;

import java.util.Optional;

public interface WeightedEdgeBuilder<W extends WeightedHyperEdge> {
    void add(int state, double weight);

    void set(int state, double weight);

    boolean isEmpty();

    Optional<W> build();
}
