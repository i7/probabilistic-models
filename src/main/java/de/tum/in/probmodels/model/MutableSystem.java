package de.tum.in.probmodels.model;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.ints.IntSets;
import java.util.Collection;

public interface MutableSystem<H extends HyperEdge> extends ControlledSystem<H> {
    default void addState(int state) {
        addStates(IntSets.singleton(state));
    }

    void addStates(Collection<Integer> states);

    void setState(int state, Actor control, Collection<? extends H> actions);

    void addInitialState(int state);

    void setInitialStates(Collection<Integer> initialStates);

    boolean isDefinedState(int state);

    default boolean areDefinedStates(IntSet states) {
        var iterator = states.intIterator();
        while (iterator.hasNext()) {
            if (!isDefinedState(iterator.nextInt())) {
                return false;
            }
        }
        return true;
    }

    default IntSet definedStates() {
        IntSet set = new IntOpenHashSet(states());
        set.removeIf(s -> !isDefinedState(s));
        return set;
    }

    default IntSet undefinedStates() {
        IntSet set = new IntOpenHashSet();
        states().forEach((int s) -> {
            if (!isDefinedState(s)) {
                set.add(s);
            }
        });
        return set;
    }
}
