package de.tum.in.probmodels.model.impl;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.ControlledSystem;
import de.tum.in.probmodels.model.HyperEdge;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.List;
import java.util.Set;
import java.util.function.IntFunction;
import javax.annotation.Nullable;

public final class RestrictedSystemView<H extends HyperEdge, SYS extends ControlledSystem<H>>
        implements ControlledSystem<H> {
    private final SYS system;
    private final IntSet states;
    private final IntSet initialStates;
    private final IntFunction<List<H>> stateActions;

    @Nullable
    private NatBitSet antagonistStates;

    @Nullable
    private NatBitSet protagonistStates;

    public static <H extends HyperEdge, SYS extends ControlledSystem<H>> RestrictedSystemView<H, SYS> of(
            SYS system, IntSet states, IntFunction<List<H>> stateActions, IntSet initialStates) {
        return new RestrictedSystemView<>(system, states, stateActions, initialStates);
    }

    private RestrictedSystemView(SYS system, IntSet states, IntFunction<List<H>> stateActions, IntSet initialStates) {
        assert states.containsAll(initialStates);
        assert states.intStream().allMatch(s -> stateActions.apply(s).stream()
                .map(HyperEdge::successors)
                .allMatch(states::containsAll));

        this.system = system;
        this.states = states;
        this.initialStates = initialStates;
        this.stateActions = stateActions;
        if (!system.hasAntagonistControlledStates()) {
            this.antagonistStates = NatBitSets.emptySet();
        }
    }

    public SYS model() {
        return system;
    }

    @Override
    public IntSet states() {
        return states;
    }

    @Override
    public IntSet initialStates() {
        return initialStates;
    }

    @Override
    public boolean isDeterministic() {
        return system.isDeterministic();
    }

    @Override
    public List<H> choices(int state) {
        assert states.contains(state);
        List<H> choices = stateActions.apply(state);
        assert !choices.isEmpty();
        assert Set.copyOf(system.choices(state)).containsAll(choices);
        return choices;
    }

    @Override
    public Actor control(int state) {
        return system.control(state);
    }

    @Override
    public boolean hasAntagonistControlledStates() {
        if (antagonistStates == null) {
            return system.hasAntagonistControlledStates()
                    && NatBitSets.intersects(states, system.antagonistControlledStates());
        }
        return antagonistStates.isEmpty();
    }

    @Override
    public IntSet antagonistControlledStates() {
        if (antagonistStates == null) {
            this.antagonistStates = NatBitSets.copyOf(states);
            antagonistStates.and(system.antagonistControlledStates());
        }
        return antagonistStates;
    }

    @Override
    public IntSet protagonistControlledStates() {
        if (protagonistStates == null) {
            this.protagonistStates = NatBitSets.copyOf(states);
            protagonistStates.and(system.protagonistControlledStates());
        }
        return protagonistStates;
    }
}
