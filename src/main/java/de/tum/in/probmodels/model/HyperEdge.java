package de.tum.in.probmodels.model;

import de.tum.in.naturals.set.NatBitSets;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.function.IntConsumer;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;

public interface HyperEdge {
    /*
     * Set of successors. Never empty.
     */
    IntSet successors();

    default IntIterator successorIterator() {
        return successors().intIterator();
    }

    default IntStream successorStream() {
        return successors().intStream();
    }

    default boolean hasSuccessor(int successor) {
        return successors().contains(successor);
    }

    default int successorCount() {
        return successors().size();
    }

    /**
     * Is this successor the only successor (i.e. {@link #successors() successors} {@code == {successor}})?
     */
    default boolean isOnlySuccessor(int successor) {
        var successors = successors();
        return successors.contains(successor) && successors.size() == 1;
    }

    default void forEachSuccessor(IntConsumer action) {
        successors().forEach(action);
    }

    /**
     * Returns whether any successor of this edge is in the given set.
     */
    default boolean someSuccessorIn(IntSet states) {
        return NatBitSets.intersects(successors(), states);
    }

    /**
     * Returns whether any successor of this edge matches the given predicate.
     */
    default boolean anySuccessorMatches(IntPredicate predicate) {
        IntIterator iterator = successors().intIterator();
        while (iterator.hasNext()) {
            if (predicate.test(iterator.nextInt())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns whether all successor of this edge are in the given set.
     */
    default boolean isSubsetOf(IntSet states) {
        return states.containsAll(successors());
    }

    /**
     * Returns whether all successor of this edge match the given predicate.
     */
    default boolean allSuccessorsMatch(IntPredicate predicate) {
        IntIterator iterator = successors().intIterator();
        while (iterator.hasNext()) {
            if (!predicate.test(iterator.nextInt())) {
                return false;
            }
        }
        return true;
    }
}
