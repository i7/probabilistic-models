package de.tum.in.probmodels.model.impl;

public enum Owner {
    SYSTEM,
    ENVIRONMENT;

    public Owner opponent() {
        return switch (this) {
            case SYSTEM -> ENVIRONMENT;
            case ENVIRONMENT -> SYSTEM;
        };
    }
}
