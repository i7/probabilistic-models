package de.tum.in.probmodels.model.index;

import de.tum.in.naturals.map.Nat2ObjectDenseArrayMap;
import de.tum.in.probmodels.model.MutableDenseSystem;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import java.util.Set;
import java.util.function.ToIntFunction;

public final class DefaultStateBijection<S> implements StateIndexBijection<S> {
    private final Object2IntMap<S> stateMap;
    private final Int2ObjectMap<S> indexMap;
    private final ToIntFunction<S> indexProvider;

    public static <S> DefaultStateBijection<S> withIndices(ToIntFunction<S> indexProvider) {
        Object2IntMap<S> stateMap = new Object2IntOpenHashMap<>();
        stateMap.defaultReturnValue(-1);
        return new DefaultStateBijection<>(stateMap, new Int2ObjectOpenHashMap<>(), indexProvider);
    }

    public static <S> DefaultStateBijection<S> forEmptyDenseSystem(MutableDenseSystem<?> system) {
        assert system.states().isEmpty();
        return withIndices(s -> system.makeState());
    }

    public static <S> DefaultStateBijection<S> increasing() {
        Object2IntMap<S> stateMap = new Object2IntOpenHashMap<>();
        stateMap.defaultReturnValue(-1);
        Int2ObjectMap<S> indexMap = new Nat2ObjectDenseArrayMap<>(1024);
        return new DefaultStateBijection<>(stateMap, indexMap, s -> stateMap.size());
    }

    private DefaultStateBijection(
            Object2IntMap<S> stateMap, Int2ObjectMap<S> indexMap, ToIntFunction<S> indexProvider) {
        this.stateMap = stateMap;
        this.indexMap = indexMap;
        this.indexProvider = indexProvider;
    }

    @Override
    public int addState(S state) {
        int stateId = indexProvider.applyAsInt(state);
        assert !indexMap.containsKey(stateId);
        assert !stateMap.containsKey(state);
        stateMap.put(state, stateId);
        indexMap.put(stateId, state);
        return stateId;
    }

    @Override
    public int addIfAbsent(S state) {
        return stateMap.computeIfAbsent(state, (S k) -> {
            int index = indexProvider.applyAsInt(k);
            S previous = indexMap.put(index, k);
            assert previous == null;
            return index;
        });
    }

    @Override
    public int addIfAbsent(S state, StateIndexConsumer<S> action) {
        return stateMap.computeIfAbsent(state, (S k) -> {
            int index = indexProvider.applyAsInt(k);
            S previous = indexMap.put(index, k);
            assert previous == null : "Index %d for %s held %s".formatted(index, k, previous);
            action.accept(k, index);
            return index;
        });
    }

    @Override
    public int indexOf(S state) {
        return stateMap.getInt(state);
    }

    public boolean check(int stateId) {
        return indexMap.containsKey(stateId) && stateMap.getInt(indexMap.get(stateId)) == stateId;
    }

    @Override
    public boolean contains(S state) {
        return stateMap.containsKey(state);
    }

    public Set<S> getStates() {
        return stateMap.keySet();
    }

    @Override
    public S getState(int stateId) {
        assert indexMap.containsKey(stateId);
        return indexMap.get(stateId);
    }

    public int size() {
        return indexMap.size();
    }

    @Override
    public void forEach(StateIndexConsumer<S> consumer) {
        for (Object2IntMap.Entry<S> entry : stateMap.object2IntEntrySet()) {
            consumer.accept(entry.getKey(), entry.getIntValue());
        }
    }

    @Override
    public Set<S> states() {
        return stateMap.keySet();
    }
}
