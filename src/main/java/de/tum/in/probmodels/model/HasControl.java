package de.tum.in.probmodels.model;

public interface HasControl {
    Actor control(int state);
}
