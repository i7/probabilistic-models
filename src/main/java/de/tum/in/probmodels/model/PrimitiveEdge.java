package de.tum.in.probmodels.model;

import it.unimi.dsi.fastutil.ints.IntSet;

public record PrimitiveEdge(IntSet successors) implements HyperEdge {
    public static HyperEdge of(IntSet successors) {
        return new PrimitiveEdge(successors);
    }
}
