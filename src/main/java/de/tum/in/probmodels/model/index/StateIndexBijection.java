package de.tum.in.probmodels.model.index;

public interface StateIndexBijection<S> extends StateIndexer<S> {
    S getState(int stateId);
}
