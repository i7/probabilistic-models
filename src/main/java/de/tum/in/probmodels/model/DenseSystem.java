package de.tum.in.probmodels.model;

import de.tum.in.naturals.set.NatBitSets;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.stream.IntStream;

public interface DenseSystem<H extends HyperEdge> extends HyperSystem<H> {
    @Override
    default IntSet states() {
        return NatBitSets.boundedFullSet(stateCount());
    }

    @Override
    default IntStream statesStream() {
        return IntStream.range(0, stateCount());
    }

    @Override
    default boolean containsState(int state) {
        return 0 <= state && state < stateCount();
    }
}
