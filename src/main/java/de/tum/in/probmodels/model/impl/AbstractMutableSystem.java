package de.tum.in.probmodels.model.impl;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.MutableSystem;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.util.Collection;

/**
 * A dense system, i.e. all states from 0 to n. Moreover, the system is mutable, i.e. states and actions can be added.
 */
public abstract class AbstractMutableSystem<H extends HyperEdge> implements MutableSystem<H> {
    private final IntSet initialStates;
    private final NatBitSet states = NatBitSets.set();

    public AbstractMutableSystem() {
        initialStates = new IntOpenHashSet();
    }

    /**
     * Ensures that all states are present in this system. May create additional states.
     */
    @Override
    public void addStates(Collection<Integer> states) {
        this.states.addAll(states);
    }

    @Override
    public void addState(int state) {
        states.set(state);
    }

    /**
     * The number of states in this system.
     */
    @Override
    public int stateCount() {
        return states.size();
    }

    @Override
    public IntSet states() {
        return states;
    }

    /**
     * Add a state as initial state. This state has to be present in the model.
     */
    @Override
    public void addInitialState(int state) {
        assert containsState(state);
        initialStates.add(state);
    }

    /**
     * Sets the set of initial state. All states have to be present in the model.
     */
    @Override
    public void setInitialStates(Collection<Integer> initialStates) {
        assert initialStates.stream().allMatch(this::containsState);
        this.initialStates.clear();
        this.initialStates.addAll(initialStates);
    }

    @SuppressWarnings("AssignmentOrReturnOfFieldWithMutableType")
    @Override
    public IntSet initialStates() {
        return initialStates;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
