package de.tum.in.probmodels.model;

import de.tum.in.naturals.set.NatBitSet;
import de.tum.in.naturals.set.NatBitSets;
import java.util.function.IntFunction;

public final class Attractor {
    private Attractor() {}

    public static <SYS extends Hypergraph<?> & HasControl> boolean addAttractor(
            NatBitSet states, SYS system, IntFunction<NatBitSet> predecessors, Actor actor) {
        NatBitSet unstable = NatBitSets.set();
        states.forEach((int s) -> unstable.or(predecessors.apply(s)));
        unstable.andNot(states);

        NatBitSet current = unstable;
        NatBitSet next = NatBitSets.set();
        boolean any = false;

        while (!current.isEmpty()) {
            var iterator = current.intIterator();
            while (iterator.hasNext()) {
                int s = iterator.nextInt();
                boolean addState =
                        actor == system.control(s) ? system.anySupportIn(s, states) : system.allSuccessorsIn(s, states);
                if (addState) {
                    assert !states.contains(s);
                    any = true;
                    states.set(s);
                    next.or(predecessors.apply(s));
                }
            }
            current.clear();
            next.andNot(states);

            NatBitSet swap = current;
            current = next;
            next = swap;
        }
        return any;
    }
}
