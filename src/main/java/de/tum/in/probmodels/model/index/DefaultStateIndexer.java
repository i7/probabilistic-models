package de.tum.in.probmodels.model.index;

import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import java.util.Set;
import java.util.function.ToIntFunction;

public final class DefaultStateIndexer<S> implements StateIndexer<S> {
    private final Object2IntMap<S> stateMap = new Object2IntOpenHashMap<>();
    private final ToIntFunction<S> indexProvider;

    public DefaultStateIndexer(ToIntFunction<S> indexProvider) {
        this.indexProvider = indexProvider;
        stateMap.defaultReturnValue(-1);
    }

    @Override
    public int addState(S state) {
        assert !stateMap.containsKey(state);
        int stateId = indexProvider.applyAsInt(state);
        stateMap.put(state, stateId);
        return stateId;
    }

    @Override
    public int addIfAbsent(S state) {
        return stateMap.computeIfAbsent(state, indexProvider);
    }

    @Override
    public int addIfAbsent(S state, StateIndexConsumer<S> action) {
        return stateMap.computeIfAbsent(state, (S k) -> {
            int index = indexProvider.applyAsInt(k);
            action.accept(k, index);
            return index;
        });
    }

    @Override
    public int indexOf(S state) {
        assert stateMap.containsKey(state);
        return stateMap.getInt(state);
    }

    @Override
    public boolean contains(S state) {
        return stateMap.containsKey(state);
    }

    @Override
    public void forEach(StateIndexConsumer<S> consumer) {
        for (Object2IntMap.Entry<S> entry : stateMap.object2IntEntrySet()) {
            consumer.accept(entry.getKey(), entry.getIntValue());
        }
    }

    @Override
    public Set<S> states() {
        return stateMap.keySet();
    }
}
