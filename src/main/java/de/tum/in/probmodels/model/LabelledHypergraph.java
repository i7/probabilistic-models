package de.tum.in.probmodels.model;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Nullable;

public interface LabelledHypergraph<L, H extends LabelledHyperEdge<L>> extends Hypergraph<H> {
    default List<H> choicesByLabel(int state, @Nullable Object label) {
        return choicesStream(state)
                .filter(c -> Objects.equals(c.label(), label))
                .collect(Collectors.toList());
    }
}
