package de.tum.in.probmodels.impl.prism;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkElementIndex;
import static de.tum.in.probmodels.explorer.ModelBuilder.DuplicateStateIndices.UNIQUE_INDICES;
import static de.tum.in.probmodels.explorer.SelfLoopHandling.KEEP_LOOPS;

import com.google.common.base.Stopwatch;
import de.tum.in.probmodels.explorer.IncreasingIndexProvider;
import de.tum.in.probmodels.explorer.ModelBuilder;
import de.tum.in.probmodels.generator.Generator;
import de.tum.in.probmodels.generator.RewardGenerator;
import de.tum.in.probmodels.graph.ComponentAnalyser;
import de.tum.in.probmodels.impl.prism.generator.ContinuousTimeEmbeddingGenerator;
import de.tum.in.probmodels.impl.prism.generator.ContinuousTimeUniformizingModel;
import de.tum.in.probmodels.impl.prism.generator.NondeterministicGenerator;
import de.tum.in.probmodels.impl.prism.generator.PrismGenerator;
import de.tum.in.probmodels.impl.prism.generator.StochasticGenerator;
import de.tum.in.probmodels.model.choice.Choice;
import de.tum.in.probmodels.model.impl.AbstractMutableDenseSystem;
import de.tum.in.probmodels.model.impl.DenseDeterministicSystem;
import de.tum.in.probmodels.model.impl.DenseNondeterministicSystem;
import de.tum.in.probmodels.model.index.DefaultStateIndexer;
import de.tum.in.probmodels.output.ComponentsStatistics;
import de.tum.in.probmodels.output.DefaultStatistics;
import de.tum.in.probmodels.output.ModelStatistics;
import de.tum.in.probmodels.problem.Problem;
import de.tum.in.probmodels.problem.property.Quantity;
import de.tum.in.probmodels.problem.property.UntilType;
import de.tum.in.probmodels.problem.query.Comparison;
import de.tum.in.probmodels.problem.query.Optimization;
import de.tum.in.probmodels.problem.query.OptimizingQuery;
import de.tum.in.probmodels.problem.query.QualitativeQuery;
import de.tum.in.probmodels.problem.query.Query;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.ints.IntSets;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import org.tinylog.Logger;
import parser.PrismParser;
import parser.State;
import parser.Values;
import parser.ast.Coalition;
import parser.ast.Expression;
import parser.ast.ExpressionQuant;
import parser.ast.ExpressionReward;
import parser.ast.ExpressionStrategy;
import parser.ast.ExpressionTemporal;
import parser.ast.ExpressionUnaryOp;
import parser.ast.LabelList;
import parser.ast.ModulesFile;
import parser.ast.PropertiesFile;
import parser.ast.RelOp;
import parser.ast.RewardStruct;
import prism.OpRelOpBound;
import prism.PrismLangException;
import prism.UndefinedConstants;
import simulator.ModulesFileModelGenerator;

public class PrismProblemInstance {
    private static final Pattern modelBaseNameRegex = Pattern.compile("^([a-zA-Z]*).*$");

    private final List<PrismRewards> rewards;
    private final List<PrismProperty> expressions;
    private final Values values;

    private final ModulesFile modulesFile;
    private final ModulesFileModelGenerator<Double> prismGenerator;

    @Nullable
    private final Double uniformization;

    public static PrismProblemInstance of(
            Path modelPath, @Nullable Path propertiesPath, List<String> constants, @Nullable Double uniformization)
            throws IOException {
        return new PrismProblemInstance(modelPath, propertiesPath, constants, uniformization);
    }

    @Nullable
    private static Path detectPropertiesPath(Path modelPath, @Nullable Path propertiesPath) {
        if (propertiesPath == null) {
            String name = modelPath.getFileName().toString();
            if (name.endsWith(".prism")) {
                String basename = name.substring(0, name.length() - 6) + ".props";
                Path basenamePath = modelPath.resolveSibling(basename);
                if (Files.exists(basenamePath)) { // NOPMD
                    return basenamePath;
                }

                Matcher matcher = modelBaseNameRegex.matcher(name);
                if (matcher.matches()) { // NOPMD
                    String withoutValues = matcher.group(1);
                    Path withoutValuesPath = modelPath.resolveSibling(withoutValues + ".props");
                    if (Files.exists(withoutValuesPath)) { // NOPMD
                        return withoutValuesPath;
                    }
                }
            }
            return null;
        }

        if ("skip".equals(propertiesPath.toString())) {
            return null;
        }

        if (Files.exists(propertiesPath)) {
            return propertiesPath;
        }

        Path siblingPath = modelPath.resolveSibling(propertiesPath);
        if (Files.exists(siblingPath)) {
            return siblingPath;
        }

        return null;
    }

    PrismProblemInstance(
            Path modelPath, @Nullable Path propertiesPath, List<String> constants, @Nullable Double uniformization)
            throws IOException {
        PrismParser parser = new PrismParser();

        ModulesFile loadedModulesFile;
        try (InputStream in = new BufferedInputStream(Files.newInputStream(modelPath))) {
            loadedModulesFile = parser.parseModulesFile(in, null);
        }
        loadedModulesFile.tidyUp();

        Path derivedPropertiesPath = detectPropertiesPath(modelPath, propertiesPath);
        if (!Objects.equals(derivedPropertiesPath, propertiesPath)) {
            Logger.debug("Derived properties path {}", derivedPropertiesPath);
        }
        @Nullable PropertiesFile loadedPropertiesFile;
        if (derivedPropertiesPath == null) {
            loadedPropertiesFile = null;
        } else {
            Logger.debug("Loading properties file from {}", derivedPropertiesPath);
            try (InputStream in = new BufferedInputStream(Files.newInputStream(derivedPropertiesPath))) {
                loadedPropertiesFile = parser.parsePropertiesFile(loadedModulesFile, in);
            }
            loadedPropertiesFile.tidyUp();
        }

        UndefinedConstants undefinedConstants = new UndefinedConstants(loadedModulesFile, loadedPropertiesFile);
        if (constants.isEmpty()) {
            undefinedConstants.checkAllDefined();
        } else {
            undefinedConstants.defineUsingConstSwitch(String.join(",", constants));
        }
        loadedModulesFile.setSomeUndefinedConstants(undefinedConstants.getMFConstantValues());
        // loadedModulesFile.expandConstants(loadedModulesFile.getConstantList(), false);
        undefinedConstants.iterateModel();

        this.prismGenerator = new ModulesFileModelGenerator<>(loadedModulesFile);
        List<PrismRewards> rewardsList = new ArrayList<>();
        for (int i = 0; i < loadedModulesFile.getNumRewardStructs(); i++) {
            RewardStruct rewardStruct = loadedModulesFile.getRewardStruct(i);
            rewardsList.add(new PrismRewards(
                    rewardStruct.getName(), new PrismRewardGenerator(rewardStruct.getName(), i, prismGenerator)));
        }

        List<PrismProperty> expressionList = new ArrayList<>();
        if (loadedPropertiesFile != null) {
            loadedPropertiesFile.setSomeUndefinedConstants(undefinedConstants.getPFConstantValues());
            // loadedPropertiesFile.expandConstants(loadedPropertiesFile.getConstantList(), false);
            LabelList combinedLabelList = loadedPropertiesFile.getCombinedLabelList();

            for (int i = 0; i < loadedPropertiesFile.getNumProperties(); i++) {
                parser.ast.Property propertyObject = loadedPropertiesFile.getPropertyObject(i);
                expressionList.add(new PrismProperty(
                        propertyObject.getName(), propertyObject.getComment(), (Expression) propertyObject
                                .getExpression()
                                .deepCopy()
                                .expandPropRefsAndLabels(loadedPropertiesFile, combinedLabelList)
                                .replaceConstants(loadedModulesFile.getConstantValues())
                                .replaceConstants(loadedPropertiesFile.getConstantValues())
                                .simplify()));
            }
        }
        undefinedConstants.iterateProperty();

        this.modulesFile = loadedModulesFile;

        this.expressions = List.copyOf(expressionList);
        this.rewards = List.copyOf(rewardsList);
        this.values = loadedModulesFile.getConstantValues();
        this.uniformization = uniformization;
    }

    public ModelStatistics modelStatistics(boolean analyseComponents) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        var generator = standardModel();
        AbstractMutableDenseSystem<Choice<Object>> model =
                (this.modulesFile.getModelType().nondeterministic()
                        ? new DenseNondeterministicSystem<>()
                        : new DenseDeterministicSystem<>());

        ModelBuilder.buildLabelled(
                model,
                generator,
                KEEP_LOOPS,
                UNIQUE_INDICES,
                new DefaultStateIndexer<>(new IncreasingIndexProvider<>(model::addState)));
        Duration constructionTime = stopwatch.elapsed();

        var states = model.stateCount();
        var choices = model.choiceCount();
        var transitions = model.transitionCount();

        @Nullable ComponentsStatistics componentsStatistics;
        if (analyseComponents) {
            Stopwatch componentTimer = Stopwatch.createStarted();
            var components = ComponentAnalyser.defaultAnalyser(model).findComponentStates(model);
            var componentDuration = componentTimer.elapsed();
            componentsStatistics = DefaultStatistics.extract(components, componentDuration);
        } else {
            componentsStatistics = null;
        }

        return new ModelStatistics.Timed(states, choices, transitions, constructionTime, componentsStatistics);
    }

    public Problem<State, Object> meanPayoffFromReward(String rewardPropertyName) {
        var property = findProperty(rewardPropertyName);
        var expressionWithAntagonist = parseAntagonists(property.expression);
        var expression = expressionWithAntagonist.expression();
        var antagonisticPlayers = expressionWithAntagonist.antagonists();

        if (!(expression instanceof ExpressionReward reward)) {
            throw new IllegalArgumentException("Need reward expression");
        }
        OpRelOpBound boundInfo = reward.getRelopBoundInfo(values);
        PrismRewards prismRewards = findRewards(reward.getRewardStructIndex().toString());
        var quantity = new PrismMeanPayoffQuantity(prismRewards.generator(), Optional.empty());
        return new Problem<>(
                "mean-payoff[%s]".formatted(rewardPropertyName),
                generatorForAntagonists(antagonisticPlayers),
                query(boundInfo, !antagonisticPlayers.isEmpty()),
                quantity);
    }

    private PrismProperty findProperty(String name) {
        return expressions.stream()
                .filter(p -> name.equals(p.name()))
                .findAny()
                .or(() -> {
                    try {
                        int index = Integer.parseInt(name) - 1;
                        checkArgument(0 <= index && index < expressions.size(), "No property with index %s", index + 1);
                        return Optional.of(expressions.get(index));
                    } catch (NumberFormatException e) {
                        return Optional.empty();
                    }
                })
                .orElseThrow(() -> new NoSuchElementException("No property found for name %s".formatted(name)));
    }

    public Problem<State, Object> problemByName(String name) {
        return makeProblem(findProperty(name));
    }

    private IntSet antagonisticPlayers(Collection<String> coalition) {
        List<String> playerList = modulesFile.getPlayerNames();
        Map<String, Integer> playerIndices = new HashMap<>(playerList.size());
        ListIterator<String> playerIterator = playerList.listIterator();
        while (playerIterator.hasNext()) {
            int next = playerIterator.nextIndex();
            playerIndices.put(playerIterator.next(), next);
        }

        IntSet antagonisticPlayers = new IntOpenHashSet(playerIndices.values());
        for (String player : coalition) {
            if (playerIndices.containsKey(player)) {
                antagonisticPlayers.remove((int) playerIndices.get(player));
            } else {
                int playerIndex;
                try {
                    playerIndex = Integer.parseInt(player) - 1;
                    checkElementIndex(playerIndex, playerList.size());
                } catch (NumberFormatException | IndexOutOfBoundsException e) {
                    throw new IllegalArgumentException(
                            "Unknown player %s in coalition, known: %s".formatted(player, playerIndices.keySet()), e);
                }
                antagonisticPlayers.remove(playerIndex);
            }
        }
        return antagonisticPlayers;
    }

    public Generator<State, Object> standardModel() {
        return generatorForAntagonists(IntSets.emptySet());
    }

    private PrismRewards findRewards(String name) {
        return rewards.stream()
                .filter(p -> name.equals(p.name()))
                .findAny()
                .or(() -> {
                    try {
                        return Optional.of(rewards.get(Integer.parseInt(name) - 1));
                    } catch (NumberFormatException e) {
                        return Optional.empty();
                    }
                })
                .orElseThrow(() -> new NoSuchElementException("No rewards found for name %s".formatted(name)));
    }

    public Problem<State, Object> meanPayoff(
            String rewardName, RelOp boundInfo, @Nullable Collection<String> coalition) {
        PrismRewards reward = findRewards(rewardName);
        IntSet antagonisticPlayers = coalition == null ? IntSets.emptySet() : antagonisticPlayers(coalition);
        Query query = query(new OpRelOpBound("R", boundInfo, null), !antagonisticPlayers.isEmpty());
        return new Problem<>(
                "mean-payoff[%s]".formatted(rewardName),
                generatorForAntagonists(antagonisticPlayers),
                query,
                new PrismMeanPayoffQuantity(reward.generator(), Optional.empty()));
    }

    public RewardGenerator<State> rewardGenerator(String name) {
        return findRewards(name).generator;
    }

    private static int parseBound(Expression expression) {
        try {
            return expression.evaluateInt();
        } catch (PrismLangException e) {
            double doubleBound = expression.evaluateDouble();
            //noinspection FloatingPointEquality
            if (Math.rint(doubleBound) != doubleBound) {
                throw e;
            }
            //noinspection NumericCastThatLosesPrecision
            return (int) doubleBound;
        }
    }

    private static Function<State, UntilType> reachFunction(ExpressionTemporal expression) {
        Expression left = expression.getOperand1() == null
                ? Expression.True()
                : (Expression) expression.getOperand1().simplify();
        Expression right = (Expression) expression.getOperand2().simplify();
        if (right.equals(Expression.False())) {
            return s -> UntilType.SINK;
        }
        PrismExpressionWrapper goal = new PrismExpressionWrapper(right);
        if (left.equals(Expression.True())) {
            return s -> goal.test(s) ? UntilType.GOAL : UntilType.SAFE;
        }
        PrismExpressionWrapper safety = new PrismExpressionWrapper(left);
        return s -> {
            if (goal.test(s)) {
                return UntilType.GOAL;
            }
            return safety.test(s) ? UntilType.SAFE : UntilType.SINK;
        };
    }

    private PrismGenerator generatorForAntagonists(IntSet antagonisticPlayers) {
        return switch (modulesFile.getModelType()) {
            case CTMC, CTMDP -> uniformization == null || uniformization.isNaN() || uniformization == 0.0
                    ? new ContinuousTimeEmbeddingGenerator(prismGenerator)
                    : new ContinuousTimeUniformizingModel(prismGenerator, uniformization);
            case DTMC -> new StochasticGenerator(prismGenerator, false);
            case SMG, MDP -> new NondeterministicGenerator(prismGenerator, antagonisticPlayers, false);
            default -> throw new IllegalArgumentException();
        };
    }

    private record ExpressionWithAntagonist(Expression expression, IntSet antagonists) {}

    private ExpressionWithAntagonist parseAntagonists(Expression expression) {
        if (expression instanceof ExpressionStrategy expressionStrategy) {
            checkArgument(expressionStrategy.getNumCoalitions() == 1, "Only one coalition supported");
            checkArgument(expressionStrategy.getNumOperands() == 1, "Only one operand supported");

            Coalition coalition = expressionStrategy.getCoalition();
            return new ExpressionWithAntagonist(
                    expressionStrategy.getOperand(0), antagonisticPlayers(coalition.getPlayers()));
        }
        return new ExpressionWithAntagonist(expression, IntSet.of());
    }

    private Problem<State, Object> makeProblem(PrismProperty prismProperty) {
        var expressionWithAntagonist = parseAntagonists(prismProperty.expression);
        var expression = expressionWithAntagonist.expression();
        var antagonisticPlayers = expressionWithAntagonist.antagonists();

        if (expression instanceof ExpressionQuant expressionQuant) {
            checkArgument(
                    expressionQuant.getExpression().isSimplePathFormula(),
                    "Property %s is not a simple path formula",
                    expression);

            Quantity<State> quantity;

            OpRelOpBound boundInfo = expressionQuant.getRelopBoundInfo(values);

            if (expressionQuant.getExpression() instanceof ExpressionTemporal temporal) {
                int lowerBound = 0;
                if (temporal.getLowerBound() != null) {
                    lowerBound = parseBound(temporal.getLowerBound());
                    if (temporal.lowerBoundIsStrict()) {
                        lowerBound += 1;
                    }
                }

                int upperBound = Integer.MAX_VALUE;
                if (temporal.getUpperBound() != null) {
                    upperBound = parseBound(temporal.getUpperBound());
                    if (temporal.upperBoundIsStrict()) {
                        upperBound -= 1;
                    }
                }

                checkArgument(0 <= lowerBound);
                checkArgument(lowerBound <= upperBound);

                OptionalInt upper = upperBound == Integer.MAX_VALUE ? OptionalInt.empty() : OptionalInt.of(upperBound);

                // TODO Until form, simple cases (lhs = true/false, rhs = true/false ...)
                switch (temporal.getOperator()) {
                    case ExpressionTemporal.P_U,
                            ExpressionTemporal.P_F,
                            ExpressionTemporal.P_G,
                            ExpressionTemporal.P_R,
                            ExpressionTemporal.P_W -> {
                        Expression untilForm = temporal.convertToUntilForm();
                        checkArgument(untilForm.isSimplePathFormula(), "LTL expressions not supported");
                        boolean negated;
                        ExpressionTemporal reachExpression;
                        if (untilForm instanceof ExpressionUnaryOp unary) {
                            assert unary.getOperator() == ExpressionUnaryOp.NOT;
                            negated = true;
                            reachExpression = (ExpressionTemporal) unary.getOperand();
                        } else {
                            negated = false;
                            reachExpression = (ExpressionTemporal) untilForm;
                        }
                        Expression left =
                                (Expression) reachExpression.getOperand1().simplify();
                        Expression right =
                                (Expression) reachExpression.getOperand2().simplify();
                        reachExpression = new ExpressionTemporal(ExpressionTemporal.P_U, left, right);
                        Logger.debug(
                                "Expression {} converted to {} ({})",
                                temporal,
                                reachExpression,
                                negated ? "negated" : "not negated");
                        Function<State, UntilType> reach = reachFunction(reachExpression);
                        quantity = new PrismUntilQuantity(lowerBound, upper, reach, temporal, negated);
                    }
                    case ExpressionTemporal.R_C, ExpressionTemporal.R_F, ExpressionTemporal.R_Fc -> {
                        ExpressionReward reward = (ExpressionReward) expressionQuant;
                        if (reward.getDiscount() != null) {
                            throw new UnsupportedOperationException("Discounted reward not supported");
                        }
                        Function<State, UntilType> reachability;
                        if (temporal.getOperator() == ExpressionTemporal.R_F
                                || temporal.getOperator() == ExpressionTemporal.R_Fc) {
                            reachability = reachFunction(temporal);
                        } else {
                            assert temporal.getOperator() == ExpressionTemporal.R_C;
                            reachability = s -> UntilType.SAFE;
                        }

                        PrismRewards prismRewards =
                                findRewards(reward.getRewardStructIndex().toString());
                        quantity = new PrismTotalRewardQuantity(
                                lowerBound, upper, prismRewards.generator(), reachability, temporal, Optional.empty());
                    }
                    default -> throw new UnsupportedOperationException(
                            "Unsupported temporal operator %s".formatted(temporal.getOperator()));
                }
            } else {
                throw new UnsupportedOperationException("Unsupported expression %s (%s)"
                        .formatted(expressionQuant, expressionQuant.getClass().getCanonicalName()));
            }

            boolean antagonist = !antagonisticPlayers.isEmpty();

            return new Problem<>(
                    prismProperty.name,
                    generatorForAntagonists(antagonisticPlayers),
                    query(boundInfo, antagonist),
                    quantity);
        } else {
            throw new UnsupportedOperationException("Could not construct a predicate from %s (%s)"
                    .formatted(expression, expression.getClass().getCanonicalName()));
        }
    }

    private Query query(OpRelOpBound relOp, boolean hasAntagonist) {
        return switch (relOp.getRelOp()) {
            case GT -> new QualitativeQuery(
                    Comparison.GREATER_THAN,
                    relOp.getBound(),
                    hasAntagonist ? Optimization.MAX_MIN_VALUE : Optimization.MIN_VALUE);
            case GEQ -> new QualitativeQuery(
                    Comparison.GREATER_OR_EQUAL,
                    relOp.getBound(),
                    hasAntagonist ? Optimization.MAX_MIN_VALUE : Optimization.MIN_VALUE);
            case LT -> new QualitativeQuery(
                    Comparison.LESS_THAN,
                    relOp.getBound(),
                    hasAntagonist ? Optimization.MIN_MAX_VALUE : Optimization.MAX_VALUE);
            case LEQ -> new QualitativeQuery(
                    Comparison.LESS_OR_EQUAL,
                    relOp.getBound(),
                    hasAntagonist ? Optimization.MIN_MAX_VALUE : Optimization.MAX_VALUE);
            case MIN, MINMIN -> new OptimizingQuery(
                    hasAntagonist ? Optimization.MIN_MAX_VALUE : Optimization.MIN_VALUE);
            case MAX, MAXMAX -> new OptimizingQuery(
                    hasAntagonist ? Optimization.MAX_MIN_VALUE : Optimization.MAX_VALUE);
            case EQ -> new OptimizingQuery(Optimization.UNIQUE_VALUE);
            case MAXMIN -> new OptimizingQuery(Optimization.MAX_MIN_VALUE);
            case MINMAX -> new OptimizingQuery(Optimization.MIN_MAX_VALUE);
        };
    }

    public ModulesFileModelGenerator<Double> generator() {
        return prismGenerator;
    }

    private record PrismProperty(String name, String comment, Expression expression) {}

    private record PrismRewards(String name, PrismRewardGenerator generator) {}
}
