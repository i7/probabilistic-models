package de.tum.in.probmodels.impl.prism;

import de.tum.in.probmodels.problem.property.StepBoundableQuantity;
import java.util.OptionalInt;
import parser.State;
import parser.ast.ExpressionTemporal;

public record PrismPathQuantity(ExpressionTemporal expression, int minimalSteps, OptionalInt maximalSteps)
        implements StepBoundableQuantity<State> {
    @Override
    public String toString() {
        return "%s%s"
                .formatted(expression(), hasStepBounds() ? "[%d;%s]".formatted(minimalSteps(), maximalSteps()) : "");
    }
}
