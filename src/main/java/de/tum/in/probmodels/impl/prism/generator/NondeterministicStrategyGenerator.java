package de.tum.in.probmodels.impl.prism.generator;

import static com.google.common.base.Preconditions.checkArgument;

import de.tum.in.probmodels.generator.Action;
import de.tum.in.probmodels.generator.Explore;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.model.WeightedHyperEdge;
import de.tum.in.probmodels.util.FloatUtil;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.IntSets;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import parser.State;
import prism.ModelGenerator;

/**
 * Generates a deterministic system from (potentially) non-deterministic model by choosing according to the given strategy.
 */
public class NondeterministicStrategyGenerator extends PrismGenerator {
    private final BiFunction<State, List<Action<State, Object>>, WeightedHyperEdge> strategy;
    private final boolean removeDeadlocks;

    public NondeterministicStrategyGenerator(
            ModelGenerator<Double> generator,
            BiFunction<State, List<Action<State, Object>>, WeightedHyperEdge> strategy,
            boolean removeDeadlocks) {
        super(generator, IntSets.emptySet());
        this.strategy = strategy;
        this.removeDeadlocks = removeDeadlocks;
    }

    @Override
    public Explore<State, Object> explore(State state) {
        // TODO Extend this to games
        ModelGenerator<Double> generator = generator();
        generator.exploreState(state);

        int choiceCount = generator.getNumChoices();
        if (choiceCount == 0) {
            return new Explore<>(Actor.PROTAGONIST, removeDeadlocks ? List.of(Action.selfLoop(state)) : List.of());
        }

        List<Action<State, Object>> choices = new ArrayList<>(choiceCount);
        for (int choiceIndex = 0; choiceIndex < choiceCount; choiceIndex++) {
            int transitionCount = generator.getNumTransitions(choiceIndex);
            Object actionLabel = generator.getChoiceAction(choiceIndex);

            Object2DoubleMap<State> map = new Object2DoubleOpenHashMap<>(transitionCount);
            map.defaultReturnValue(Double.NaN);
            transitions(choiceIndex, transitionCount)
                    .forEach(entry -> map.mergeDouble(
                            entry.getKey(), entry.getDoubleValue(), (o, p) -> Double.isNaN(o) ? p : p + o));
            checkArgument(!map.isEmpty());
            choices.add(Action.of(actionLabel, map));
        }
        WeightedHyperEdge actionDistribution = strategy.apply(state, choices);

        Object2DoubleMap<State> transitions;
        if (actionDistribution.successorCount() == 1) {
            transitions = choices.get(actionDistribution.iterator().next().getIntKey())
                    .transitions();
        } else {
            transitions = new Object2DoubleOpenHashMap<>();
            transitions.defaultReturnValue(Double.NaN);
            for (Int2DoubleMap.Entry entry : actionDistribution) {
                int actionIndex = entry.getIntKey();
                double actionProbability = entry.getDoubleValue();
                for (Object2DoubleMap.Entry<State> transition :
                        choices.get(actionIndex).transitions().object2DoubleEntrySet()) {
                    transitions.merge(
                            transition.getKey(), transition.getDoubleValue() * actionProbability, Double::sum);
                }
            }
        }
        assert FloatUtil.isEqual(transitions.values().doubleStream().sum(), 1.0);
        return new Explore<>(Actor.PROTAGONIST, List.of(Action.of(transitions)));
    }

    @Override
    public String toString() {
        return "MDPuniform";
    }
}
