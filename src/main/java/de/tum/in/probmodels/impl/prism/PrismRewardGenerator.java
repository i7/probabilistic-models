package de.tum.in.probmodels.impl.prism;

import com.google.common.base.Preconditions;
import de.tum.in.probmodels.generator.RewardGenerator;
import javax.annotation.Nullable;
import parser.State;

public record PrismRewardGenerator(String name, int rewardIndex, prism.RewardGenerator<Double> generator)
        implements RewardGenerator<State> {
    @Override
    public double stateReward(State state) {
        double reward = generator.getStateReward(rewardIndex, state);
        Preconditions.checkState(!Double.isNaN(reward), "NaN reward on state %s", state);
        return reward;
    }

    @Override
    public double actionReward(State state, @Nullable Object label) {
        // Note: getStateActionReward expects to receive null as label in some cases
        double reward = generator.getStateActionReward(rewardIndex, state, label);
        Preconditions.checkState(!Double.isNaN(reward), "NaN reward on state %s / transition %s", state, label);
        return reward;
    }

    @Override
    public String toString() {
        return name;
    }
}
