package de.tum.in.probmodels.impl.prism;

import de.tum.in.probmodels.problem.property.MeanPayoffQuantity;
import de.tum.in.probmodels.values.Bounds;
import java.util.Optional;
import parser.State;

public record PrismMeanPayoffQuantity(PrismRewardGenerator reward, Optional<Bounds> rewardBounds)
        implements MeanPayoffQuantity<State> {
    @Override
    public PrismMeanPayoffQuantity withBounds(Bounds rewardBounds) {
        return new PrismMeanPayoffQuantity(reward, Optional.of(rewardBounds));
    }

    @Override
    public String toString() {
        return "MP[" + reward + "]" + (rewardBounds.isEmpty() ? "" : "@" + rewardBounds);
    }
}
