package de.tum.in.probmodels.impl.prism;

import static com.google.common.base.Preconditions.checkState;

import de.tum.in.naturals.Indices;
import de.tum.in.probmodels.model.DenseSystem;
import de.tum.in.probmodels.model.HyperEdge;
import de.tum.in.probmodels.model.HyperSystem;
import de.tum.in.probmodels.model.LabelledHyperEdge;
import de.tum.in.probmodels.model.WeightedHyperEdge;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.ListIterator;
import java.util.PrimitiveIterator;
import java.util.Set;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;

public class PrismExporter {
    private final int size;
    private final IntUnaryOperator stateIndexMap;
    private final HyperSystem<? extends WeightedHyperEdge> system;

    public PrismExporter(HyperSystem<? extends WeightedHyperEdge> system) {
        this.system = system;
        size = system.stateCount();
        if (system instanceof DenseSystem) {
            stateIndexMap = IntUnaryOperator.identity();
        } else {
            stateIndexMap = Indices.elementToIndexMap(system.states().intIterator());
        }
    }

    public void writeTrans(BufferedWriter stream) throws IOException {
        if (system.isDeterministic()) {
            int transitions = system.statesStream()
                    .mapToObj(system::onlyChoice)
                    .mapToInt(HyperEdge::successorCount)
                    .sum();
            checkState(transitions >= 0, "Integer overflow");
            stream.write(size + " " + transitions);
            stream.newLine();

            PrimitiveIterator.OfInt states = system.statesIterator();
            while (states.hasNext()) {
                int s = states.nextInt();
                int index = stateIndexMap.applyAsInt(s);
                for (Int2DoubleMap.Entry transition : system.onlyChoice(s)) {
                    stream.write(index + " " + stateIndexMap.applyAsInt(transition.getIntKey()) + " "
                            + transition.getDoubleValue());
                    stream.newLine();
                }
            }
        } else {
            PrimitiveIterator.OfInt counting = system.statesIterator();
            int actions = 0;
            int transitions = 0;
            while (counting.hasNext()) {
                int s = counting.nextInt();
                var choices = system.choices(s);
                actions += choices.size();
                for (WeightedHyperEdge distribution : choices) {
                    transitions += distribution.successorCount();
                }
                checkState(transitions >= 0, "Integer overflow");
            }
            stream.write(size + " " + actions + " " + transitions);
            stream.newLine();

            PrimitiveIterator.OfInt states = system.statesIterator();
            while (states.hasNext()) {
                int s = states.nextInt();
                int index = stateIndexMap.applyAsInt(s);
                ListIterator<? extends WeightedHyperEdge> choices =
                        system.choices(s).listIterator();
                while (choices.hasNext()) {
                    int action = choices.nextIndex();
                    var choice = choices.next();
                    for (Int2DoubleMap.Entry transition : choice) {
                        stream.write(index + " " + action + " " + stateIndexMap.applyAsInt(transition.getIntKey()) + " "
                                + transition.getDoubleValue());
                        if (choice instanceof LabelledHyperEdge<?> labelled && labelled.label() != null) {
                            stream.write(" " + labelled.label());
                        }
                        stream.newLine();
                    }
                }
            }
        }
    }

    public void writeLabels(Set<String> labels, IntFunction<Set<String>> labelling, BufferedWriter stream)
            throws IOException {
        Object2IntMap<String> labelIndex = new Object2IntOpenHashMap<>(labels.size() + 2);
        labelIndex.defaultReturnValue(-1);
        labelIndex.put("init", 0);
        labelIndex.put("deadlock", 1);
        stream.write("0=\"init\" 1=\"deadlock\"");
        for (String label : labels) {
            int index = labelIndex.size();
            if (labelIndex.putIfAbsent(label, index) == -1) {
                stream.write(" " + index + "=\"" + label.replace("\"", "\\\"") + "\"");
            }
        }
        stream.newLine();
        IntSet initial = system.initialStates();

        PrimitiveIterator.OfInt states = system.statesIterator();
        int stateLabelCount = 0;
        int[] stateLabelIndices = new int[labelIndex.size()];
        while (states.hasNext()) {
            int s = states.nextInt();
            if (initial.contains(s)) {
                stateLabelIndices[0] = 0;
                stateLabelCount += 1;
            }
            var stateLabels = labelling.apply(s);
            assert labels.containsAll(stateLabels);
            for (String label : stateLabels) {
                stateLabelIndices[stateLabelCount] = labelIndex.getInt(label);
                stateLabelCount += 1;
            }
            if (stateLabelCount > 0) {
                Arrays.sort(stateLabelIndices, 0, stateLabelCount);
                stream.write(s + ":");
                for (int i = 0; i < stateLabelCount; i++) {
                    stream.write(" " + stateLabelIndices[i]);
                }
                stream.newLine();
            }
            stateLabelCount = 0;
        }
    }
}
