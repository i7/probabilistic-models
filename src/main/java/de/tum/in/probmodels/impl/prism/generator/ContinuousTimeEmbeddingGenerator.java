package de.tum.in.probmodels.impl.prism.generator;

import de.tum.in.probmodels.generator.Action;
import de.tum.in.probmodels.generator.Explore;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.util.FloatUtil;
import it.unimi.dsi.fastutil.ints.IntSets;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;
import java.util.Collections;
import parser.State;
import prism.ModelGenerator;

/**
 * Creates the embedded DTMC of a given CTMC.
 */
public class ContinuousTimeEmbeddingGenerator extends PrismGenerator {
    public ContinuousTimeEmbeddingGenerator(ModelGenerator<Double> generator) {
        super(generator, IntSets.emptySet());
    }

    @Override
    public Explore<State, Object> explore(State state) {
        ModelGenerator<Double> generator = generator();
        generator.exploreState(state);

        int choiceCount = generator.getNumChoices();
        if (choiceCount == 0) {
            return new Explore<>(Actor.PROTAGONIST, Collections.emptyList());
        }

        Object2DoubleMap<State> map = new Object2DoubleOpenHashMap<>();
        map.defaultReturnValue(Double.NaN);
        FloatUtil.KahanSum sum = new FloatUtil.KahanSum();
        for (int choiceIndex = 0; choiceIndex < choiceCount; choiceIndex++) {
            int choiceTransitionCount = generator.getNumTransitions(choiceIndex);

            for (Object2DoubleMap.Entry<State> entry : transitions(choiceIndex, choiceTransitionCount)) {
                double probability = entry.getDoubleValue();
                double oldValue = map.put(entry.getKey(), probability);
                assert Double.isNaN(oldValue);
                sum.add(probability);
            }
        }

        if (map.isEmpty()) {
            return new Explore<>(Actor.PROTAGONIST, Collections.emptyList());
        }
        if (!FloatUtil.isOne(sum.get())) {
            for (Object2DoubleMap.Entry<State> entry : map.object2DoubleEntrySet()) {
                entry.setValue(entry.getDoubleValue() / sum.get());
            }
        }
        return new Explore<>(Actor.PROTAGONIST, Collections.singleton(Action.of(map)));
    }

    @Override
    public String toString() {
        return "CTMCEmbeddingGen";
    }
}
