package de.tum.in.probmodels.impl.prism;

import java.util.function.Predicate;
import parser.State;
import parser.ast.Expression;

public record PrismExpressionWrapper(Expression expression) implements Predicate<State> {
    @Override
    public boolean test(State state) {
        return expression.evaluateBoolean(state);
    }

    @Override
    public String toString() {
        return expression.toString();
    }
}
