package de.tum.in.probmodels.impl.prism.generator;

import static com.google.common.base.Preconditions.checkArgument;

import de.tum.in.probmodels.generator.Action;
import de.tum.in.probmodels.generator.Explore;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.util.FloatUtil;
import it.unimi.dsi.fastutil.ints.IntSets;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;
import java.util.Set;
import parser.State;
import prism.ModelGenerator;

/**
 * Creates the uniformized model of a given continuous time model.
 */
public class ContinuousTimeUniformizingModel extends PrismGenerator {
    private final double rate;

    public ContinuousTimeUniformizingModel(ModelGenerator<Double> generator, double rate) {
        super(generator, IntSets.emptySet());
        checkArgument(rate > 0.0);
        this.rate = rate;
    }

    @Override
    public Explore<State, Object> explore(State state) {
        ModelGenerator<Double> generator = generator();
        generator.exploreState(state);

        int choiceCount = generator.getNumChoices();
        int transitionCount = generator.getNumTransitions();
        Object2DoubleMap<State> map = new Object2DoubleOpenHashMap<>(transitionCount);
        map.defaultReturnValue(Double.NaN);

        FloatUtil.KahanSum sum = new FloatUtil.KahanSum();
        for (int choiceIndex = 0; choiceIndex < choiceCount; choiceIndex++) {
            int choiceTransitionCount = generator.getNumTransitions(choiceIndex);

            for (Object2DoubleMap.Entry<State> entry : transitions(choiceIndex, choiceTransitionCount)) {
                State target = entry.getKey();
                double probability = entry.getDoubleValue();
                checkArgument(probability <= rate, "Rate %s smaller than transition probability %s", rate, probability);

                if (!target.equals(state)) {
                    double uniformizedProbability = probability / rate;
                    sum.add(uniformizedProbability);
                    double oldValue = map.put(target, uniformizedProbability);
                    assert Double.isNaN(oldValue);
                }
            }
        }

        if (sum.get() < 1.0d) {
            map.put(state, 1 - sum.get());
        }
        return new Explore<>(Actor.PROTAGONIST, Set.of(Action.of(map)));
    }

    @Override
    public String toString() {
        return String.format("CTMCUniformizingGen(%.2f)", rate);
    }
}
