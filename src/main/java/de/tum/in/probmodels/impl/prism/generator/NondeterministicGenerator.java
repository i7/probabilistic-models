package de.tum.in.probmodels.impl.prism.generator;

import static com.google.common.base.Preconditions.checkArgument;

import de.tum.in.probmodels.generator.Action;
import de.tum.in.probmodels.generator.Explore;
import de.tum.in.probmodels.model.Actor;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;
import java.util.ArrayList;
import java.util.List;
import parser.State;
import prism.ModelGenerator;

/**
 * Generates a (potentially) non-deterministic system.
 */
public class NondeterministicGenerator extends PrismGenerator {
    private final boolean removeDeadlocks;

    public NondeterministicGenerator(
            ModelGenerator<Double> generator, IntSet antagonisticPlayers, boolean removeDeadlocks) {
        super(generator, antagonisticPlayers);
        this.removeDeadlocks = removeDeadlocks;
    }

    @Override
    public Explore<State, Object> explore(State state) {
        ModelGenerator<Double> generator = generator();
        generator.exploreState(state);

        int choiceCount = generator.getNumChoices();
        List<Action<State, Object>> choices = new ArrayList<>(choiceCount);

        for (int choiceIndex = 0; choiceIndex < choiceCount; choiceIndex++) {
            int transitionCount = generator.getNumTransitions(choiceIndex);
            Object actionLabel = generator.getChoiceAction(choiceIndex);

            Object2DoubleMap<State> map = new Object2DoubleOpenHashMap<>(transitionCount);
            map.defaultReturnValue(Double.NaN);
            transitions(choiceIndex, transitionCount)
                    .forEach(entry -> map.mergeDouble(
                            entry.getKey(), entry.getDoubleValue(), (o, p) -> Double.isNaN(o) ? p : p + o));
            checkArgument(!map.isEmpty());
            choices.add(Action.of(actionLabel, map));
        }
        List<Action<State, Object>> result =
                (removeDeadlocks && choices.isEmpty()) ? List.of(Action.selfLoop(state)) : choices;
        boolean protagonist = !multiplePlayers() || isProtagonist(generator.getPlayerOwningState());
        return new Explore<>(protagonist ? Actor.PROTAGONIST : Actor.ANTAGONIST, result);
    }

    @Override
    public String toString() {
        return "NondetGen";
    }
}
