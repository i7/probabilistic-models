package de.tum.in.probmodels.impl.prism;

import de.tum.in.probmodels.generator.RewardGenerator;
import de.tum.in.probmodels.problem.property.TotalRewardQuantity;
import de.tum.in.probmodels.problem.property.UntilType;
import de.tum.in.probmodels.values.Bounds;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.function.Function;
import parser.State;
import parser.ast.Expression;

public record PrismTotalRewardQuantity(
        int minimalSteps,
        OptionalInt maximalSteps,
        RewardGenerator<State> reward,
        Function<State, UntilType> reachability,
        Expression expression,
        Optional<Bounds> rewardBounds)
        implements TotalRewardQuantity<State> {
    @Override
    public String toString() {
        return expression.toString();
    }
}
