package de.tum.in.probmodels.impl.prism;

import static picocli.CommandLine.Option;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

@SuppressWarnings("PMD.ImmutableField")
public class PrismModelMixin {
    @Option(
            names = {"-m", "--model"},
            required = true,
            description = "Path to model file")
    private Path modelPath;

    @Option(
            names = {"-c", "--const"},
            description = "Model constants",
            split = ",")
    private List<String> constants = List.of();

    @Option(names = "--uniformization", description = "Uniformization rate (for CTMC)")
    private double uniformizationRate;

    @Option(
            names = {"-p", "--properties"},
            description = "Path to properties file")
    private Path propertiesPath;

    public Path modelPath() {
        return modelPath;
    }

    public List<String> constants() {
        return List.copyOf(constants);
    }

    public PrismProblemInstance parse() throws IOException {
        return PrismProblemInstance.of(modelPath, propertiesPath, constants(), uniformizationRate);
    }
}
