package de.tum.in.probmodels.impl.prism.generator;

import static com.google.common.base.Preconditions.checkArgument;

import de.tum.in.probmodels.generator.Explore;
import de.tum.in.probmodels.generator.Generator;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.objects.AbstractObject2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;
import parser.State;
import prism.ModelGenerator;

/**
 * Abstract base class for generators based on PRISM models.
 */
public abstract class PrismGenerator implements Generator<State, Object> {
    private final ModelGenerator<Double> generator;
    private final IntSet antagonisticPlayers;
    private final boolean deterministic;
    private final int playerCount;

    public PrismGenerator(ModelGenerator<Double> generator, IntSet antagonisticPlayers) {
        this.generator = generator;
        deterministic = !generator.getModelType().nondeterministic();
        int playerCount = generator.getNumPlayers();
        if (antagonisticPlayers.isEmpty() && !deterministic && playerCount == 0) {
            playerCount = 1;
        }
        this.playerCount = playerCount;
        checkArgument(antagonisticPlayers.intStream().allMatch(i -> i < this.playerCount));
        this.antagonisticPlayers = antagonisticPlayers;
        assert deterministic || playerCount > 0;
    }

    public ModelGenerator<Double> generator() {
        return generator;
    }

    @Override
    public Set<State> initialStates() {
        return Set.copyOf(generator.getInitialStates());
    }

    @Override
    public abstract Explore<State, Object> explore(State state);

    @Override
    public boolean isDeterministic() {
        return deterministic;
    }

    public boolean multiplePlayers() {
        return playerCount > 1;
    }

    public boolean isProtagonist(int player) {
        assert player < playerCount;
        return !antagonisticPlayers.contains(player);
    }

    protected Collection<Object2DoubleMap.Entry<State>> transitions(int choiceIndex, int count) {
        return new TransitionLazyCollection(generator, count, choiceIndex);
    }

    private static final class TransitionLazyCollection extends AbstractCollection<Object2DoubleMap.Entry<State>> {
        private final ModelGenerator<Double> generator;
        private final int count;
        private final int choiceIndex;

        TransitionLazyCollection(ModelGenerator<Double> generator, int count, int choiceIndex) {
            this.generator = generator;
            this.count = count;
            this.choiceIndex = choiceIndex;
        }

        @Override
        public Iterator<Object2DoubleMap.Entry<State>> iterator() {
            return new TransitionLazyIterator(generator, choiceIndex, count);
        }

        @Override
        public int size() {
            return count;
        }
    }

    private static final class TransitionLazyIterator implements Iterator<Object2DoubleMap.Entry<State>> {
        private final ModelGenerator<Double> generator;
        private final int transitionCount;
        private final int choiceIndex;
        private int transitionIndex = 0;

        TransitionLazyIterator(ModelGenerator<Double> generator, int choiceIndex, int transitionCount) {
            this.generator = generator;
            this.choiceIndex = choiceIndex;
            this.transitionCount = transitionCount;
        }

        @Override
        public boolean hasNext() {
            return transitionIndex < transitionCount;
        }

        @Override
        public Object2DoubleMap.Entry<State> next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            State target = generator.computeTransitionTarget(choiceIndex, transitionIndex);
            double probability = generator.getTransitionProbability(choiceIndex, transitionIndex);
            assert probability > 0.0d;

            var entry = new AbstractObject2DoubleMap.BasicEntry<>(target, probability);
            transitionIndex += 1;
            return entry;
        }
    }
}
