package de.tum.in.probmodels.impl.prism.generator;

import de.tum.in.probmodels.generator.Action;
import de.tum.in.probmodels.generator.Explore;
import de.tum.in.probmodels.model.Actor;
import it.unimi.dsi.fastutil.ints.IntSets;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleMaps;
import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;
import java.util.Collections;
import parser.State;
import prism.ModelGenerator;

/**
 * Generates a deterministic system (i.e. only stochastic transitions).
 */
public class StochasticGenerator extends PrismGenerator {
    private final boolean removeDeadlocks;

    public StochasticGenerator(ModelGenerator<Double> generator, boolean removeDeadlocks) {
        super(generator, IntSets.emptySet());
        this.removeDeadlocks = removeDeadlocks;
    }

    @Override
    public Explore<State, Object> explore(State state) {
        ModelGenerator<Double> generator = generator();
        generator.exploreState(state);

        int choiceCount = generator.getNumChoices();
        assert choiceCount <= 1 : String.format("Got %d choices for %s", choiceCount, state);
        if (choiceCount == 0) {
            return new Explore<>(Actor.PROTAGONIST, Collections.emptyList());
        }

        int transitionCount = generator.getNumTransitions(0);
        Object2DoubleMap<State> map = new Object2DoubleOpenHashMap<>(transitionCount);
        map.defaultReturnValue(Double.NaN);
        Object label = generator.getChoiceAction(0);
        for (Object2DoubleMap.Entry<State> entry : transitions(0, transitionCount)) {
            assert entry.getDoubleValue() > 0.0;
            map.mergeDouble(entry.getKey(), entry.getDoubleValue(), (o, p) -> Double.isNaN(o) ? p : p + o);
        }

        return new Explore<>(
                Actor.PROTAGONIST,
                Collections.singleton(Action.of(
                        label, removeDeadlocks && map.isEmpty() ? Object2DoubleMaps.singleton(state, 1.0) : map)));
    }

    @Override
    public String toString() {
        return "MCgen";
    }
}
