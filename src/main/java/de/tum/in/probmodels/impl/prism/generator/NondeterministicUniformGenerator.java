package de.tum.in.probmodels.impl.prism.generator;

import de.tum.in.probmodels.generator.Action;
import de.tum.in.probmodels.generator.Explore;
import de.tum.in.probmodels.model.Actor;
import de.tum.in.probmodels.util.FloatUtil;
import it.unimi.dsi.fastutil.ints.IntSets;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;
import java.util.List;
import parser.State;
import prism.ModelGenerator;

/**
 * Generates a deterministic system from (potentially) non-deterministic model by uniformly choosing among the given
 * actions.
 */
public class NondeterministicUniformGenerator extends PrismGenerator {
    private final boolean removeDeadlocks;

    public NondeterministicUniformGenerator(ModelGenerator<Double> generator, boolean removeDeadlocks) {
        super(generator, IntSets.emptySet());
        this.removeDeadlocks = removeDeadlocks;
    }

    @Override
    public Explore<State, Object> explore(State state) {
        ModelGenerator<Double> generator = generator();
        generator.exploreState(state);

        int choiceCount = generator.getNumChoices();
        if (choiceCount == 0) {
            return new Explore<>(Actor.PROTAGONIST, removeDeadlocks ? List.of(Action.selfLoop(state)) : List.of());
        }
        Object2DoubleMap<State> map = new Object2DoubleOpenHashMap<>();
        map.defaultReturnValue(Double.NaN);
        for (int choiceIndex = 0; choiceIndex < choiceCount; choiceIndex++) {
            transitions(choiceIndex, generator.getNumTransitions(choiceIndex))
                    .forEach(entry -> map.mergeDouble(
                            entry.getKey(), entry.getDoubleValue(), (o, p) -> Double.isNaN(o) ? p : p + o));
        }
        assert FloatUtil.isEqual(map.values().doubleStream().sum(), 1.0 * choiceCount);
        Object2DoubleMap<State> remap = new Object2DoubleOpenHashMap<>(map.size());
        map.forEach((s, p) -> remap.put(s, p / choiceCount));
        return new Explore<>(Actor.PROTAGONIST, List.of(Action.of(remap)));
    }

    @Override
    public String toString() {
        return "NDetUniform";
    }
}
