package de.tum.in.probmodels.impl.prism;

import de.tum.in.probmodels.problem.property.UntilQuantity;
import de.tum.in.probmodels.problem.property.UntilType;
import java.util.OptionalInt;
import java.util.function.Function;
import parser.State;
import parser.ast.Expression;

public record PrismUntilQuantity(
        int minimalSteps,
        OptionalInt maximalSteps,
        Function<State, UntilType> type,
        Expression expression,
        boolean isNegated)
        implements UntilQuantity<State> {
    @Override
    public String toString() {
        return expression.toString();
    }
}
